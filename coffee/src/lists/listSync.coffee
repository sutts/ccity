{ListController} = require './listController.coffee'

class ListSync

  constructor: (@Resource, @url) ->
    throw new Error "ListSync really needs its Resource else it can't not do bugger all" unless @Resource?

  initialize: (@controller, loadNow=true) ->
    @controller.newResource = => new @Resource
    @controller.broadcast 'syncInitialized'
    @controller.on 'itemAdded',   (item) => @_crud item, 'save'
    @controller.on 'itemUpdated', (item) => @_crud item, 'save'
    @controller.on 'itemDeleted', (item) => @_crud item, 'delete'
    @controller.resource = @Resource
    @load() if loadNow

  load: ->
    promise = if @url then @Resource.$get(@_resolveUrl(@url)) else @Resource.query()
    success = (results) => @controller.initialize results
    failure = (error) => @controller.broadcast 'error', error
    promise.then success, failure

  _crud: (item, action) ->
    if @controller.prepareSync?
      item = @controller.prepareSync item, action
    promise = item[action]()
    success = (results) => # all good
    failure = (error) => @controller.broadcast 'error', error
    promise.then success, failure

  _resolveUrl: (url) ->
    vars = /\$\([^)]*\)/.exec url
    if vars
      name = vars[0][2..-2]
      root = @window or window
      value = root[name]
      throw new Error "Could not resolve variable '#{name}'" unless value?
      url.replace /\$\([^)]*\)/, value 
    else
      url




module.exports.ListSync = ListSync