_ = require 'lodash'
{BaseController} = require './baseController.coffee'

class MultiListController extends BaseController

  constructor: ->
    @lists = []
    super

  withList: (ctrl, name, typeName, referenceDescriptors...) ->
    throw new Error "Naming conflict on '#{name}'" if @[name]
    @[name] = ctrl
    @['list'] = ctrl unless @['list']
    @lists.push ctrl
    @types ||= {}
    if typeName
      @types[typeName] = ctrl
      ctrl.referenceDescriptors = referenceDescriptors
    ctrl.on 'error', (err) =>
      @broadcast 'error', err
    ctrl.on 'ready', =>
      for list in @lists
        return unless list.ready
      @_resolveReferences(list) for list in @lists
      @ready = true
      @broadcast 'ready'
    ctrl.on ['itemUpdated', 'itemAdded'], => @_resolveReferences ctrl
    this

  _resolveReferences: (list) ->
    resolve = (value) =>
      if value?.id > 0 and value?.type
        referencedList = @types[value.type]
        referencedList?.find value.id
    walkAndWork = (obj, pathTokens) =>
      if pathTokens.length is 1
        token = pathTokens[0]
        value = obj[token]
        if _.isArray(value)
          obj["$#{token}"] = _.map value, (val) -> resolve(val)
        else
          obj["$#{token}"] = resolve(value)
      else
        next = obj[_.first(pathTokens)]
        if next
          if _.isArray(next)
            _.each next, (nextValue) -> walkAndWork nextValue, _.rest(pathTokens)
          else
            walkAndWork next, _.rest(pathTokens)
    if list.referenceDescriptors
      for referenceDescriptor in list.referenceDescriptors
        for item in list.items
          walkAndWork item, referenceDescriptor.split('.')
 

module.exports.MultiListController = MultiListController