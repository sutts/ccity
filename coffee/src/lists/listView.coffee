{ListController} = require './listController.coffee'

class ListView

  constructor: (@controller, @pagesize = 50) ->
    @filters = []
    @refresh()
    @controller.on 'listChanged', => @refresh()

  addFilter: (filter) ->
    @filters.push filter
    @refresh()
    filter.onChange = => @refresh()
    filter

  refresh: ->
    if @controller.ready
      @availableItems = @controller.items.filter (item) =>
        for filter in @filters
          return false unless filter.accept(item)
        true
      @controller.broadcast 'viewChanged'
    else
      @availableItems = []
    @pages = Math.min Math.ceil(@availableItems.length / @pagesize)
    @setPage 1

  setPage: (page) ->
    if @pages is 0
      @items = []
      @page = 0
      @controller.broadcast 'pageChanged'
    else if page >= 1 and page <= @pages
      @page = page
      indexOfFirstItem = (page - 1) * @pagesize
      @items = @availableItems[indexOfFirstItem...(indexOfFirstItem + @pagesize)]
      @controller.broadcast 'pageChanged'

module.exports.ListView = ListView

