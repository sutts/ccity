_ = require 'lodash'

class ListEditor

  constructor: (@controller) ->
    @rollbackProperties = []
    @controller.on 'syncInitialized', => @newItem = @controller.newResource()

  create: ->
    console.log 'ListEditor::create is deprecated. Use commit() instead'
    @controller.add @newItem
    @newItem = @controller.newResource()

  begin: (item) ->
    @cancel() unless @current is item or not @current?
    @current = item
    @controller.snapshot @current
    this

  editing: (item) ->
    @current is item

  cancel: () ->
    if @current?
      @controller.rollback @current
      @current = undefined

  commit: ->
    if @current?
      if @current is @newItem
        @controller.add @newItem
        @newItem = @controller.newResource()
      else
        @controller.updated @current
      @current = undefined

  delete: (item) ->
    if item?
      @controller.remove item
    else if @current?
      @delete @current
      @current = undefined

module.exports.ListEditor = ListEditor