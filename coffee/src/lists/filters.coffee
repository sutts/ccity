_ = require 'lodash'

class BaseFilter

  constructor: (fieldNames...) ->
    @fieldNames = _.flatten fieldNames

  setValue: (value) ->
    @value = if value? then @compile(value) else undefined
    @onChange() if @onChange?

  withValue: (value) ->
    @setValue value
    this

  compile: (value) ->
    value

  accept: (item) ->
    throw new Error "Please specify at least one field name you bozo!" unless @fieldNames.length > 0
    return true unless @value
    for fieldName in @fieldNames
      return true if item[fieldName]? and @matches item[fieldName]
    false

  matches: (fieldValue) ->
    fieldValue is @value


class NumericFilter extends BaseFilter
  compile: (value) -> parseInt(value)


class PartialMatchFilter extends BaseFilter
  compile: (value) ->
    if value?.length > 0
      @regex = new RegExp value, 'i'
      value

  matches: (fieldValue) ->
    @regex.test(fieldValue)

class RovingPartialMatchFilter extends BaseFilter

  constructor: (@maxSearchDepth=3) ->

  compile: (value) ->
    if value?.length > 0
      @regex = new RegExp value, 'i'
      value

  accept: (item) ->
    return true unless @value
    matches = (maxDepth, regex, obj, depth) ->
      for own key, value of obj
        if _.isString(value)
          return true if regex.test(value)
        if _.isArray(value)
          return true if _.find value, (val) -> regex.test(val)
        if _.isObject(value) and depth < maxDepth
          return true if matches maxDepth, regex, value, depth + 1
      false
    matches @maxSearchDepth, @regex, item, 1

module.exports =
  createBaseFilter: (fieldNames...) -> new BaseFilter fieldNames
  createNumericFilter: (fieldNames...) -> new NumericFilter fieldNames
  createPartialMatchFilter: (fieldNames...) -> new PartialMatchFilter fieldNames  
  createRovingPartialMatchFilter: (maxDepth) -> new RovingPartialMatchFilter maxDepth
