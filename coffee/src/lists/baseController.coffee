_ = require 'lodash'

class BaseController

  constructor: ->
    @ready = false
    @eventListeners = []

  on: (event, callback) =>
    if _.isArray(event)
      @on e, callback for e in event
    else
      @eventListeners.push {event: event, callback: callback}

  broadcast: (event, arg) ->
    @lastError = arg if event is 'error'
    for eventListener in @eventListeners
      eventListener.callback(arg) if event is eventListener.event

module.exports=BaseController: BaseController