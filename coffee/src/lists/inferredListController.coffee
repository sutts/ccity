_ = require 'lodash'
{ListController} = require './listController.coffee'

class InferredListController extends ListController

  constructor: (@parent, @propertyName) ->
    super
    @parent.on 'listChanged', => @_refresh()
    @.on 'error', (error) => @parent.broadcast 'error', error 

  _refresh: ->
    tmp = {}
    for parentItem in @parent.items
      handle = (property) ->
        if property?
          tmp[property] ?= 
            name: property
            originalName: property
            references: []
          tmp[property].references.push parentItem
      if _.isArray parentItem[@propertyName]
        handle property for property in parentItem[@propertyName]
        # aside (aka doing too many things) - sort the parent's items while we're here
        parentItem[@propertyName].sort()
      else
        handle parentItem[@propertyName]
    # convert object (map) to simple array and sort
    unsorted = (val for key,val of tmp)
    items = _.sortBy unsorted, 'name'
    if @ready
      @items = items
      @broadcast "listChanged"
    else
      @initialize items

  itemNames: =>
    _.pluck @items, 'name'

  snapshot: (item) ->
    # do nothing (only one editable property and we keep a safe record of that already)

  rollback: (item) ->
    item.name = item.originalName

  updated: (item) ->
    for parentItem in @parent.items
      if _.isArray parentItem[@propertyName]
        parentItem[@propertyName] = parentItem[@propertyName].map (e) -> if e is item.originalName then item.name else e
      else
        parentItem[@propertyName] = item.name if parentItem[@propertyName] is item.originalName
    super

  remove: (item) ->
    for parentItem in @parent.items
      if _.isArray parentItem[@propertyName]
        parentItem[@propertyName] = _.without parentItem[@propertyName], item.name
      else if parentItem[@propertyName]?
        throw new Error 'InferredListController::delete not supported on single value properties'
    super

  prepareSync: (item, action) ->
    # throw new Error "Illegal state (inferred list parent does not have typename)" unless @parent.typeName?
    resource = @newResource()
    resource.id = item.originalName
    resource.name = item.name
    resource


module.exports.InferredListController = InferredListController