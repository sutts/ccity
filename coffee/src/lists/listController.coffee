_ = require 'lodash'
{BaseController} = require './baseController.coffee'

class ListController extends BaseController

  constructor: ->
    @items = []
    @expansions = []
    super

  broadcast: (event, arg) ->
    super
    if event in ['ready', 'itemAdded', 'itemUpdated', 'itemDeleted']
      @broadcast 'listChanged'

  initialize: (items) ->
    throw new Error "Already initialized!" if @ready
    @items = if @sortFn then @sortFn(items) else items
    @ready = true
    @broadcast 'ready'

  add: (item) ->
    throw new Error "ListController asked to add but no item supplied" unless item?
    @items.push item
    @broadcast 'itemAdded', item

  remove: (item) ->
    throw new Error "ListController asked to remove an item but item not in list" unless item in @items
    _.remove @items, item
    @broadcast 'itemDeleted', item

  updated: (item) ->
    throw new Error "ListController told item updated but item not in list" unless item in @items
    @broadcast 'itemUpdated', item

  snapshot: (item) ->
    item.snapshot()

  rollback: (item) ->
    item.rollback()
    item.snapshot()

  find: (id) ->
    _.find @items, (item) -> item.id is id

  findRef: (ref) ->
    tokens = ref.split ':'
    if tokens.length is 2
      @find parseInt(tokens[1])

  references: (listName) -> 
      expansions = @expansions
      through: (fieldName) -> 
        andExpandsTo: (expandedFieldName) ->
          expansions.push { references: listName, through: fieldName, expandTo: expandedFieldName }


module.exports.ListController = ListController