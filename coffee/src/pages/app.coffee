window.app = angular.module 'app', ['rails', 's2', 'multi-transclude', 'angularFileUpload']

app.config ['$logProvider', ($logProvider) ->
  $logProvider.debugEnabled false
]

createFactory = (resource, name, url, extenderFn) ->
  app.factory resource, ['railsResourceFactory', (railsResourceFactory) ->
    f = railsResourceFactory { name: name, url: url, extensions: ['snapshots'] }
    extenderFn(f) if extenderFn
    f
  ]

createFactory 'Position', 'position', '/admin/positions'
createFactory 'PositionTag', 'tag', '/admin/positions/tags'

createFactory 'Submittable', 'submittable', '/admin/submittables'
createFactory 'SubmittableCategory', 'category', '/admin/submittables/categories'

createFactory 'Requirement', 'requirement', '/admin/requirements'

createFactory 'User', 'user', '/admin/users'
createFactory 'UserTag', 'tag', '/admin/users/tags'

createFactory 'Vendor', 'contract', '/admin/vendors'
createFactory 'Client', 'contract', '/admin/clients'

createFactory 'Account', 'account', 'account'
createFactory 'Password', 'password', 'account/password'

createFactory 'Person', 'person', '/people'
createFactory 'Submission', 'submission', '/submissions'

createFactory 'Registration', 'registration', '/registrations', (Registration) ->
  Registration.prototype.submitterName = () -> "#{@submitter?.givenName} #{@submitter?.familyName}"

createFactory 'Decision', 'decision', '/decisions'

createFactory 'ExemptionRequest', 'exemptionRequest', '/exemption_requests'
