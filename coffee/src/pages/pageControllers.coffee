_ = require 'lodash'
{ListController} = require '../lists/listController.coffee'
{InferredListController} = require '../lists/inferredListController.coffee'
{ListView} = require '../lists/listView.coffee'
{ListSync} = require '../lists/listSync.coffee'
{ListEditor} = require '../lists/listEditor.coffee'
{MultiListController} = require '../lists/multiListController.coffee'
{RegistrationPageCtrl} = require '../pages/registrationPageCtrl.coffee'
{PersonPageCtrl} = require '../pages/personPageCtrl.coffee'
filters = require '../lists/filters.coffee'

class ListControllerBuilder 
  
  constructor: () ->
    @listController = new ListController
  withView: ->
    @listController.view = new ListView @listController, 50
    this
  withEditor: () ->
    @listController.editor = new ListEditor @listController
    @listController.on ['itemAdded', 'itemUpdated'], (item) =>
      e.attentionSeeking = false for e in @listController.items
      item.attentionSeeking = true
    this
  withChoosables: (type, fields...) ->
    @listController.on 'ready', =>
      @listController.choosables = _.map @listController.items, (e) => 
        obj = {type: type, id: e.id}
        _.each fields, (f) -> obj[f] = e[f]
        obj
    this
  withQuickSearchFilter: (maxDepth) ->
    @listController.quickSearchFilter = @listController.view.addFilter filters.createRovingPartialMatchFilter(maxDepth)
    this
  withInferred: (controllerPropertyName, itemPropertyName, Resource) ->
    @listController[controllerPropertyName] = new InferredListController @listController, itemPropertyName
    if Resource?
      @listController[controllerPropertyName].editor = new ListEditor @listController[controllerPropertyName]
      @listController[controllerPropertyName].editor.rollbackProperties = ['name']
      sync = new ListSync Resource
      sync.initialize @listController[controllerPropertyName], false
    this
  withSimpleSort: (property) ->
    @listController.sortFn = (items) -> _.sortBy items, property 
    this
  sync: (Resource, path) ->
    sync = new ListSync Resource, path
    sync.initialize @listController
    @listController

positions = (PositionResource, PositionTagResource) ->
  new ListControllerBuilder().withSimpleSort('name').withView().withEditor().withQuickSearchFilter().
                              withInferred('tags', 'tags', PositionTagResource).
                              sync(PositionResource)

positionsReadOnly = (PositionResource) ->
  new ListControllerBuilder().withSimpleSort('name').withInferred('tags', 'tags').
                              withChoosables('Position', 'name').
                              sync(PositionResource)

submittables = (SubmittableResource, SubmittableCategoryResource) ->
  new ListControllerBuilder().withSimpleSort(['name', 'category']).withView().withEditor().withQuickSearchFilter().
                              withInferred('categories', 'category', SubmittableCategoryResource).
                              sync(SubmittableResource)

submittablesReadOnly = (SubmittableResource) ->
  new ListControllerBuilder().withSimpleSort(['name', 'category']).withInferred('categories', 'category').
                              withChoosables('Submittable', 'name', 'category').
                              sync(SubmittableResource)

submissions = (Submission) ->
  new ListControllerBuilder().withView().withEditor().
                              sync(Submission, '/submissions/person/$(personId)')

users = (UserResource, UserTagResource) ->
  new ListControllerBuilder().withView().withEditor().withQuickSearchFilter().
                              withInferred('tags', 'tags', UserTagResource).
                              sync(UserResource)

usersReadOnly = (UserResource) ->
  new ListControllerBuilder().withInferred('tags', 'tags').
                              sync(UserResource)

vendors = (VendorResource) ->
  new ListControllerBuilder().withView().sync(VendorResource)

clients = (ClientResource) ->
  new ListControllerBuilder().withView().sync(ClientResource)

requirements = (RequirementResource) ->
  new ListControllerBuilder().withView().withEditor().withQuickSearchFilter(5).sync(RequirementResource)

people = (Person) ->
  new ListControllerBuilder().withEditor().sync(Person)

app.controller 'PositionsPageCtrl',
  class PositionsPageCtrl extends MultiListController
    constructor: ($scope, Position, PositionTag) ->
      super
      @withList positions(Position, PositionTag), 'positions'

app.controller 'SubmittablesPageCtrl',
  class SubmittablesPageCtrl extends MultiListController
    constructor: ($scope, Submittable, SubmittableCategory) ->
      super
      @withList submittables(Submittable, SubmittableCategory), 'submittables'
    fieldNames: (item) =>
      list = []
      for field in ['number', 'issuedBy', 'issueDate', 'expiryDate']
        list.push item["#{field}As"] if item[field]
      list

app.controller 'PeoplePageCtrl',
  class PeoplePageCtrl extends MultiListController
    constructor: ($scope, Person) ->
      super
      @withList people(Person), 'persons'
      @on 'ready', => @persons.editor.begin @persons.editor.newItem
      @persons.on 'itemAdded', (item) => @redirectTo item
    redirectTo: (item) ->
      if item.id
        window.location.href = "/people/#{item.id}"
      else
        _.delay (=> @redirectTo item), 100

app.controller 'UsersPageCtrl',
  class UsersPageCtrl extends MultiListController
    constructor: ($scope, User, UserTag) ->
      super
      @withList users(User, UserTag), 'users'

app.controller 'VendorsPageCtrl',
  class VendorsPageCtrl extends MultiListController
    constructor: ($scope, Vendor) ->
      super
      @withList vendors(Vendor), 'vendors'

app.controller 'ClientsPageCtrl',
  class ClientsPageCtrl extends MultiListController
    constructor: ($scope, Client) ->
      super
      @withList clients(Client), 'clients'

app.controller 'RequirementsPageCtrl',
  class RequirementsPageCtrl extends MultiListController
    constructor: ($scope, Requirement, Submittable, Position, User) ->
      super
      @withList requirements(Requirement), 'requirements', 'Requirement', 'target', 'filters.references'
      @withList positionsReadOnly(Position), 'positions', 'Position'
      @withList submittablesReadOnly(Submittable), 'submittables', 'Submittable'
      @withList usersReadOnly(User), 'users', 'User'
      @on 'ready', =>
        @list.items = _.sortBy @list.items, (e) -> e.$target.name


app.controller 'AccountPageCtrl',
  class AccountPageCtrl
    constructor: (Account) ->
      builder = new ListControllerBuilder('account').withEditor('first_name', 'last_name', 'email')
      @page = builder.sync(Account)
      @page.on 'ready', => @page.editor.begin @page.items[0]

app.controller 'PersonPageCtrl', PersonPageCtrl
app.controller 'RegistrationPageCtrl', RegistrationPageCtrl
