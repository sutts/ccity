_ = require 'lodash'

taskLabels = 
  upfront: { when: 'now',  css: 'label-danger',  sortIndex: 0 }
  soon:    { when: 'soon', css: 'label-warning', sortIndex: 1 }

markAsResolved = (task) ->
  task.resolved = true
  task.label = { when: 'done', css: 'label-success'  }

class SubmissionsEditor

  constructor: (@ctrl, @Submission) ->

  begin: (task) ->
    unless @current?
      @current = task
      @current.entries = @ctrl.submissionEntries @current.submittable

  cancel: ->
    @current = undefined

  commit: ->
    if @current
      # decorate
      markAsResolved @current
      # resource stuff
      resource = new @Submission
      resource[entry.property] = entry.value for entry in @current.entries
      resource.attachments = @current.attachments
      resource.person = { id: @ctrl.person.id }
      resource.submittable = { id: @current.submittable.id } 
      @ctrl.saveAndSync resource
      # done
      @current = undefined


class ExemptionsEditor

  constructor: (@ctrl, @ExemptionRequest) ->

  begin: (task) ->
    unless @current?
      @current = { task: task }

  cancel: ->
    @current = undefined

  commit: ->
    if @current
      # decorate
      markAsResolved @current.task
      # resource stuff
      resource = new @ExemptionRequest 
        reason: @current.reason
        requirementId: @current.task.requirement.id
        registrationId: @ctrl.$window.registrationId
      @ctrl.saveAndSync resource
      # done
      @current = undefined


class ReviewsEditor

  constructor: (@ctrl, @Decision) ->

  open: (review) ->
    # just UI
    event.show = false for event in @ctrl.timeline
    review.target.show = true

  decide: (review, type, ok) ->
    if review
      # decorate
      review.resolved = true
      # resource stuff
      resource = new @Decision
      resource.decidable = { type: type, id: review.targetId }
      resource.ok = ok
      @ctrl.saveAndSync resource
      # unlink
      review.target.obj.review = undefined
      review.target = undefined

class RegistrationPageCtrl

  constructor: (@$http, @$window, Submission, Decision, ExemptionRequest) ->
    @lastError = null
    @timeline = []
    @reviews = []
    @tasks = []
    @editor =
      submissions: new SubmissionsEditor(this, Submission)
      exemptions: new ExemptionsEditor(this, ExemptionRequest)
      reviews: new ReviewsEditor(this, Decision)
    @sync(false)

  failure: (error) => 
    @lastError = error

  sync: (incremental=true) =>
    if @$http
      promise = @$http.get "#{@$window.registrationId}.json"
      success = (results) => @initialize results.data, incremental
      promise.then success, @failure

  initialize: (initializationBlob, incremental) ->
    
    @person = initializationBlob.person
    @position = initializationBlob.position
    @status = initializationBlob.status

    for event in initializationBlob.timeline.reverse()
      alreadyThere = _.any @timeline, (e) -> e.when is event.when and e.type is event.type
      unless alreadyThere
        if event.type is 'submission'
          event.obj.entries = @submissionEntries event.obj.submittable, event.obj
        event.isReviewerEvent = (event.type is 'decision')
        event.attentionSeeking = true if incremental
        @timeline.unshift event

    for review in initializationBlob.reviews
      alreadyThere = _.any @reviews, (e) -> e.targetId is review.targetId
      unless alreadyThere
        targetEvent = _.find @timeline, (e) -> e.obj.id is review.targetId
        review.isExemption = review.problem is 'pending_exemption_request_decision'
        if targetEvent
          review.target = targetEvent
          targetEvent.obj.review = review
        review.attentionSeeking = true if incremental
        @reviews.push review

    for task in initializationBlob.tasks
      alreadyThere = _.any @tasks, (e) -> e.requirement.id is task.requirement.id and e.resolved isnt true
      unless alreadyThere
        task.label = taskLabels[task.requirement.when]
        task.attentionSeeking = true if incremental
        @tasks.push task

    unless incremental
      @tasks = _.sortBy @tasks, (t) -> [t.label.sortIndex, t.submittable.name]

    @ready = true

  submissionEntries: (submittable, obj) ->
    entries = []
    for field in ['number', 'issuedBy', 'issueDate', 'expiryDate']
      if submittable[field]
        entry = 
          property: field
          name: submittable["#{field}As"]
          type: if field in ['issueDate', 'expiryDate'] then 'date' else 'text'
        entry.value = obj[field] if obj
        entries.push entry
    entries

  saveAndSync: (resource) ->
    resource.save().then @sync, @failure



module.exports.RegistrationPageCtrl = RegistrationPageCtrl
