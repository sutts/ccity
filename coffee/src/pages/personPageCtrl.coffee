_ = require 'lodash'

class NewRegistrationHelper

  constructor: (@ctrl) ->
    @positions = []
    registeredPositions = @ctrl.registrations.map (r) -> r.position.name
    for position in @ctrl.positions
      @positions.push position unless position.name in registeredPositions

  positionChanged: ->
    @busy = true
    @items = undefined
    promise = @ctrl.$http.get "/registrations/submittables/position/#{@position.id}"
    promise.then @success, @error 

  success: (results) =>
    results = results.data
    @refresh results
    @items = _.sortBy results, (e) -> e.submittable?.name
    @busy = false

  error: (err) =>
    @ctrl.lastError = err
    @busy = false

  refresh: (items) ->
    for item in items
      item.submittable = _.find @ctrl.submittables, (e) -> e.id is item.target.id

class PersonPageCtrl

  constructor: (@$http, @$window, @Submission) ->
    @lastError = null
    @sync()

  failure: (error) => 
    @lastError = error

  sync: () =>
    if @$http
      promise = @$http.get "#{@$window.personId}.json"
      success = (results) => @initialize results.data
      promise.then success, @failure

  initialize: (blob) ->
    @person = blob.person
    @submissions = @person.submissions
    @registrations = _.sortBy @person.registrations, (e) -> e.position.name
    @positions = _.sortBy blob.positions, 'name'
    @submittables = _.sortBy blob.submittables, 'name'
    @newRegistrationHelper = new NewRegistrationHelper this
    @newSubmission = new @Submission
    @ready = true

  submissionEntries: (submittable, obj) ->
    entries = []
    for field in ['number', 'issuedBy', 'issueDate', 'expiryDate']
      if submittable[field]
        entry = 
          property: field
          name: submittable["#{field}As"]
          type: if field in ['issueDate', 'expiryDate'] then 'date' else 'text'
        entry.value = obj[field] if obj
        entries.push entry
    entries

  beginSubmission: ->
    @newSubmission.entries = @submissionEntries @newSubmission.submittable

  commit: ->
    resource = @newSubmission
    resource[entry.property] = entry.value for entry in resource.entries
    resource.person = { id: @$window.personId }
    @saveAndSync resource
    @newSubmission = new @Submission

  cancel: ->
    @newSubmission = new @Submission

  saveAndSync: (resource) ->
    resource.save().then @sync, @failure


module.exports.PersonPageCtrl = PersonPageCtrl
