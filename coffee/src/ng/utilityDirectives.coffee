_ = require 'lodash'
{Expression} = require './expression.coffee'

app.directive 'onChange', ->
  require: 'ngModel'
  restrict: 'A'
  scope: true
  link: (scope, elm, attrs) ->
    scope.$watch attrs.ngModel, (newValue, oldValue) ->
      new Expression(attrs.onChange).set scope, newValue

app.directive 'equals', -> # used for confirmation field in change password dialog 
  restrict: 'A'
  require: '?ngModel'
  link: (scope, elem, attrs, ngModel) ->
    validate = =>
      val1 = ngModel.$viewValue
      val2 = attrs.equals
      ngModel.$setValidity 'equals', val1 is val2
    scope.$watch attrs.ngModel, => validate()
    attrs.$observe 'equals', (val) => validate()

app.directive 'relDate', -> 
  restrict: 'E'
  template: '<span title={{absolute}}>{{relative}}</span>'
  scope:
    date: '='
  link: (scope, elem, attrs) ->
    refresh = ->
      date = if _.isDate(scope.date) then scope.date else new Date(scope.date)
      relative = moment(date).fromNow()
      relative = relative[0].toUpperCase() + relative[1..-1] if attrs.capitalize
      scope.relative = relative 
      scope.absolute = "#{date.toDateString()} #{date.toLocaleTimeString()}"
    refresh()
    scope.$watch 'date', refresh

app.directive 'attachmentInfo', ->
  restrict: 'E'
  scope: true
  link: (scope, elem, attrs) ->
    scope.attachment.url = "#{attrs.baseUrl}/#{scope.attachment.s3Name}"
    scope.attachment.googleDocsUrl = "https://drive.google.com/viewerng/viewer?url=#{scope.attachment.url}&embedded=true"
    if /image\/*/.test scope.attachment.type
      scope.attachment.image = true
    else if scope.attachment.type is 'application/pdf'
      scope.attachment.pdf = true

