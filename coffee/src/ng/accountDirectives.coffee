app.directive 'changePasswordCtrl', ['Password', '$log', (Password, $log) ->
  restrict: 'A'
  scope: true
  require: '^form'
  link: (scope, el, attrs, formCtrl) ->
    scope.password = new Password
    scope.submit = ->
      scope.error = undefined
      scope.success = undefined
      scope.$broadcast 'show-errors-check-validity'
      unless formCtrl.$invalid
        success = (results) => 
          scope.password = new Password
          scope.$broadcast 'show-errors-reset'
          scope.success = true
        failure = (error) => 
          if error.status is 412 
            formCtrl['old'].$setValidity 'correct', false
          else
            $log.error error
            scope.error = error
        promise = scope.password.update()
        promise.then success, failure
    scope.cancel = ->
      scope.password = new Password
      scope.$broadcast 'show-errors-reset'
    scope.$watch 'password.old', => formCtrl['old'].$setValidity 'correct', true
    scope.$watchCollection 'password', => scope.error = undefined
]

app.directive 'changePersonalDetailsCtrl', ['Account', '$log', (Account, $log) ->
  restrict: 'A'
  scope: true
  require: '^form'
  link: (scope, el, attrs, formCtrl) ->
    scope.account.snapshot()
    scope.submit = ->
      scope.error = undefined
      scope.success = undefined
      scope.$broadcast 'show-errors-check-validity'
      unless formCtrl.$invalid
        success = (results) => 
          scope.$broadcast 'show-errors-reset'
          scope.success = true
        failure = (error) => 
          if error.status is 412 
            formCtrl['email'].$setValidity 'unique', false
          else
            @log.error error
            scope.error = error
        promise = scope.account.$put 'account'
        promise.then success, failure
    scope.cancel = ->
      scope.account.rollback()
      scope.account.snapshot()
    scope.$watch 'account.email', => formCtrl['email'].$setValidity 'unique', true
    scope.$watchCollection 'account', => scope.error = undefined
]

