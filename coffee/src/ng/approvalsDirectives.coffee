interpret = (item) ->
  obj = (glyph, style, text) -> {glyph: glyph, style: style, text: text}
  decision = if item.decisions then item.decisions[0] else item.decision
  if decision?.ok is true
    obj 'fa fa-thumbs-o-up', 'ok', "Accepted by #{decision.by} #{moment(decision.createdAt).fromNow()}"
  else if decision?.ok is false
    obj 'fa fa-thumbs-o-down', 'not-ok', "Rejected by #{decision.by} #{moment(decision.createdAt).fromNow()}"
  else
    {}

app.directive 'decidable', ->
  restrict: 'A'
  scope:
    item: '=decidable'
  link: (scope, elm, attrs) ->
    refresh = ->
      scope.item.display = interpret scope.item
    refresh()
    scope.$watch 'item.decision', refresh
    scope.$watchCollection 'item.decisions', refresh

app.directive 'decisionGlyph', ->
  restrict: 'E'
  replace: true
  template: "<span class='{{display.glyph}} {{display.style}} {{display.pullRight}}' title='{{display.text}}'></span>"
  scope:
    item: '='
    pullRight: '='
  link: (scope, elm, attrs) ->
    refresh = ->
      scope.display = interpret scope.item
      scope.display.pullRight = 'pull-right' unless scope.pullRight is false
    refresh()
    scope.$watch 'item.decision', refresh
    scope.$watchCollection 'item.decisions', refresh

app.directive 'registrationGlyph', ->
  restrict: 'E'
  replace: true
  template: "<span class='fa {{glyph}} {{style}} {{position}}'></span>"
  scope:
    registration: '='
    pullRight: '='
  link: (scope, elm, attrs) ->
    styleMap = 
      green: 'ok'
      red: 'not-ok'
      amber: 'amber'
    refresh = ->
      if scope.registration?.status
        scope.glyph = if scope.registration.status.pending then 'fa-circle-o' else 'fa-circle'
        scope.style = styleMap[scope.registration.status.color]
      else
        scope.glyph = ''
        scope.style = ''
      scope.position = 'pull-right' unless scope.pullRight is false
    refresh()
    scope.$watch 'registration.status', refresh
