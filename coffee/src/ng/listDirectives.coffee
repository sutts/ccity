_ = require 'lodash'

app.directive 'listCountHelper', ->
  restrict: 'A'
  replace: true
  template: '<span> ({{countText}})<small>{{paging}}</small></span>'
  scope:
    list: '='
  link: ($scope, elm, attrs) ->
    list = $scope.list
    startIndex = -> (list.view.page - 1) * list.view.pagesize + 1
    endIndex = -> startIndex() + list.view.items.length - 1
    refresh = ->
      if list.items.length is list.view.availableItems.length
        $scope.countText = list.items.length
      else
        $scope.countText = "#{list.view.availableItems.length} of #{list.items.length}"
      if list.view.pages > 1
        $scope.paging = " Showing items #{startIndex()}-#{endIndex()}"
      else
        $scope.paging = ''

    list.on 'viewChanged', refresh
    list.on 'pageChanged', refresh
    refresh()

app.directive 'unique', ->
  restrict: 'A'
  require: 'ngModel'
  link: (scope, elm, attrs, ctrl) ->
    attrValue = attrs['unique']
    tokens = attrValue?.split ' '
    unless tokens?.length is 3 and tokens[1] is 'in'
      throw new Error "Bad value for 'unique' directive: need something like '' but got '#{value}'"
    propertyNameToken = tokens[0]
    collectionPathToken = tokens[2]
    ctrl.$parsers.unshift (viewValue) ->
      collection = scope.$eval collectionPathToken
      valid = viewValue not in _.pluck(collection, propertyNameToken)
      ctrl.$setValidity 'unique', valid
      viewValue

