_ = require 'lodash'

class SubmissionFieldsSummaryHelper

  # Todo: use submissionFields directive instead

  constructor: (@scope) ->
    submittable = scope.item.submittable 
    if submittable
      scope.fields = []
      for field in ['number', 'issuedBy', 'issueDate', 'expiryDate']
        if submittable[field]
          name = submittable["#{field}As"]
          value = submittable.submission[field]
          value = new Date(value).toDateString() if value and field in ['issueDate', 'expiryDate']
          scope.fields.push {name: name, value: value}


class RolesHelper

  constructor: (@scope) ->
    @[role] = true for role in @scope.roles

instantiate = (name, scope, elem, attrs) ->
  map = 
    SubmissionFieldsSummaryHelper: SubmissionFieldsSummaryHelper
    RolesHelper: RolesHelper
  throw new Error "Unknown helper '#{name}'" unless map[name]
  new map[name] scope, elem, attrs 

app.directive 'helper', ->
  scope: true
  link: (scope, elem, attrs) ->
    tokens = attrs.helper?.split ' '
    unless tokens.length is 3 and tokens[1] is 'as'
      throw new Error "Expected 'HelperName as varname' but got '#{attrs.helper}'"
    helper = instantiate tokens[0], scope, elem, attrs
    scope[tokens[2]] = helper
