_ = require 'lodash'

class ResolvedExpression

  constructor: (@instance, @attributeName) ->

  isFunction: -> _.isFunction @instance[@attributeName]

  get: ->
    if @isFunction()
      @instance[@attributeName]()
    else
      @instance[@attributeName]

  set: (value) ->
    if @isFunction()
      @instance[@attributeName](value)
    else
      @instance[@attributeName] = value

class Expression

  constructor: (expressionString) ->
    throw new Error "Empty expression" unless expressionString?
    tokens = expressionString.split '.'
    @targetPath = tokens[0...tokens.length-1].join '.' if tokens.length > 1
    @targetProperty = _.last tokens

  resolve: (scope) ->
    if @targetPath?
      new ResolvedExpression scope.$eval(@targetPath), @targetProperty
    else
      new ResolvedExpression scope, @targetProperty

  get: (scope) ->
    @resolve(scope).get()

  set: (scope, value) ->
    @resolve(scope).set(value)


module.exports.Expression = Expression

