_ = require 'lodash'

class FilterWrapper

  constructor: (obj, type, list) ->
    @obj = obj
    @type = type
    @list = list

  isNullFilter: -> @obj is undefined
  isExclude:    -> @obj?.mode is 'exclude'
  hasTag:       -> @obj?.tags?.length > 0
  hasRef:       -> @obj?.references?.length > 0
  isTag:        -> @hasTag()

  rangeDescriptor:  ->
    if @hasTag()
      if @isExclude()
        "All #{@type.toLowerCase()}s not tagged"
      else
        "All #{@type.toLowerCase()}s tagged"
    else if @hasRef()
      if @isExclude()
        "All #{@type.toLowerCase()}s except"
      else
        "Specific #{@type.toLowerCase()}"
    else
      "All #{@type.toLowerCase()}s"

  referenceDescriptor:  ->
    if @hasTag()
      @obj.tags[0]
    else if @hasRef()
      id = parseInt(@obj.references[0].id)
      if id?
        refItem = @list.find id
        if refItem?.name? then refItem.name else "Unresolved reference '#{id}'"
      else
        "Bad reference (no id)"
    else
      undefined

  isValid: ->
    @isNullFilter() or @hasTag() or @hasRef()


class RequirementFilterChooser

  constructor: (@list, @type) ->
    throw new Error "RequirementFilterChooser really wants a page and a type" unless @list? and @type?
    @modes = [
      new FilterWrapper undefined, @type
      new FilterWrapper { mode: 'include', references: [{id: 999}]   }, @type
      new FilterWrapper { mode: 'include', tags:       ['X']         }, @type
      new FilterWrapper { mode: 'exclude', references: [{id: 999}]   }, @type
      new FilterWrapper { mode: 'exclude', tags:       ['X']         }, @type
    ]
    @selectMode '0'

  modesAsSelect2Options: ->
    @modes.map (mode,index) -> {id: "#{index}", text: mode.rangeDescriptor()} 

  selectMode: (index) ->
    @currentMode = @modes[parseInt(index)]
    @currentItem = undefined

  selectedMode: ->
    "#{@modes.indexOf(@currentMode)}"

  itemsAsSelect2Options: ->
    if @currentMode.isNullFilter()
      throw new Error "IllegalState (no options cos not filtering)"
    else if @currentMode.isTag()
      @list.tags.items.map (tag) -> {id: tag.name, text: tag.name}
    else
      list = @list.items.map (item) -> {id: "#{item.id}", text: item.name}
      _.sortBy list, 'text'

  selectItem: (item) ->
    throw new Error "Illegal State (can't set an item when filtering is switched off)" if @currentMode.isNullFilter()
    @currentItem = if item is '' then undefined else item

  selectedItem: () ->
    if @currentItem? then "#{@currentItem}" else @currentItem

  toFilter: ->
    unless @currentMode.isNullFilter() 
      obj = 
        type: @type
        mode: @currentMode.obj.mode
        references: []
        tags: []
      if @currentItem?
        if @currentMode.isTag()
          obj.tags.push @currentItem
        else
          obj.references.push { type: @type, id: parseInt(@currentItem) }
      obj

  fromFilter: (filter) ->
    wrapper = new FilterWrapper filter
    for mode,index in @modes
      if mode.hasTag() is wrapper.hasTag() and mode.hasRef() is wrapper.hasRef() and mode.isExclude() is wrapper.isExclude()
        @selectMode index
        unless index is 0
          if mode.isTag()
            @selectItem filter.tags[0]
          else
            @selectItem filter.references[0].id
        return
    console.log "Whoops, unable to find a matching mode for ", filter

  isValid: -> 
    new FilterWrapper(@toFilter()).isValid()

module.exports = 
  FilterWrapper: FilterWrapper
  RequirementFilterChooser: RequirementFilterChooser
