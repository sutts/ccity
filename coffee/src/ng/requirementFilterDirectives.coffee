_ = require 'lodash'
{FilterWrapper,RequirementFilterChooser} = require './requirementFilters.coffee'

app.directive 'requirementFilter', ->
  require: 'ngModel'
  restrict: 'E'
  scope:
    type: '@'
    list: '='
  template: -> # our isolated scope variables only get picked if we declare this empty function????
  link: (scope, elm, attrs, ngModel) ->
    refresh = ->
      filter = _.find ngModel.$modelValue, (e) -> e.type is scope.type
      wrapper = new FilterWrapper(filter, scope.type, scope.list)
      scope.filterRangeDescriptor = "#{wrapper.rangeDescriptor()}"
      scope.filterReferenceDescriptor = wrapper.referenceDescriptor()
    refresh()
    scope.$watch refresh

app.directive 'requirementFilterChooser', ->
  require: 'ngModel'
  restrict: 'E'
  scope: 
    requirement: '='
    type: '@'
    list: '='
  
  link: (scope, elm, attrs, ngModel) ->
    # ensure resource object structure if necessary
    scope.requirement.filters ||= [] 
    filter = _.find scope.requirement.filters, (e) -> e.type is scope.type
    # my helper
    chooser = new RequirementFilterChooser scope.list, scope.type
    # update model to reflect current widget state
    chooser.fromFilter filter
    # find my widgets    
    widgets = elm.find 'input'
    raise "Illegal state (requirement scope selector expecting 2 widgets but got #{widgets.length})" unless widgets.length is 2
    widget1 = widgets[0]
    widget2 = widgets[1]
    # initialize widget1
    $(widget1).select2 { data: chooser.modesAsSelect2Options() }
    $(widget1).select2 'val', chooser.selectedMode()

    populateSecondLevelOptions = ->
      if chooser.currentMode.isNullFilter() 
        $(widget2).select2('container').hide()
      else
        type = if chooser.currentMode.isTag() then 'tag' else scope.type.toLowerCase()
        $(widget2).select2 { data: chooser.itemsAsSelect2Options(), placeholder: "Please select a #{type}" }
        $(widget2).select2 'val', ''
        $(widget2).select2('container').show()

    populateSecondLevelOptions()
    $(widget2).select2 'val', chooser.selectedItem() || ''

    refresh = =>
      newFilter = chooser.toFilter()
      _.remove scope.requirement.filters, (e) -> e.type is scope.type
      scope.requirement.filters.push newFilter if newFilter?
      ngModel.$setViewValue newFilter
      ngModel.$setValidity 'filter', chooser.isValid()
      scope.$apply()

    $(widget1).on 'change', =>
      chooser.selectMode $(widget1).select2("val")
      populateSecondLevelOptions()
      refresh()
    
    $(widget2).on 'change', ->
      chooser.selectItem $(widget2).select2("val")
      refresh()

