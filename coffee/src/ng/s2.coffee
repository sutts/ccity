_ = require 'lodash'

s2 = angular.module 's2', []

# Select2 in tag mode; messier than might be expected because both model and widget work with an 
# an array of strings but the angular $viewValue bit in the middle doesn't like arrays. Hence lots of 
# converting back and forth along the way
s2.directive 's2Tags', [ '$log', ($log) ->  
  require: 'ngModel'
  restrict: 'A'
  link: (scope, elm, attrs, ngModel) ->
    SPLITTER = ',,,'
    ngModel.$parsers.push (value) -> 
      $log.debug 's2 model parser >> parser', value
      if _.isString(value) then value.split(SPLITTER) else value
    ngModel.$formatters.push (value) -> 
      $log.debug 's2 model formatter >>', value
      if _.isArray(value) then value.join(SPLITTER) else value
    tagListFn = -> scope.$eval attrs.s2Tags
    $(elm).select2
      tags: tagListFn
    $(elm).on 'change', =>
      value = $(elm).select2 'val'
      $log.debug "s2 widget value changed to ", value 
      ngModel.$setViewValue value.join(SPLITTER)
      scope.$apply()
    scope.$watch attrs.ngModel, =>
      $log.debug "s2Tags model $watch says changed to ", ngModel.$viewValue
      $(elm).select2 'val', if ngModel.$viewValue then ngModel.$viewValue.split(SPLITTER) else []
]

# Same as above but single rather than multi select
s2.directive 's2Tag', [ '$log', ($log) ->   
  require: 'ngModel'
  restrict: 'A'
  link: (scope, elm, attrs, ngModel) ->
    tagListFn = -> scope.$eval attrs.s2Tag
    $(elm).select2
      tags: tagListFn
      maximumSelectionSize: 1
    $(elm).on 'change', =>
      value = $(elm).select2 'val'
      ngModel.$setViewValue value[0]
      scope.$apply()
    scope.$watch attrs.ngModel, =>
      $log.debug "s2Tag model $watch says changed to ", ngModel.$viewValue
      $(elm).select2 'val', if ngModel.$viewValue then [ngModel.$viewValue] else []
]

# Turns a bog-standard select into a select2
# Can optionally be used alongside ngModel in which case view-to-model works out of the box but 
# model to view needs a nudge: basically, given a $watch model change, code picks an attribute of 
# the view value (which is assumed to be an object) and pipes this through to select2 widget as current 
# selection. This behaviour only applies if an attribute name is passed in via the s2 attribute value
s2.directive 's2', [ '$log', ($log) ->   
  require: '?ngModel'
  restrict: 'A'
  link: (scope, elm, attrs, ngModel) ->
    options = {}
    options.allowClear = true if attrs.allowClear
    options.placeholder = attrs.placeholder if attrs.placeholder
    $(elm).select2 options
    if attrs.s2
      scope.$watch attrs.ngModel, =>
        $log.debug "s2 model $watch says changed to ", ngModel.$viewValue
        $(elm).select2 'val', if ngModel.$viewValue then ngModel.$viewValue[attrs.s2] else ''
]
