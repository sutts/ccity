# https://github.com/paulyoder/angular-bootstrap-show-errors/blob/master/src/showErrors.coffee

showErrorsModule = angular.module('ui.bootstrap.showErrors', [])

app.directive 'showErrors', ['$timeout', ($timeout) ->

    getShowSuccess = (options) ->
      showSuccess = false
      if options && options.showSuccess?
        showSuccess = options.showSuccess
      showSuccess

    linkFn = (scope, el, attrs, formCtrl) ->
      blurred = false
      options = scope.$eval attrs.showErrors
      showSuccess = getShowSuccess options

      inputEl   = el[0].querySelector("[name]")
      inputNgEl = angular.element(inputEl)
      inputName = inputNgEl.attr('name')
      unless inputName
        throw "show-errors element has no child input elements with a 'name' attribute"

      triggerEvent = attrs.showErrors or 'blur'
      inputNgEl.bind triggerEvent, ->
        blurred = true
        toggleClasses formCtrl[inputName].$invalid

      scope.$watch ->
        formCtrl[inputName] && formCtrl[inputName].$invalid
      , (invalid) ->
        return if !blurred
        toggleClasses invalid

      scope.$on 'show-errors-check-validity', ->
        toggleClasses formCtrl[inputName].$invalid

      reset = =>
        el.removeClass 'has-error'
        el.removeClass 'has-success'
        blurred = false
      scope.$on 'show-errors-reset', reset
      scope.$on 'show-errors-reset-later', -> $timeout reset, 0, false

      toggleClasses = (invalid) ->
        el.toggleClass 'has-error', invalid
        if showSuccess
          el.toggleClass 'has-success', !invalid

    {
      restrict: 'A'
      require: '^form'
      compile: (elem, attrs) ->
        unless elem.hasClass 'form-group'
          throw "show-errors element does not have the 'form-group' class"
        linkFn
    }
]

showErrorsModule.provider 'showErrorsConfig', ->
  _showSuccess = false

  @showSuccess = (showSuccess) ->
    _showSuccess = showSuccess

  @$get = ->
    showSuccess: _showSuccess

  return