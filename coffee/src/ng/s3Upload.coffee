_ = require 'lodash'

randomString = (length) ->
  possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
  chars = [1..length].map -> possible.charAt(Math.floor(Math.random() * possible.length))
  chars.join ''

app.directive 's3Upload', ['$upload', '$log', ($upload, $log) ->

  require: 'ngModel'
  restrict: 'E'

  link: (scope, elm, attrs, ctrl) ->

    # sets validity of 'uploaded' to true if there are no files still uploading
    checkUploads = ->
      stillUploading = _.some ctrl.$viewValue, (e) -> e.uploaded is false
      ctrl.$setValidity 'uploaded', not(stillUploading)
      _.defer -> scope.$apply

    # updates file set and marks 'notEmpty' as valid if there is at least one file
    set = (attachments) ->
      ctrl.$setViewValue attachments
      ctrl.$setValidity 'notEmpty', attachments.length > 0
      checkUploads()

    # async upload
    upload = (attachment) ->
      uploadInfo = 
        url: attrs.url, 
        method: 'POST'
        data: 
          key: "uploads/#{attachment.s3Name}"
          AWSAccessKeyId: attrs.key
          acl: 'public-read'
          policy: attrs.policy
          signature: attrs.signature
          "Content-Type": attachment.type
        file: attachment.file
      attachment.promise = $upload.upload(uploadInfo)
      attachment.promise.progress (evt) -> 
        attachment.progress = Math.ceil(evt.loaded * 100 / evt.total)
      attachment.promise.success -> 
        attachment.uploaded = true
        attachment.file = undefined
        checkUploads()
      attachment.promise.error (err) -> 
        attachment.error = err
        $log.error err
        scope.ctrl?.lastError = err

    # new selection event (merge selected files into existing selection)
    scope.onFileSelect = (selectedFiles) ->
      # make sure not tooooo many files (on this selection)
      if selectedFiles.length > 5
        ctrl.$setValidity 'tooMany', false
        return
      # make sure not tooooo much data (on this selection)
      size = selectedFiles.map( (e) -> e.size ).reduce (x,y) -> x + y
      if size > (5 * 1024 * 1024)
        ctrl.$setValidity 'tooMuch', false
        return
      # enough validation already
      ctrl.$setValidity 'tooMany', true
      ctrl.$setValidity 'tooMuch', true
      # helper to extract file extension
      fileExtension = (file) ->
        tokens = file.name.split '.'
        if tokens.length is 0 then '' else ".#{_.last(tokens)}"
      # okay, let's merge new selection with existing 
      tmp = ctrl.$viewValue.map (e) -> e # safe clone
      for file in selectedFiles
        existing = _.find tmp, (a) -> a.file.name is file.name
        unless existing
          attachment = 
            originalName: file.name
            s3Name: randomString(64) + fileExtension(file)
            file: file
            type: file.type or 'application/octet-stream'
            size: file.size
            uploaded: false
          upload attachment
          tmp.push attachment
      set tmp

    # function to remove
    scope.removeAttachment = (attachment) ->
      attachment.promise.abort() if attachment.promise and not attachment.uploaded
      set _.without(ctrl.$viewValue, attachment)

    # initial state
    set []

]
