{BaseController} = require '../../src/lists/baseController.coffee'

chai = require 'chai'
chai.should()

describe "BaseController", ->

  ctrl = undefined

  beforeEach ->
    ctrl = new BaseController

  describe "eventing", ->

    it "should be possible to register listeners and trigger events", (done) ->
      ctrl.on 'hello', ->
        done()
      ctrl.broadcast 'hello'

    it "should be possible to register for multiple events in one go", (done) ->
      ctrl.on ['hello', 'world'], ->
        done()
      ctrl.broadcast 'world'

    it "should have special treatment for broadcast 'error', namely to remember it", (done) ->
      ctrl.on 'error', ->
        ctrl.lastError.should.equal 'Shit happens'
        done()
      ctrl.broadcast 'error', 'Shit happens'

