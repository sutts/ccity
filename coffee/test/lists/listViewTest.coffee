{ListController} = require '../../src/lists/listController.coffee'
{ListView} = require '../../src/lists/listView.coffee'

chai = require 'chai'
chai.should()

createFakeItems = (howMany) -> [1..howMany].map (i) -> { id: i, name: "Item #{i}" }
eventually = (fn, args...) -> setTimeout fn, 20, args...

PAGE_SIZE_OF_20 = 20  
  
describe "ListView", ->

  ctrl = view = undefined

  describe "while the controller is not ready", ->

    it "should bide its time", ->
      ctrl = new ListController
      view = new ListView ctrl
      view.availableItems.length.should.equal 0

  describe "available items", ->

    beforeEach ->
      ctrl = new ListController
      view = new ListView ctrl

    describe "with no filters", ->

      it "should include all items from its list controller", ->
        ctrl.initialize createFakeItems(3)
        view.availableItems.length.should.equal 3

    describe "with one filter", ->

      it "should only include items that are accepted by that filter", ->
        view.addFilter { accept: (item) -> item.name is 'Item 2' }
        ctrl.initialize createFakeItems(3)
        view.availableItems.length.should.equal 1

    describe "with multiple filters", ->

      it "should only include items that are accepted by all its filters ", ->
        view.addFilter { accept: (item) -> true  }
        view.addFilter { accept: (item) -> false }
        ctrl.initialize createFakeItems(3)
        view.availableItems.length.should.equal 0

  describe "pagination with no filters (default 50 records per page, here set to 20)", ->

    beforeEach ->
      ctrl = new ListController
      view = new ListView ctrl, PAGE_SIZE_OF_20
      ctrl.initialize createFakeItems(41)

    it "should have 3 pages", ->
      view.pages.should.equal 3

    it "should start on page 1", ->
      view.page.should.equal 1

    it "should have the first 20 items", ->
      view.items.length.should.equal 20
      view.items[0].name.should.equal 'Item 1'
      view.items[19].name.should.equal 'Item 20'

    it "should be able to switch pages", ->

      view.setPage 2
      view.page.should.equal 2
      view.items.length.should.equal 20
      view.items[0].name.should.equal 'Item 21'
      view.items[19].name.should.equal 'Item 40'

      view.setPage 3
      view.page.should.equal 3
      view.items.length.should.equal 1
      view.items[0].name.should.equal 'Item 41'

    it "should ignore bad setPage instructions", ->
      view.setPage 5
      view.page.should.equal 1

  describe "pagination with filters", ->

    it "should take account of those filters", ->
      ctrl = new ListController
      view = new ListView ctrl, PAGE_SIZE_OF_20
      view.addFilter
        accept: (item) -> item.id <= 25
      ctrl.initialize createFakeItems(41)
      view.pages.should.equal 2

    it "should find its way to page zero if there are no matches", ->
      ctrl = new ListController
      view = new ListView ctrl, PAGE_SIZE_OF_20
      view.addFilter
        accept: (item) -> false
      ctrl.initialize createFakeItems(41)
      view.pages.should.equal 0
      view.page.should.equal 0

  describe "viewChanged event", ->

    it "should be broadcast (via the parent controller) after a load (or reload)", (done) ->
      ctrl = new ListController
      ctrl.initialize createFakeItems(41)
      ctrl.on 'viewChanged', ->
        done()
      view = new ListView ctrl, PAGE_SIZE_OF_20

    xit "should be broadcast (via the parent controller) whenever a filter value changes", (done) ->
      ctrl = new ListController
      view = new ListView ctrl, PAGE_SIZE_OF_20
      filter = { valueToReturn: true, accept: (item) -> @valueToReturn}
      view.addFilter filter
      ctrl.initialize createFakeItems(3)
      view.availableItems.length.should.equal 3

      filter.valueToReturn = false
      filter.onChange()
      eventually ->
        view.availableItems.length.should.equal 0
