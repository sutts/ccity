filters = require '../../src/lists/filters.coffee'

chai = require 'chai'
chai.should()

describe "List filters", ->

  describe "BaseFilter", ->

    it "should complain if asked to accept an item but no field names have been specified", ->

      filter = filters.createBaseFilter()
      ( -> filter.accept {} ).should.throw "Please specify at least one field name you bozo!"

    it "should accept anything (without asking subclasses) where no filter has been set", ->

      for filterValue in [undefined, null]
        filter = filters.createBaseFilter 'name'
        filter.compile = (value) -> throw new Error "Shouldn't even try to compile"
        filter.setValue filterValue
        filter.accept({}).should.equal true

    filter = undefined

    beforeEach ->
      filter = filters.createBaseFilter 'color'
      filter.setValue 'green'

    it "should tell registered listener if the filter value changes", (done) ->
      filter.onChange = -> done()
      filter.setValue 'abc'

    it "should do strict equality filtering", ->
      filter.accept({color: 'green'}).should.equal true
      filter.accept({color: 'Green'}).should.equal false
      filter.accept({fruit: 'apple'}).should.equal false

    it "should accept an item if there is at least one match", ->
      filter.fieldNames = ['color', 'fruit']
      filter.setValue 'apple'
      filter.accept({color: 'red', fruit: 'apple'}).should.equal true
      filter.accept({color: 'red', fruit: 'banana'}).should.equal false

    it "should not do any type manipulation", ->
      filter.fieldNames = ['id']
      filter.withValue(1).accept({id: 1}).should.equal true
      filter.withValue(1).accept({id: '1'}).should.equal false
      filter.withValue('1').accept({id: 1}).should.equal false

  describe "NumericFilter", ->

    it "should massage filter values to be numeric", ->
      filter = filters.createNumericFilter 'id'
      filter.withValue(1).accept({id: 1}).should.equal true
      filter.withValue('1').accept({id: 1}).should.equal true
      filter.withValue(1).accept({id: '1'}).should.equal false

  describe "PartialMatchFilter", ->

    it "should accept (case insensitive) partial matches on one or more property values", ->

      filter = filters.createPartialMatchFilter 'color', 'fruit'
      item =
        color: 'green'
        fruit: 'apple'

      filter.withValue('Z').accept(item).should.equal false
      for value in ['e', 'E', 'P', 'green', 'APPLE']
        filter.withValue(value).accept(item).should.equal true

  describe "RovingPartialMatchFilter", ->

    filter = undefined

    beforeEach ->
      filter = filters.createRovingPartialMatchFilter()

    it "should do its thing without needing to be told which fields to search", ->

      item =
        color: 'green'
        fruit: 'apple'

      filter.withValue('Z').accept(item).should.equal false
      for value in ['e', 'E', 'P', 'green', 'APPLE']
        filter.withValue(value).accept(item).should.equal true

    it "should also work for array values", ->
      item =
        colors: ['green', 'red']
      filter.withValue('E').accept(item).should.equal true
      filter.withValue('Z').accept(item).should.equal false

    it "should also work for nested objects", ->
      item =
        property: 
          nested: 
            again: ['green', 'red']
      filter.withValue('E').accept(item).should.equal true
      filter.withValue('Z').accept(item).should.equal false

    it "should be possible to control search depth across nested objects", ->
      item =
        level1: 
          level2: 
            level3: ['green', 'red']
      filters.createRovingPartialMatchFilter(2).withValue('E').accept(item).should.equal false
      filters.createRovingPartialMatchFilter(3).withValue('E').accept(item).should.equal true

