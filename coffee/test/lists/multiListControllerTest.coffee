{MultiListController} = require '../../src/lists/multiListController.coffee'
{ListController} = require '../../src/lists/listController.coffee'

chai = require 'chai'
chai.should()

describe "MultiListController", ->

  multi = undefined

  beforeEach ->
    multi = new MultiListController

  describe "ready", ->

    beforeEach ->
      multi.withList new ListController, 'list1', 'Type1'
      multi.withList new ListController, 'list2', 'Type2'

    it "should start out not ready", ->
      multi.ready.should.equal false

    it "should become ready when all its members are ready", ->
      multi.list1.initialize []
      multi.ready.should.equal false
      multi.list2.initialize []
      multi.ready.should.equal true

    it "should broadcast ready when it becomes ready", (done) ->
      multi.on 'ready', ->
        done()
      multi.list1.initialize []
      multi.list2.initialize []

  describe "withList", ->

    ctrl1 = ctrl2 = undefined

    beforeEach ->
      ctrl1 = new ListController
      ctrl2 = new ListController
      multi.withList ctrl1, 'list1', 'Type1'
      multi.withList ctrl2, 'list2', 'Type2'

    it "should provide access to it via the passed name", ->
      multi.list1.should.equal ctrl1
      multi.list2.should.equal ctrl2

    it "but complain bitterly if this leads to any conflicts", ->
      ctrl3 = new ListController
      ( -> multi.withList ctrl3, 'list1' ).should.throw Error

    it "should additionally make the first list available as 'list'", ->
      multi.list.should.equal ctrl1

    it "should additionally provide a lookup via the type name", ->
      multi.types['Type1'].should.equal ctrl1

    it "should listen for errors and propagate accordingly", (done) ->
      multi.on 'error', (err) ->
        err.should.equal 'shit happens'
        done()
      ctrl1.broadcast 'error', 'shit happens'

  describe "list reference resolution", ->

    red = green = blue = undefined

    beforeEach ->
      blue  = { id: 1, name: 'blue'  }
      green = { id: 2, name: 'green' }
      red   = { id: 3, name: 'red'   } 
      multi.withList new ListController, 'colors', 'Color'

    it "should be able to resolve simple references", -> 
      
      apple   = { name: 'apple',  color: { type: 'Color', id: 2 } }
      cherry  = { name: 'cherry', color: { type: 'Color', id: 3 } }

      multi.withList new ListController, 'fruits', 'Fruit', 'color'
      multi.fruits.initialize [apple, cherry] 
      multi.colors.initialize [red, green, blue]
      
      apple.$color.should.equal green
      cherry.$color.should.equal red

    it "should not explode if a reference cannot be resolved", ->

      apple   = { name: 'apple',  color: { type: 'Color', id: 27 } }

      multi.withList new ListController, 'fruits', 'Fruit', 'color'
      multi.fruits.initialize [apple] 
      multi.colors.initialize [red, green, blue]
      
      (apple.$color is undefined).should.equal true

    it "should be able to resolve array references", -> 
      
      tomato = { name: 'tomato', colors: [ { type: 'Color', id: 2 },
                                           { type: 'Color', id: 3 } ] }

      multi.withList new ListController, 'vegetables', 'Vegetable', 'colors'
      multi.vegetables.initialize [tomato] 
      multi.colors.initialize [red, green, blue]

      tomato.$colors[0].should.equal green
      tomato.$colors[1].should.equal red

    it "should be able to resolve nested references", -> 
      
      toy = { name: 'house', parts: [ { name: 'door', color: { type: 'Color', id: 2 } },
                                      { name: 'roof', color: { type: 'Color', id: 1 } } ] }

      multi.withList new ListController, 'toys', 'Toy', 'parts.color'
      multi.toys.initialize [toy] 
      multi.colors.initialize [red, green, blue]

      toy.parts[0].$color.should.equal green
      toy.parts[1].$color.should.equal blue

