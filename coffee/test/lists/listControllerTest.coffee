_ = require 'lodash'
{ListController} = require '../../src/lists/listController.coffee'

chai = require 'chai'
chai.should()

describe "ListController", ->

  ctrl = undefined

  beforeEach ->
    ctrl = new ListController

  describe "initialisation", ->

    it "should start out not ready", ->
      ctrl.ready.should.equal false

    it "should become ready once it has been initialised",(done) ->
      ctrl.on 'ready', ->
        ctrl.ready.should.equal true
        done()
      ctrl.initialize []

    it "should additionally notify listChanged (for listeners who aren't interested in fine grained detail)",(done) ->
      ctrl.on 'listChanged', -> done()
      ctrl.initialize []

    it "should treat initialization data as initial contents", (done) ->
      item = {id: 1, name: 'Item 1'}
      ctrl.on 'ready', ->
        ctrl.items.length.should.equal 1
        ctrl.items[0].should.equal item
        done()
      ctrl.initialize [item]

    it "should sort contents if a sort callback is provided", (done) ->
      item1 = {id: 1, name: 'Item 1'}
      item2 = {id: 2, name: 'Item 2'}
      ctrl.on 'ready', ->
        ctrl.items.should.deep.equal [item1, item2]
        done()
      ctrl.sortFn = (items) -> _.sortBy items, 'name'
      ctrl.initialize [item2, item1]

    it "should complain if initialized more than once", ->
      ctrl.initialize []
      (-> ctrl.initialize []).should.throw Error

  describe "when asked to add an item", ->

    beforeEach ->
      ctrl.initialize []

    it "should complain if the item is undefined", ->
      ( -> ctrl.add() ).should.throw Error

    it "should add it to the end of the list", ->
      ctrl.add {id: 1}
      ctrl.add {id: 2}
      ctrl.items.length.should.equal 2
      ctrl.items[1].id.should.equal 2

    it "should tell people that it added it", (done) ->
      ctrl.on 'itemAdded', (item) ->
        item.id.should.equal 1;
        done()
      ctrl.add {id: 1}

    it "should also broadcast generic listChanged", (done) ->
      ctrl.on 'listChanged',  ->
        done()
      ctrl.add {id: 1}

  describe "when asked to remove an item", ->

    beforeEach ->
      ctrl.initialize [ {id: 1}, {id: 2} ]

    it "should complain if the item to delete is not in its list", ->
      ( -> ctrl.remove {id: 3} ).should.throw Error

    it "should remove it", ->
      ctrl.remove ctrl.items[0]
      ctrl.items.length.should.equal 1
      ctrl.items[0].id.should.equal 2

    it "should tell people that it removed it", (done) ->
      ctrl.on 'itemDeleted', (item) ->
        item.id.should.equal 1;
        done()
      ctrl.remove ctrl.items[0]

    it "should also broadcast generic listChanged", (done) ->
      ctrl.on 'listChanged',  ->
        done()
      ctrl.remove ctrl.items[0]

  describe "when told that an item has changed", ->

    beforeEach ->
      ctrl.initialize [ {id: 1}, {id: 2} ]

    it "should complain if the item in question is not in its list", ->
      ( -> ctrl.updated {id: 3} ).should.throw Error

    it "should tell people that an item has changed", (done) ->
      ctrl.on 'itemUpdated', (item) ->
        item.id.should.equal 1;
        done()
      ctrl.updated ctrl.items[0]

    it "should also broadcast generic listChanged", (done) ->
      ctrl.on 'listChanged',  ->
        done()
      ctrl.updated ctrl.items[0]

  describe "finders", ->

    createFakeItems = (howMany) -> [1..howMany].map (i) -> { id: i, name: "Item #{i}" }

    beforeEach ->
      ctrl.initialize createFakeItems(3)

    describe "find", ->

      it "should return an item by id if it exists", ->
        ctrl.find(1).id.should.equal 1
        (ctrl.find(27) is undefined).should.equal true

    describe "findRef", ->

      it "should return an item if it exists", ->
        ctrl.findRef('i:1').id.should.equal 1
        (ctrl.findRef('i:27') is undefined).should.equal true

  describe "expansions", ->

    it "should provide a fancy way of declaring references and how they should be expanded", ->

      ctrl.references('fruits').through('fruitRef').andExpandsTo('fruit')
      ctrl.expansions.length.should.equal 1
      ctrl.expansions[0].should.deep.equal
        references: 'fruits'
        through: 'fruitRef'
        expandTo: 'fruit'





