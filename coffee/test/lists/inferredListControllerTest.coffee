{InferredListController} = require '../../src/lists/inferredListController.coffee'
{ListController} = require '../../src/lists/listController.coffee'

chai = require 'chai'
chai.should()

stringify = (array, propertyName) -> (array.map (item) -> item[propertyName]).join ', '
eventually = (fn, args...) -> setTimeout fn, 20, args...

createFakeItems = (howMany) -> [1..howMany].map (i) -> { id: i, name: "Item #{i}" }

describe "InferredListController", ->

  ctrl = undefined

  beforeEach ->
    ctrl = new ListController

  describe "initialization", ->

    beforeEach ->
      subCtrl = new InferredListController ctrl, 'name'

    it "should start out not ready", ->
      subCtrl = new InferredListController ctrl, 'name'
      subCtrl.ready.should.equal false

    it "should become ready after the parent controller is initialized", (done) ->
      subCtrl = new InferredListController ctrl, 'name'
      subCtrl.on 'ready', ->
        subCtrl.ready.should.equal true
        done()
      ctrl.initialize createFakeItems(3)

    it "should hook up errors to its parent controller", (done) ->
      ctrl.on 'error', (error) ->
        error.should.equal 'inferred problem'
        done()
      subCtrl = new InferredListController ctrl, 'name'
      ctrl.initialize createFakeItems(3)
      subCtrl.broadcast 'error', 'inferred problem'

  describe "items where values are inferred from an array type", ->

    colorsCtrl = blue = undefined

    beforeEach ->
      items = createFakeItems 4
      items[0].colors = ['red', 'blue']
      items[1].colors = ['blue', 'orange']
      items[2].colors = [undefined]
      items[3].colors = undefined
      colorsCtrl = new InferredListController ctrl, 'colors'
      ctrl.initialize items

    it "should be a sorted list of distinct items with no undefineds thank you very much", ->
      stringify(colorsCtrl.items, 'name').should.equal 'blue, orange, red'

    it "should also provide access to a simple array version of its item names for the convenience of arbitrary consumers", ->
      colorsCtrl.itemNames().join(', ').should.equal 'blue, orange, red'

    it "should also sort parent items in passing", ->
      ctrl.items[0].colors.join(', ').should.equal 'blue, red'

    it "should collect references", ->
      blue.references.length.should.equal 2
      # stringify(blue.references, 'name').should.equal 'Item1, Item2'

    it "should be refreshed whenever the parent controller notifies change", ->
      ctrl.items[2].colors = ['green']
      ctrl.updated ctrl.items[2]
      stringify(colorsCtrl.items, 'name').should.equal 'blue, green, orange, red'

    beforeEach ->
      blue = colorsCtrl.items[0]
      colorsCtrl.newResource = -> {}

    describe "and when we remove an item", ->

      it "should remove that aspect from all parent items", ->
        colorsCtrl.remove blue
        ctrl.items[0].colors.join(',').should.equal 'red'
        ctrl.items[1].colors.join(',').should.equal 'orange'

      it "should broadcast change same as any list controller", (done) ->
        colorsCtrl.on 'itemDeleted', (item) ->
          item.should.equal blue
          done()
        colorsCtrl.remove blue

      it "should create an appropriate resource instance for sync-ing (as and when asked to do so)", ->
        resource = colorsCtrl.prepareSync blue
        resource.id.should.equal 'blue'

    describe "and when we update an item", ->

      it "should update that aspect in all parent items", ->
        blue.name = 'blUE'
        colorsCtrl.updated blue
        ctrl.items[0].colors.join(',').should.equal 'blUE,red'
        ctrl.items[1].colors.join(',').should.equal 'blUE,orange'

      it "should broadcast change same as any list controller", (done) ->
        blue.name = 'blUE'
        colorsCtrl.on 'itemUpdated', (item) ->
          item.should.equal blue
          done()
        colorsCtrl.updated blue

      it "should create an appropriate resource instance for sync-ing (as and when asked to do so)", ->
        blue.name = 'blUE'
        resource = colorsCtrl.prepareSync blue
        resource.id.should.equal 'blue'
        resource.name.should.equal 'blUE'

    describe "snapshot and rollback", ->

      it "should provide a custom implementation given we're working with an inferred rather than real resource", ->
        colorsCtrl.snapshot blue
        blue.name = 'BLUE'
        colorsCtrl.rollback blue
        blue.name.should.equal 'blue'

  describe "items where values are inferred from a single value type", ->

    colorCtrl = blue = undefined

    beforeEach ->
      items = createFakeItems 3
      items[0].color = 'red'
      items[1].color = 'blue'
      items[2].color = undefined
      colorCtrl = new InferredListController ctrl, 'color'
      ctrl.initialize items

    it "should be a sorted list of distinct items with no undefineds thank you very much", ->
      stringify(colorCtrl.items, 'name').should.equal 'blue, red'

    describe "and when we try and remove an item", ->

      it "should complain (we don't do delete on single value property types)", ->
        ( -> colorCtrl.remove blue).should.throw Error

    describe "and when we update an item", ->

      it "should update that aspect in all parent items", ->
        blue = colorCtrl.items[0]
        blue.name = 'blUE'
        colorCtrl.updated blue
        ctrl.items[0].color.should.equal 'red'
        ctrl.items[1].color.should.equal 'blUE'

