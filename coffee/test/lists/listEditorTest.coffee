{ListController} = require '../../src/lists/listController.coffee'
{ListEditor} = require '../../src/lists/listEditor.coffee'

chai = require 'chai'
chai.should()

class FakeItem
  constructor: (@id, @name) ->
  snapshot: ->
    @backup = {id: @id, name: @name}
  rollback: ->
    @id = @backup.id
    @name = @backup.name  

createFakeItems = (howMany) -> [1..howMany].map (i) -> new FakeItem i, "Item #{i}" 

describe "ListEditor", ->

  ctrl = editor = undefined

  beforeEach ->
    ctrl = new ListController
    editor = new ListEditor ctrl
    # stuff usually performed by ListSync
    ctrl.newResource = -> { id: undefined, name: undefined }
    ctrl.broadcast 'syncInitialized'
    ctrl.initialize createFakeItems(3)

  describe "new item", ->

    it "should have a new fella ready and waiting", ->
      (editor.newItem.id is undefined).should.equal true
      (editor.newItem.name is undefined).should.equal true

    describe "create", ->

      it "should add new fella to the the list", ->
        editor.newItem.name = 'Item 27'
        editor.create()
        ctrl.items.length.should.equal 4
        ctrl.items[3].name.should.equal 'Item 27'

      it "should tell the controller that the item was added", (done) ->
        ctrl.on 'itemAdded', (item) ->
          item.name.should.equal 'New item'
          done()
        editor.newItem.name = 'New item'
        editor.create()

      it "should have a new new fella ready and waiting", ->
        editor.newItem.name = 'Item 27'
        editor.create()
        (editor.newItem.id is undefined).should.equal true
        (editor.newItem.name is undefined).should.equal true

  describe "edit and delete", ->

    item1 = item2 = undefined

    beforeEach ->
      item1 = ctrl.items[0]
      item2 = ctrl.items[1]

    it "should track the item currently under edit (without current edit)", ->
      (editor.current is undefined).should.equal true

    it "should track the item currently under edit (with current edit)", ->
      editor.begin item1
      editor.current.should.equal item1

    describe "abandoning an edit", ->

      beforeEach ->
        editor.rollbackProperties = ['name']
        editor.begin item1
        item1.name = "Item 1 renamed"

      it "should roll back changes when an edit is explicitly cancelled", ->
        editor.cancel()
        item1.name.should.equal 'Item 1'

      it "should roll back changes when an edit is implicitly abandoned", ->
        editor.begin item2
        item1.name.should.equal 'Item 1'

    describe "completing an edit", ->

      it "should tell the controller that the item was updated", (done) ->
        ctrl.on 'itemUpdated', (item) ->
          item.should.equal item1
          done()
        editor.begin item1
        editor.update()

      it "should reset the item under edit", ->
        editor.begin item1
        editor.update()
        (editor.current is undefined).should.equal true

    describe "deletion", ->

      it "should remove the item from the controller's list", ->
        editor.begin item1
        editor.delete()
        editor.controller.items.length.should.equal 2

      it "should tell the controller that the item was deleted", (done) ->
        ctrl.on 'itemDeleted', (item) ->
          item.should.equal item1
          done()
        editor.begin item1
        editor.delete()

      it "should reset the item under edit", ->
        editor.begin item1
        editor.delete()
        (editor.current is undefined).should.equal true

      it "should also be able to delete an item that is not the current item", ->
        editor.begin item1
        editor.delete item2
        editor.controller.items.length.should.equal 2
        editor.current.should.equal item1
