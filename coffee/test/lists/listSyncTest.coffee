{ListController} = require '../../src/lists/listController.coffee'
{ListSync} = require '../../src/lists/listSync.coffee'
{Deferred} = require '../helpers/promise.coffee'

chai = require 'chai'
chai.should()

class FakeResource
  i_am_a_fake: true

createGoodQueryPromise = (numInstancesToReturn) ->
  deferred = new Deferred
  instancesToReturn = [1..numInstancesToReturn].map (i) -> new FakeResource
  deferred.resolve instancesToReturn
  deferred.promise

createGoodPromise = ->
  deferred = new Deferred
  deferred.resolve
  deferred.promise

createBadPromise = (errorToReturn) ->
  deferred = new Deferred
  deferred.reject errorToReturn
  deferred.promise

eventually = (fn, args...) -> setTimeout fn, 20, args...

describe "ListSync", ->

  describe "construction", ->

    it "should complain if no resource is provided", ->
      (-> new ListSync).should.throw Error

  ctrl = sync = undefined

  beforeEach ->
    ctrl = new ListController
    sync = new ListSync FakeResource

  describe "initialization", ->

    it "should kindly ask its resource to do a fetch and use the results to initialize the controller", (done) ->
      ctrl.on 'ready', ->
        ctrl.items.length.should.equal 3
        done()
      FakeResource.query = -> createGoodQueryPromise 3
      sync.initialize ctrl

    it "should pass on errors where suchlike happens", (done) ->
      ctrl.on 'error', ->
        ctrl.lastError.should.equal 'Shit happens'
        done()
      FakeResource.query = -> createBadPromise 'Shit happens'
      sync.initialize ctrl

    describe "with custom urls", ->

      it "should call $get rather than query", (done) ->
        sync = new ListSync FakeResource, '/apples/bananas'
        ctrl.on 'ready', ->
          ctrl.items.length.should.equal 3
          done()
        FakeResource.$get = (path) -> 
          path.should.equal '/apples/bananas'
          createGoodQueryPromise 3
        sync.initialize ctrl

      describe "which contain variables", ->

        it "should resolve them if possible", (done) ->
          sync = new ListSync FakeResource, '/apples/$(appleId)'
          sync.window = {appleId: 27}
          ctrl.on 'ready', ->
            ctrl.items.length.should.equal 3
            done()
          FakeResource.$get = (path) -> 
            path.should.equal '/apples/27'
            createGoodQueryPromise 3
          sync.initialize ctrl

        it "should complain if it cannot resolve them", ->
          sync = new ListSync FakeResource, '/apples/$(appleId)'
          (-> sync.initialize ctrl).should.throw Error


  describe "other crud", ->

    beforeEach ->
      FakeResource.query = -> createGoodQueryPromise 3
      sync.initialize ctrl

    it "should extend its controller with a newResource method", ->
      ctrl.newResource().i_am_a_fake.should.equal true

    describe "changes (happy path)", ->

      happy = undefined

      beforeEach ->
        happy =
          memory: []
          save: ->
            @memory.push 'save'
            createGoodPromise()
          delete: ->
            @memory.push 'delete'
            createGoodPromise()

      it "should call Resource.save() on a new item and fingers crossed all is well with the world", (done) ->
        ctrl.add happy
        eventually ->
          happy.memory.join(',').should.equal 'save'
          done()

      it "should call Resource.save() on an updated item and fingers crossed all is well with the world", (done) ->
        ctrl.items.push happy
        ctrl.updated happy
        eventually ->
          happy.memory.join(',').should.equal 'save'
          done()

      it "should call Resource.delete() on a deleted item and fingers crossed all is well with the world", (done) ->
        ctrl.items.push happy
        ctrl.remove happy
        eventually ->
          happy.memory.join(',').should.equal 'delete'
          done()

    describe "changes (unhappy path)", ->

      sad = undefined

      beforeEach ->
        sad =
          save: -> createBadPromise 'Save failed'
          delete: -> createBadPromise 'Delete failed'

      it "should notify the controller if the create failed", (done) ->
        ctrl.on 'error', ->
          ctrl.lastError.should.equal 'Save failed'
          done()
        ctrl.add sad

      it "should notify the controller if the update failed", (done) ->
        ctrl.on 'error', ->
          ctrl.lastError.should.equal 'Save failed'
          done()
        ctrl.items.push sad
        ctrl.updated sad

      it "should notify the controller if the delete failed", (done) ->
        ctrl.on 'error', ->
          ctrl.lastError.should.equal 'Delete failed'
          done()
        ctrl.items.push sad
        ctrl.remove sad
