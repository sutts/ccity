_ = require 'lodash'
{RegistrationPageCtrl} = require '../../src/pages/registrationPageCtrl.coffee'
{Deferred} = require '../helpers/promise.coffee'

chai = require 'chai'
chai.should()

class FakeResource

describe "RegistrationPageCtrl", ->

  fakeWindow = { registrationId: 28 }

  ctrl = requirement1 = requirement2 = submittable1 = submittable2 = task1 = task2 = undefined
  submission = event = review = registration = undefined
  
  beforeEach ->
    
    person        =  { id: 27 }
    requirement1  =  { when: 'soon', id: 1 }
    requirement2  =  { when: 'upfront', id: 2 }
    submittable1  =  { id: 11, name: 'XYZ', attachments: true, number: true, numberAs: 'Fruit', issuedBy: false, issueDate: false, expiryDate: false }
    submittable2  =  { id: 12, name: 'ABC', attachments: false, number: true, numberAs: 'Vegetable', issuedBy: false, issueDate: false, expiryDate: false }
    submission    =  { id: 13, number: 'Banana', by: 'whoever', when: new Date, submittable: submittable1 }
    event         =  { type: 'submission', by: 'whoever', when: new Date, obj: submission }
    task1         =  { problem: 'incomplete_submission', requirement: requirement1, submittable: submittable1 }
    task2         =  { problem: 'no_submission', requirement: requirement2, submittable: submittable2 }
    review        =  { problem: 'no_review', targetId: 13 }
    blob          =  { person: person, tasks: [ task1, task2 ], timeline: [event], reviews: [review] }

    FakeResource::['save'] = ->
      new Deferred().promise

    ctrl = new RegistrationPageCtrl undefined, fakeWindow, FakeResource, FakeResource, FakeResource
    ctrl.initialize blob

  describe 'initialize', ->
    
    beforeEach ->

    describe 'tasks', ->
      it "should extract tasks from the initialization blob", ->
        ctrl.tasks.length.should.equal 2 
      it "should sort them by when and name", ->
        ctrl.tasks[0].should.equal task2
        ctrl.tasks[1].should.equal task1
      it "should besmirch itself with some passing UI concerns", ->
        ctrl.tasks[0].label.should.deep.equal {when: 'now', css: 'label-danger', sortIndex: 0}
        ctrl.tasks[1].label.should.deep.equal {when: 'soon', css: 'label-warning', sortIndex: 1}

    describe 'reviews', ->
      it "should extract reviews from the initialization blob", ->
        ctrl.reviews.should.deep.equal [review] 
      it "should hook up reviews with their targets (and vice versa)", ->
        review.target.should.equal event
        submission.review.should.equal review

    describe 'timeline', ->
      it "should extract events from the initialization blob", ->
        ctrl.timeline.length.should.equal 1
      it "should populate entries array on submission events", ->
        ctrl.timeline[0].obj.entries.should.deep.equal [ {property: 'number', name: 'Fruit', type: 'text', value: 'Banana' } ]

  describe 'submission tasks', ->

    submissions = undefined
    beforeEach -> submissions = ctrl.editor.submissions

    describe 'begin task', ->
      it "should remember task in progress", ->
        submissions.begin task1
        submissions.current.should.equal task1
      it "should ignore other calls to begin while this task is open", ->
        submissions.begin task1
        submissions.begin task2
        submissions.current.should.equal task1
      it "should decorate the current task with some extra stuff", ->
        submissions.begin task1
        task1.entries.should.deep.equal [ { property: 'number', name: 'Fruit', type: 'text' } ]
    describe 'cancel task', ->
      it "should cancel task in progress", ->
        submissions.begin task1
        submissions.cancel()
        (submissions.current is undefined).should.equal true
      it "should not be stoopid", ->
        submissions.cancel()
        submissions.cancel()
        (submissions.current is undefined).should.equal true
    describe 'commit', ->
      beforeEach ->
        submissions.begin task1
        task1.entries[0].value = 'Apple'
      it "should not be stoopid", ->
        submissions.commit()
        submissions.commit()
        (submissions.current is undefined).should.equal true
      it "should mark the task as resolved", ->
        submissions.commit()
        task1.resolved.should.equal true
      it "should construct an appropriate resource instance to save", (done) ->
        FakeResource::['save'] = ->
          @number.should.equal 'apple'
          @attachments.should.deep.equal ['banana']
          @person.id.should.equal 27
          @submittable.id.should.equal 11
          done()
          new Deferred().promise
        task1.entries[0].value = 'apple'  
        task1.attachments = ['banana' ]
        submissions.commit()


  describe 'reviews', ->
    
    reviews = undefined
    beforeEach -> reviews = ctrl.editor.reviews

    describe 'decide', ->
      it 'should mark the review as resolved', ->
        reviews.decide(review, 'Submission', true)
        review.resolved.should.equal true
      it "should construct an appropriate resource instance to save", (done) ->
        FakeResource::['save'] = ->
          @ok.should.equal true
          @decidable.type.should.equal 'Submission'
          @decidable.id.should.equal review.targetId
          done()
          new Deferred().promise
        reviews.decide(review, 'Submission', true)
      it 'should unlink the review', ->
        reviews.decide(review, 'Submission', true)
        (review.target is undefined).should.equal true
        (submission.review is undefined).should.equal true


  describe 'exemptions', ->
    
    exemptions = undefined
    beforeEach -> exemptions = ctrl.editor.exemptions

    describe 'begin', ->
      it "should remember task in progress", ->
        exemptions.begin(task1)
        exemptions.current.task.should.equal task1
    describe 'commit', ->
      it "should mark the task as resolved", ->
        exemptions.begin task1
        exemptions.commit()
        task1.resolved.should.equal true
      xit "should construct an appropriate resource instance to save", (done) ->
        FakeResource::['save'] = =>
          @reason.should.equal 'Please with cherry on top'
          done()
        exemptions.begin task1
        exemptions.current.reason = 'Please with cherry on top'
        exemptions.commit()
