{FilterWrapper,RequirementFilterChooser} = require '../../src/ng/requirementFilters.coffee'

chai = require 'chai'
chai.should()

describe "FilterWrapper", ->

  listOfFruits = {}
  listOfFruits.find = (id) -> 
    if id is 167 then { name: 'Lemon' } else undefined

  fruitFilterObj = { type: 'Fruit', mode: 'include',   tags: [],           references: [ {id: 167} ] } 
  colorFilterObj = { type: 'Color', mode: 'exclude',   tags: [ 'green' ],  references: [] } 
  badFilterObj   = { type: 'Mineral', mode: 'include', tags: [],           references: [] } 

  fruitFilter = new FilterWrapper(fruitFilterObj, 'fruit', listOfFruits)
  colorFilter = new FilterWrapper(colorFilterObj, 'color')
  badFilter = new FilterWrapper(badFilterObj, 'mineral')
  vegetableFilter = new FilterWrapper(undefined, 'vegetable')

  describe "rangeDescriptor", ->

    it "should be a human readable representation of the filter's range", ->
      fruitFilter.rangeDescriptor().should.equal 'Specific fruit'
      colorFilter.rangeDescriptor().should.equal 'All colors not tagged'
      vegetableFilter.rangeDescriptor().should.equal 'All vegetables'

  describe "referenceDescriptor", ->
    describe "when no reference item has been set", ->
      it "should be undefined", ->
        (vegetableFilter.referenceDescriptor() == undefined).should.equal true
    describe "when a tag  is set", ->
      it "should be the value of the first tag", ->
        colorFilter.referenceDescriptor().should.equal 'green'
    describe "when an item reference is set", ->
      it "should be the name of the first item", ->
        fruitFilter.referenceDescriptor().should.equal 'Lemon'

  describe "isValid", ->
    describe "with null filter", ->
      it "should always be true", ->
        vegetableFilter.isValid().should.equal true
    describe "with non-null filter", ->
      it "should be true if reference or tag is set", ->
        colorFilter.isValid().should.equal true
        fruitFilter.isValid().should.equal true
      it "should be false if neither reference nor tag are set", ->
        badFilter.isValid().should.equal false


describe "RequirementFilterChooser", ->

  list = 
    items: [ {id: 27, name: 'Apple'}, {id: 28, name: 'Grapefruit'} ]
    tags:
      items: [ {name: 'Citrus'} ]

  chooser = undefined

  beforeEach ->
    chooser = new RequirementFilterChooser list, 'Fruit'

  describe "modes", ->

    it "should generate a list of logical modes to pick from", ->
      chooser.modes.length.should.equal 5
      chooser.modes[0].isNullFilter().should.equal true
      chooser.modes[1].isNullFilter().should.equal false
      chooser.modes[1].isExclude().should.equal false
      chooser.modes[4].isExclude().should.equal true
      chooser.modes[4].isTag().should.equal true

    it "should be able to map these to a Select2-friendly array", ->
      chooser.modesAsSelect2Options().should.deep.equal [ { id: '0', text: 'All fruits' }
                                                          { id: '1', text: 'Specific fruit' }
                                                          { id: '2', text: 'All fruits tagged' }
                                                          { id: '3', text: 'All fruits except' }
                                                          { id: '4', text: 'All fruits not tagged' }  ]
    
    it "should default itself to mode 0", ->
      chooser.currentMode.isNullFilter().should.equal true

  describe "selectMode and selectItem", ->

    it "should be possible select a mode from a mode id (as integer or string)", ->
      chooser.selectMode '2'
      chooser.currentMode.isTag().should.equal true
      chooser.currentMode.isExclude().should.equal false

    it "should be possible select an item", ->
      chooser.selectMode '2'
      chooser.selectItem 'Citrus'
      chooser.currentItem.should.equal 'Citrus'

    it "should automatically reset item whenever mode changes", ->
      chooser.selectMode '2'
      chooser.selectItem 'Citrus'
      chooser.selectMode '3'
      (chooser.currentItem is undefined).should.equal true

    it "should treat empty string as undefined", ->
      chooser.selectMode '2'
      chooser.selectItem ''
      (chooser.currentItem is undefined).should.equal true

    it "should complain if an item is set in when mode is null filter", ->
      chooser.selectMode '0'
      (-> chooser.selectItem 'Citrus').should.throw()

  describe "selectedMode", ->

    it "should always return selected mode as a string", ->
      chooser.selectMode 2
      chooser.selectedMode().should.equal '2'

  describe "selectedItem", ->

    it "should always return selected tag references as a string (which is fine because they are strings)", ->
      chooser.selectMode 2
      chooser.selectItem 'Citrus'
      chooser.selectedItem().should.equal 'Citrus'

    it "should always return selected item references as a string", ->
      chooser.selectMode 1
      chooser.selectItem 27
      chooser.selectedItem().should.equal '27'

  describe "itemsAsSelect2Options", ->

    it "should give me a list of Select2-friendly items when item filtering is set", ->
      chooser.selectMode 1
      chooser.itemsAsSelect2Options().should.deep.equal [ {id: '27', text: 'Apple'}, {id: '28', text: 'Grapefruit'}]

    it "should give me a list of Select2-friendly tags when tag filtering is set", ->
      chooser.selectMode 2
      chooser.itemsAsSelect2Options().should.deep.equal [ {id: 'Citrus', text: 'Citrus'} ]

  describe "toFilter", ->
    describe "with null filter", ->
      it "should be undefined", ->
        chooser.selectMode 0
        (chooser.toFilter() == undefined).should.equal true
        chooser.isValid().should.equal true
    describe "with tags", ->
      it "should be incomplete when no tag is selected", ->
        chooser.selectMode 2
        chooser.toFilter().should.deep.equal { type: 'Fruit', mode: 'include', tags: [], references: [] }
        chooser.isValid().should.equal false
      it "should hold the tag name when a tag is selected", ->
        chooser.selectMode 2
        chooser.selectItem 'Citrus'
        chooser.toFilter().should.deep.equal { type: 'Fruit', mode: 'include', tags: ['Citrus'], references: [] }
        chooser.isValid().should.equal true
    describe "with references", ->
      it "should be incomplete when no reference is selected", ->
        chooser.selectMode 3
        chooser.toFilter().should.deep.equal { type: 'Fruit', mode: 'exclude', tags: [], references: [] }
        chooser.isValid().should.equal false
      it "should flesh out a min object when a reference is selected", ->
        chooser.selectMode 3
        chooser.selectItem '27'
        chooser.toFilter().should.deep.equal { type: 'Fruit', mode: 'exclude', tags: [], references: [{type: 'Fruit', id: 27}] }
        chooser.isValid().should.equal true

  describe "fromFilter", ->

    it "should work for all supported modes", ->
      for mode in chooser.modes
        chooser.fromFilter mode.obj
        chooser.currentMode.isExclude().should.equal mode.isExclude()
        chooser.currentMode.hasTag().should.equal mode.hasTag()
        chooser.currentMode.hasRef().should.equal mode.hasRef()
        if mode.hasTag()
          chooser.currentItem.should.equal mode.obj.tags[0]
        if mode.hasRef()
          chooser.currentItem.should.equal mode.obj.references[0].id


  describe "roundTrip", ->

    it "should work", ->

      chooser.selectMode 2
      chooser.selectItem 'Citrus'
      filter1 = chooser.toFilter()

      chooser.selectMode 1
      chooser.selectItem 'Something else'

      chooser.fromFilter(filter1)
      filter2 = chooser.toFilter()
      filter2.should.deep.equal filter1
           
