{Expression} = require '../../src/ng/expression.coffee'

chai = require 'chai'
chai.should()

describe "Expression", ->

  it "should complain if no expression is supplied", ->
    (-> new Expression).should.throw "Empty expression"

  it "should always take the last token as the target property / function name", ->
    for s in ['a.b.c', 'b.c', 'c']
      new Expression(s).targetProperty.should.equal 'c'

  it "should always take all but the last token as the target path", ->
    new Expression('a.b.c').targetPath.should.equal 'a.b'
    new Expression('a.b').targetPath.should.equal 'a'
    (new Expression('a').targetPath is undefined).should.equal true

  createFakeScope = (correctPath, objectToReturn) ->
    {
      $eval: (somePath) ->
        if somePath is correctPath
          objectToReturn
        else
          undefined
    }

  it "should be able to resolve target with the help of the passed scope object", ->
    obj = {}
    scope = createFakeScope 'a.b.c', obj
    new Expression('a').resolve(scope).instance.should.equal scope
    new Expression('a.b.c.d').resolve(scope).instance.should.equal obj

  it "should be able to get from a property", ->
    obj = {d: 'xyz'}
    scope = createFakeScope 'a.b.c', obj
    new Expression('a.b.c.d').get(scope).should.equal 'xyz'

  it "should be able to set a property", ->
    obj = {}
    scope = createFakeScope 'a.b.c', obj
    new Expression('a.b.c.d').set scope, 'xyz'
    obj.d.should.equal 'xyz'

  it "should also be able to get from a function", ->
    obj =
      d: -> 'xyz'
    scope = createFakeScope 'a.b.c', obj
    new Expression('a.b.c.d').get(scope).should.equal 'xyz'

  it "should be able to call a function", ->
    obj =
      x: (param) -> @y = param
      y: 'banana'
    scope = createFakeScope 'a.b.c', obj
    new Expression('a.b.c.x').set scope, 'apple'
    obj.y.should.equal 'apple'
