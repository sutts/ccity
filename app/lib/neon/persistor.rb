module Neon
  class Persistor 

    def initialize(tx = CudTransaction.new)
      @tx = tx
    end

    def save(node)
      do_save node
    end

    def destroy(node) 
      detach node, true
      @tx.remove_node node
    end

    private 

    def do_save(node, always_create = false)
      node.before_save if node.respond_to? :before_save
      raise "Not valid: #{node.errors.full_messages}" unless node.valid?
      new_record = node.new_record? || always_create
      if new_record
        @tx.create_node node
        unless node.id and not(node.new_record?)
          raise "Illegal state (call to tx.create_node didn't really do what I was looking for)" 
        end
      else
        @tx.update_node node
        detach node, false
      end
      node.class.relationships.each do |rel|
        refs = node.send rel.as
        Array(refs).map do |ref|
          do_save ref, true if rel.nested? and (new_record or rel.mutable?)
          @tx.create_edge node, ref, rel if new_record or rel.mutable? 
        end
      end
      node
    end

    def detach(node, with_intent_to_destroy)
      node.class.relationships.each do |rel|
        if rel.nested? and (rel.mutable? or with_intent_to_destroy) 
          @tx.remove_nesteds node, rel
        elsif rel.mutable? or with_intent_to_destroy
          @tx.remove_edges node, rel
        else
          # do nothing
        end
      end
    end

  end
end