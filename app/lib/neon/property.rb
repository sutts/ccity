module Neon
  
  class Property

    attr_reader :name, :options

    def initialize(clazz, name, options={})
      @name = name
      @options = options
      clazz.send :define_method, @name do
        instance_variable_get "@#{name}"
      end
      clazz.send :define_method, "#{@name}!" do
        value = instance_variable_get "@#{name}"
        raise "#{@name} not set" if value.nil?
        value 
      end
      clazz.send :define_method, "#{@name}=" do |value|
        unless value.nil?
          type = options[:type]
          if type == :boolean
            raise "Argument error '#{value}' on #{clazz.name}##{name}" unless value == true || value == false
          elsif type
            raise "Argument error '#{value}' for type #{type} on #{clazz.name}##{name}" unless value.is_a? type
          end
        end
        instance_variable_set "@#{name}", value
      end
    end

    def required?
      @options[:required] != false
    end

    def public?
      @options[:public] != false
    end

    def explicit_type
      @options[:type]
    end

  end

end
