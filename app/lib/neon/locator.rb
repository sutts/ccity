module Neon
  
  module Joinable
    
    def join(rel_name, options={})
      Join.new(self, rel_name, options).tap { |j| @joins << j } 
    end
    
    def with(*rel_patterns)
      unless rel_patterns.empty?
        pattern = rel_patterns.first
        if pattern.is_a? Array
          rel_patterns.each { |p| with *p }
        else
          if pattern[0] == '>'
            arrow = :right
          elsif pattern[0] == '<'
            arrow = :left
          else
            raise "Rel pattern must start with '<' or '>' but got '#{pattern}'"
          end
          optional = pattern[-1, 1] != '!'
          rel_name_with_comments = optional ? pattern[1..-1] : pattern[1...-1]
          rel_name = rel_name_with_comments.sub(/\([^)]*\)/, '').to_sym
          join(rel_name, optional: optional, direction: arrow).with  *rel_patterns[1..-1]
        end
      end
      self
    end
    
    def find_join(rel_name, complain_if_not_found=true)
      return self if self.is_a? Join and self.rel_name == rel_name
      @joins.each do |child|  
        gotcha = child.find_join rel_name, false
        return gotcha if gotcha
      end
      raise "No such join '#{rel_name}'" if complain_if_not_found
      nil
    end

    def apply_joins(joins, query, node_symbol)
      joins.each { |join| join.decorate query, node_symbol unless join.optional }
      joins.each { |join| join.decorate query, node_symbol if join.optional }
    end

  end

  class Locator
    include Joinable
    
    def initialize(clazz, gateway = Neon::Gateway.new, unanchored = false)
      @clazz = clazz
      @gateway = gateway
      @joins = []
      clazz.send :with_defaults, self if clazz.respond_to? :with_defaults
    end

    def with_anchor(node, rel_name, rel_direction=:right)
      @anchor = Anchor.new node, rel_name, rel_direction
      self
    end

    def all
      where
    end

    def where(options = {})
      query = construct_query options
      yield query if block_given?
      @gateway.run_query(query).select @clazz
    end

    def find_by(options = {})
      query = construct_query options
      @gateway.run_query(query).one_and_only @clazz
    end

    def find(id)
      query = construct_query do |q, sym|
        q.where.id(sym, id.to_i)
      end
      result = @gateway.run_query query
      result.one_and_only @clazz
    end

    def construct_query(options={})
      query = Neon::CypherQuery.new
      @anchor ? @anchor.decorate(query) : query.match
      node_symbol = query.reserve_symbol
      query.node! node_symbol, label: @clazz.label
      query.find_element node_symbol do |node| 
        options.each { |k,v| node.property k, v }
      end
      yield query, node_symbol if block_given?
      apply_joins @joins, query, node_symbol
      query
    end

    def join(rel_name, options={})
      Join.new(self, rel_name, options).tap { |j| @joins << j } 
    end

    def method_missing(method_name, *args, &block)
      if @clazz.respond_to? method_name
        @clazz.send method_name, self, *args
      else
        super 
      end
      self
    end

  end

  class Join
    include Joinable
    
    attr_reader :parent, :rel_name, :direction
    attr_accessor :optional

    def initialize(parent, rel_name, options={})
      @parent = parent
      @rel_name = rel_name
      @direction = options.fetch :direction, :out
      @optional = options.fetch :optional, true
      @joins = []
      @filters = []
    end

    def decorate(query, node_symbol)
      symbol_for_rel = query.reserve_symbol
      symbol_for_ref_node = query.reserve_symbol
      query.optional if @optional
      query.match.node(node_symbol)
                 .rel!(symbol_for_rel, type: @rel_name, arrow: @direction)
                 .node!(symbol_for_ref_node)
      @filters.each_with_index do |filter,index|
        query.s (index == 0) ? 'WHERE' : 'AND'
        (property_name,property_value,op) = filter[0],filter[1],filter[2]
        if property_name == :id
          query.id symbol_for_ref_node, property_value
        else
          field_name = "#{symbol_for_ref_node}.#{property_name}"
          var_name = "value_for_#{symbol_for_ref_node}_dot_#{property_name}" 
          query.s(field_name).s(op).var(var_name, property_value)
        end
      end
      query.where.id(symbol_for_ref_node, @id_filter) if @id_filter                 
      apply_joins @joins, query, symbol_for_ref_node
    end

    def where(property_name, property_value, op='=')
      @filters << [property_name, property_value, op]
      self
    end

    def where!(property_name, property_value, op='=')
      where property_name, property_value, op
      j = self
      while j.is_a? Join
        j.optional = false
        j = j.parent
      end
      self
    end

    def root
      @parent.is_a?(Locator) ? @parent : @parent.root 
    end

  end

  class Anchor
    def initialize(node, rel_name, rel_direction = :out)
      @node = node
      @rel_name = rel_name
      @rel_direction = rel_direction
    end
    def decorate(query)
      query .start_with_node(:a, @node.id!)
            .match
            .node!(:a)
            .relationship!(:b, type: @rel_name, arrow: @rel_direction)
    end
  end

end