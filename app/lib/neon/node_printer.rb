module Neon
  class NodePrinter

    def initialize(node, depth=0, already=Set.new)

      puts '=================' if depth==0

      @indent = ''
      depth.times { @indent << '  '}

      write "#{node.class} (#{node.id})"
      node.class.properties.each do |prop|
        write "  #{prop.name}: #{node.send(prop.name)}" unless [:created_at, :last_modified].include? prop.name 
      end
      node.class.relationships.each do |rel|
        write "  #{rel.as}:"
        Array(node.send(rel.as)).each do |ref|
          if ref.is_a? Node
            if already.include? ref
              write "    -> #{ref.id} "
            else
              already << ref
              NodePrinter.new ref, depth+2, already
            end
          else
            write "    #{ref.inspect}"
          end
        end
      end

      puts '=================' if depth==0

    end

    def write(ln)
      puts "#{@indent}#{ln}"
    end

  end
end