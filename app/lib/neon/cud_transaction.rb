module Neon
  class CudTransaction

    def initialize(gateway = Neon::Gateway.new)
      @gateway = gateway
    end

    def create_node(node)
      q = new_query.create.node! :n, label: node.label, properties: node.properties_to_persist
      result = @gateway.run_query q
      created = result.one_and_only node.class
      node.id = created.id
    end

    def update_node(node)
      @gateway.run_query base_query(node).set_map(:n, node.properties_to_persist)
    end

    def remove_node(node) 
      @gateway.run_query base_query(node).delete
    end

    def create_edge(node, ref, rel)
      q = new_query .start_with_nodes( [:n, node.id], [:r, ref.id] )
                    .merge
                    .node!(:n)
                    .rel!(:nr, type: rel.name, arrow: arrow_direction(rel))
                    .node!(:r)
      @gateway.run_query q
    end

    def remove_edges(node, rel)
      q = new_query.start_with_node(:n, node.id)
               .match
               .node(:n)
               .rel!(:r, type: rel.name, arrow: arrow_direction(rel))
               .node(:m) 
               .delete
      @gateway.run_query q
    end

    def remove_nesteds(node, rel)
      q = new_query.start_with_node(:n, node.id!)
               .match
               .node(:n)
               .rel!(:r, type: rel.name, arrow: arrow_direction(rel))
               .node!(:m)
               .optional.match
               .node(:m)
               .rel!(:any, arrow: :none)
               .node(:x)
               .delete
      puts q.to_q
      result = @gateway.run_query q
      puts result
    end

    private

    def new_query
      Neon::CypherQuery.new
    end

    def base_query(node)
      new_query.match.node!(:n).where.id(:n, node.id)
    end

    def arrow_direction(rel)
      rel.outgoing? ? :right : :left    
    end

  end
end