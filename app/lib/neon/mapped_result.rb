module Neon
  class MappedResult

    attr_reader :models

    def initialize(query_result)
      model_lookups = {}
      @models = []
      query_result.nodes.each do |node|
        model = node_to_model node
        model_lookups[node] = model
        @models << model
      end
      query_result.relationships.each do |rel|
        from_model = model_lookups[rel.start_node]
        to_model = model_lookups[rel.end_node]
        from_model.rel_from rel.type.to_sym, to_model, :log
        to_model.rel_to rel.type.to_sym, from_model, :log
      end
    end

    def one_and_only(clazz)
      selected = select clazz
      raise "Expected one model but got #{selected.count}" unless selected.count == 1
      selected.first
    end

    def select(clazz)
      @models.select { |m| m.class == clazz}
    end

    private

    def node_to_model(node)
      clazz_name = node.data['class']
      raise "Can't map result because node does not class property" unless clazz_name
      model = Object.const_get(clazz_name).new
      model.restore_properties node.data
      model.id = node.id
      model
    end

  end
end