module Neon
  
  class NodeValidator
    include Veto.validator 
    def method_missing(method_name, *args, &block)
      args.first.send method_name, self.errors
    end
  end

  class Node
  
    class << self
      attr_reader :timestamps
      def validator
        @validator ||= Class.new(NodeValidator)
      end
      def properties
        @properties ||= []
        @properties        
      end
      def relationships
        @relationships ||= []
        @relationships       
      end
      def validations(*method_names)
        method_names.each { |method_name| validator.validate method_name }
      end
      def property(name, options={})
        property = Neon::Property.new self, name, options
        properties << property
        validator.validates name, :presence => true if property.required? 
        validator.validates name, options[:validates] if options.has_key? :validates
      end
      def property_names
        properties.map(&:name)
      end
      def public_property_names
        properties.select { |p| p.public? }.map(&:name)
      end
      def api_property_names
        public_property_names.concat [:id]
      end
      alias :api :api_property_names
      def has(cardinality, direction, name, options={})
        relationship = Neon::Relationship.new self, name, cardinality, direction, options
        relationships << relationship
        validator.validates relationship.as, :presence => true if relationship.required? 
      end
      def label
        self.name.sub 'Node', ''
      end
      def merge
        false
      end
      def with_timestamps
        @timestamps = true
        self.property :created_at, :required => false, :type => DateTime
        self.property :last_modified, :required => false, :type => DateTime
      end
    end

    attr_accessor :id, :errors
    
    def initialize(attrs = {})
      self.class.properties.each do |prop|
        self.send "#{prop.name}=", prop.options[:default] if prop.options.has_key? :default
      end
      self.class.relationships.each do |rel|
        self.send "#{rel.as}=", [] if rel.many?
      end
      assign attrs
    end

    def assign(attrs = {})
      attrs.each do |k, v|
        setter = "#{k}="  
        self.send setter, v if self.respond_to? setter   
      end
      self
    end

    def label 
      self.class.label
    end

    def same_as(other)
      self == other or (self.id and self.id == other.id)
    end

    def valid?
      self.class.validator.new.valid? self
      self.class.relationships.each do |rel| 
        if rel.nested? 
          Array(self.send rel.as).each do |nested| 
            errors.add rel.as, nested.errors.full_messages unless nested.valid?
          end
        end
      end
      errors.empty?
    end

    def validate!
      raise "Not valid (#{errors.full_messages})" unless valid? 
    end

    def new_record?
      id.nil?
    end

    def id!
      raise "id not set" unless id
      id
    end

    def properties_to_persist
      if self.class.timestamps
        now = DateTime.now
        self.created_at = now unless self.created_at
        self.last_modified = now
      end
      raise "Illegal State (not valid) #{self.inspect}" unless valid?
      {}.tap do |hash|
        self.class.properties.each do |property| 
          value = self.send property.name
          unless value.nil?
            value = value.iso8601 if [Date, DateTime].include? property.explicit_type
            hash[property.name] = value
          end
        end
        hash[:class] = self.class.name
      end
    end

    def restore_properties(attrs = {})
      self.class.properties.each do |property| 
        value = attrs[property.name.to_s] || attrs[property.name]
        unless value.nil?
          value = Date.iso8601(value) if property.explicit_type == Date
          value = DateTime.iso8601(value) if property.explicit_type == DateTime
          self.send "#{property.name}=", value
        end
      end
    end

    def to_hash(*extras)
      hash = { :id => id }
      self.class.properties.each do |property| 
        hash[property.name] = self.send property.name if property.public? 
      end      
      extras.each do |extra|
        value = self.send extra
        if value.is_a? Array
          hash[extra] = value.map(&:to_hash)
        else
          hash[extra] = value.try :to_hash
        end
      end
      yield hash if block_given?
      hash
    end

    def to_jhash(*extras)
      convert_hash_keys to_hash *extras
    end

    def to_maxi_hash(visited=Set.new)
      self.to_hash.tap do |hash|
        unless visited.include? self
          visited << self
          self.class.relationships.each do |rel| 
            value = self.send rel.as
            if rel.many?
              hash[rel.as] = value.map { |e| e.to_maxi_hash(visited) }
            else
              hash[rel.as] = value.try :to_maxi_hash, visited
            end
          end      
        end
      end
    end

    def to_mini_hash
      hash = { :type => self.class.name, :id => id }
      self.class.properties.each do |property| 
        if property.options[:mini_hash]
          hash[property.name] = self.send property.name
        end
      end      
      yield hash if block_given?
      hash
    end

    def self.from_mini_hash(hash)
      type = hash.fetch :type
      Object.const_get(type).new hash
    end

    def save(persistor = Neon::Persistor.new)
      persistor.save self 
    end

    def destroy(persistor = Neon::Persistor.new)
      persistor.destroy self 
    end

    def self.locator
      Neon::Locator.new self
    end

    def locator
      self.class.locator
    end

    def self.create(attrs = {})
      node = self.new attrs
      node.save
      node
    end

    def expound()
      locator.unanchored.find self.id
    end

    def rel(rel_name, direction, value, option=nil)
      rel_def = self.class.relationships.find { |r| r.name == rel_name and r.direction == direction }
      if rel_def
        if rel_def.one?
          self.send "#{rel_def.as}=", value
        elsif rel_def.many? 
          if value.is_a? Array
            self.send "#{rel_def.as}=", value
          else
            array_value = self.send rel_def.as  
            array_value ||= []
            array_value << value
            self.send "#{rel_def.as}=", array_value
          end
        end
      elsif option == :strict
        raise ArgumentError, "Unknown relationship '#{rel_name}'"
      elsif option == :log
        # puts "[Neon::Node#rel] Ignoring unknown relationship '#{rel_name}'"
      end
    end

    def rel_from(rel_name, value, option=nil)
      rel rel_name, :outgoing, value, option
    end

    def rel_to(rel_name, value, option=nil)
      rel rel_name, :incoming, value, option
    end

    private 

    def validate_nested_element(list, rel, item)
      problems = item.validation_errors
      list << [rel.as, 'not valid', problems] unless problems.empty?
    end

    def convert_hash_keys(value)
      case value
        when Array
          value.map { |v| convert_hash_keys(v) }
          # or `value.map(&method(:convert_hash_keys))`
        when Hash
          Hash[value.map { |k, v| [k.to_s.camelize(:lower), convert_hash_keys(v)] }]
        else
          value
       end
    end
  end
end