module Neon
  class CypherQuery

    [:optional, :match, :create, :merge, :where, :not].each do |keyword|
      define_method keyword do
        push_and_return keyword.upcase
      end
    end

    def initialize(auto_return = true)
      @auto_return = auto_return
      @elements = []
      @reserved_symbols = []
    end

    def auto_return(val)
      @auto_return = val
      self
    end

    def start_with_node(symbol, id)
      start_with_nodes [symbol, id]
    end

    def start_with_nodes(*symbol_id_tuples)
      @elements << 'START' unless symbol_id_tuples.empty?
      symbol_id_tuples.each_with_index do |tuple, index|
        symbol = tuple.first
        id = tuple.last
        var_name = "#{symbol}_id"
        sometimes_need_a_comma = (index == symbol_id_tuples.count - 1) ? '' : ','
        string_form = "#{symbol}=node({#{var_name}})#{sometimes_need_a_comma}"
        @elements.push SimpleVariableElement.new string_form, var_name, id, true
      end
      self
    end

    def node(symbol, options = {})
      node = NodeElement.new symbol, options
      yield node if block_given?
      push_and_return node
    end

    def node!(symbol, options={})
      node symbol, options.merge({:return => true})
    end

    def relationship(symbol, options = {})
      rel = RelationshipElement.new symbol, options
      yield rel if block_given?
      push_and_return rel
    end

    def relationship!(symbol, options={})
      relationship symbol, options.merge({:return => true})
    end

    alias :rel :relationship
    alias :rel! :relationship!

    def id(symbol, id)
      var_name = "id_of_#{symbol}"
      string_form = "id(#{symbol})={#{var_name}}"
      push_and_return SimpleVariableElement.new string_form, var_name, id, true
    end

    def var(name, value, *string_tokens)
      @elements.push SimpleVariableElement.new "{#{name}}", name, value
      string_tokens.each { |token| s token }
      self
    end

    def set(symbol, property_name, property_value) 
      var_name = "set_#{property_name}_for_#{symbol}"
      string_form = "SET #{symbol}.#{property_name}={#{var_name}}"
      push_and_return SimpleVariableElement.new string_form, var_name, property_value
    end

    def set_map(symbol, properties) 
      var_name =  "map_for_#{symbol}"
      string_form = "SET #{symbol}={#{var_name}}"
      push_and_return SimpleVariableElement.new string_form, var_name, properties
    end

    def return(what=nil)
      push_and_return TerminalElement.new self, 'RETURN', what
    end

    def delete(what=nil)
      push_and_return TerminalElement.new self, 'DELETE', what
    end

    def with(what=nil)
      push_and_return TerminalElement.new self, 'WITH', what
    end

    def for_each(var_name, value)
       for_each_element = ForEachElement.new var_name, value
       @elements << for_each_element
       yield for_each_element
       @elements << ')'
       self
    end

    def s(some_string)
      push_and_return some_string
    end

    alias :append_return :return 

    def nodes 
      @elements.select { |n| n.is_a? NodeElement }
    end

    def elements_flagged(flag) 
      @elements.select { |e| e.options[flag] if e.respond_to? :options }
    end

    def find_elements(symbol)
      @elements.select { |e| e.respond_to? :symbol and e.symbol == symbol}
    end

    def find_element(symbol)
      e = find_elements(symbol).first
      yield e if block_given?
      e
    end

    def next_symbol(sym=:a)
      return next_symbol sym.next if @reserved_symbols.include? sym
      @elements.each do |e|
        if e.respond_to? :symbol
          if e.symbol == sym
            return next_symbol sym.next
          end
        end
      end 
      sym
    end

    def reserve_symbol
      reserved = next_symbol
      @reserved_symbols << reserved
      reserved
    end

    def symbols()
      [].tap do |list|
        @elements.each { |e| list << e.symbol if e.respond_to? :symbol }
      end
    end

    def deferred_parameter(name)
      DeferredParameter.new name
    end

    def start_with_deferred(symbol, name)
      start_with_node symbol, deferred_parameter(name)
    end

    def to_s
      before_to_s_or_params
      @elements.join ' '
    end

    def params(ctx = {})
      before_to_s_or_params
      hash = {}
      @elements.each do |e| 
        hash.merge! e.params if e.respond_to? :params
      end
      resolve_deferred_params hash, ctx
      hash
    end

    def to_q
      to_s.tap do |query_with_embedded_params__handy_for_debuggering|
        params.each do |k, v|
          # this won't work with maps but worry about that later if needed
          replacement = v.is_a?(String) ? "'#{v}'" : v.to_s
          query_with_embedded_params__handy_for_debuggering["{#{k}}"] = replacement
        end   
      end
    end

    private 

    def push_and_return(element)
      @elements << element
      self
    end

    def before_to_s_or_params
      append_return if @auto_return and not @elements.last.is_a?(TerminalElement)
    end

    def resolve_deferred_params(hash, ctx)
      hash.each do |k, v|
        if v.is_a?(DeferredParameter)
          if ctx.has_key? v.name
            # concurrent modification???
            hash[k] = ctx[v.name]
          else
            raise "IllegalState: Deferred parameter '#{v.name}' not resolved"
          end
        elsif v.is_a?(Hash)
          resolve_deferred_params v, ctx
        end
      end
    end    

  end

  class SimpleVariableElement
    def initialize(string_form, var_name, value, always_numeric = false)
      @string_form = string_form
      @var_name = var_name
      @value = value
      @always_numeric = always_numeric
    end
    def to_s
      @string_form
    end
    def params
      value = (@always_numeric and @value.is_a? String) ? Integer(@value) : @value 
      { @var_name.to_sym => value }
    end
  end

  class NodeElement
    attr_reader :options, :symbol
    def initialize(symbol, options)
      @symbol = symbol
      @options = options
      @properties = []
      property_map options[:property_map] if options[:property_map]
      property_map options[:properties] if options[:properties]
    end
    def property(name, value)
      @properties << NodeProperty.new(name, var_name(name), value, false)
    end
    def property_map(hash = {})
      @properties << NodeProperty.new(var_name('map'), var_name('map'), hash, true)
    end
    def property_blob(blob)
      @properties << blob
    end
    def var_name(name)
      "#{name}_for_#{@symbol}"
    end
    def to_s
      label = @options[:label] ? ":#{@options[:label]}" : ''
      blob = @options[:blob] ? " #{@options[:blob]}" : ''
      properties = @properties.empty? ? '' : " {#{@properties.join ', '}}"
      "(#{@symbol}#{label}#{blob}#{properties})"
    end
    def params
      {}.tap do |hash|
        @properties.each do |property| 
          hash[property.var_name.to_sym] = property.value if property.respond_to? :var_name
        end
      end
    end
  end

  class NodeProperty
    attr_reader :var_name, :value
    def initialize(name, var_name, value, is_map)
      @name = name
      @var_name = var_name
      @value = value
      @is_map = is_map
    end
    def to_s
      @is_map ? "#{@name}" : "#{@name}: {#{@var_name}}"
    end
  end 

  class RelationshipElement 
    attr_reader :options, :symbol
    def initialize(symbol, options)
      @symbol = symbol
      @options = options
    end
    def to_s
      middle_bit = @symbol.nil? ? '' : @symbol.to_s
      middle_bit += ":#{@options[:type]}" if @options[:type]
      middle_bit = middle_bit.empty? ? '' : "[#{middle_bit}]"
      if @options[:arrow] == :left
        "<-#{middle_bit}-"
      elsif @options[:arrow] == :none
        "-#{middle_bit}-"
      else
        "-#{middle_bit}->"
      end
    end
  end

  class ForEachElement
    def initialize(var_name, value)
      @var_name = var_name
      @value = value
      @param_name = "for_each_#{var_name}s"
    end
    def to_s
      end_bit = @value.is_a?(Array) ? "{#{@param_name}}" : @value
      "FOREACH ( #{@var_name} IN #{end_bit} |"
    end
    def params
      @value.is_a?(Array) ? {@param_name.to_sym => @value} : {}
    end
  end

  class TerminalElement
    def initialize(query, operation, what)
      @query = query
      @operation = operation
      @what = what
    end
    def to_s
      what = @what || @query.elements_flagged(:return).map(&:symbol).join(',')    
      raise "CypherQuery: Nothing to return" if what.empty?
      "#{@operation} #{what}"
    end
  end

  class DeferredParameter
    attr_reader :name
    def initialize(name)
      @name = name
    end
    def to_s
      "<DeferredParameter{#{@name}}>"
    end
  end
end