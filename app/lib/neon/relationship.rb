module Neon
  class Relationship
  
    attr_reader :name, :direction, :cardinality, :options, :as
  
    def initialize(clazz, name, cardinality, direction, options={})

      raise ArgumentError, "Bad direction #{direction}" unless [:incoming, :outgoing].include? direction
      raise ArgumentError, "Bad cardinality #{cardinality}" unless [:one, :many].include? cardinality

      @name = name
      @direction = direction
      @cardinality = cardinality
      @as = options[:as] || @name
      @options = options
      
      as = @as # '@as' is not in scope for define_method below but 'as' is 

      if clazz.methods.include? @as.to_sym
        raise "Method name conflict on #{@as} - that's a recipe for disaster if ever I've seen one"
      end

      clazz.send :define_method, @as do
        instance_variable_get "@#{as}"
      end
      clazz.send :define_method, "#{@as}!" do
        value = instance_variable_get "@#{as}"
        raise "Value for #{as} not set" if value.nil?
        value 
      end
      clazz.send :define_method, "#{@as}=" do |value|
        instance_variable_set "@#{as}", value
      end
      clazz.send :define_method, "#{@as}_by_id" do
        instance_variable_get("@#{as}").map(&:id)
      end
      clazz.send :define_method, "#{@as}_by_id=" do |value|
        mapped_value = value.map { |id| Node.new id: id } 
        instance_variable_set "@#{as}", mapped_value
      end

    end
  
    def one?
      cardinality == :one
    end
  
    def many?
      cardinality == :many
    end
  
    def incoming?
      direction == :incoming
    end
  
    def outgoing?
      direction == :outgoing
    end

    def required?
      @options[:required] == true || anchor?
    end

    def nested?
      @options[:nested] == true
    end

    def anchor?
      @options[:anchor] == true
    end

    def clazz
      @options[:clazz]
    end
  
    def mutable?
      @options[:mutable] == true 
    end

    def immutable?
      not mutable?
    end

  end
end
