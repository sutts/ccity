module Neon
  class GraphletNode

    attr_reader :id, :identifier, :data, :outgoing, :incoming 

    def initialize(hash)
      @identifier = hash[:self]
      @id = @identifier.split('/').last.to_i
      @data = hash[:data]
      @outgoing = []
      @incoming = []
    end

    def to_s
      "#{@id}: #{@data}".sub "#<Hashie::Mash ", "<"
    end

    def to_h
      @data.merge 'id' => @id
    end

    def to_mash
      Hashie::Mash.new to_h
    end

  end

  class GraphletRelationship

    attr_reader :start, :end, :type, :data, :identifier
    attr_accessor :start_node, :end_node

    def initialize(hash)
      @start = hash[:start]
      @end = hash[:end]
      @type = hash[:type]
      @data = hash[:data]
      @identifier = "#{@start} -[#{@type}]-> #{@end}"
    end

  end

  class QueryResult < Array

    def initialize(results_from_neo)
      lookups = {}
      results_from_neo["data"].each do |data_blob|
        data_blob.each do |hash|
          hash = Hashie::Mash.new hash
          if hash.has_key? :start and hash.has_key? :end
            edge = GraphletRelationship.new(hash)
            unless lookups.has_key? edge.identifier
              lookups[edge.identifier] = edge
              self << edge
            end
          elsif hash.has_key? :data and hash.has_key? :self
            node = GraphletNode.new hash
            unless lookups.has_key? node.identifier
              lookups[node.identifier] = node
              self << node
            end
          elsif hash.empty?
            # puts "Empty hash in my results blob. Very disconcerting..."
          else
            puts PP.pp results_from_neo
            raise "Eeek, not a node, not an edge, what can it be... #{hash.to_json}"
          end
        end
      end
      relationships.each do |rel|
        rel.start_node = lookups.fetch rel.start
        rel.end_node = lookups.fetch rel.end
        rel.start_node.outgoing << rel
        rel.end_node.incoming << rel
      end
    end

    def nodes
      self.select { |e| e.is_a?(GraphletNode) }
    end

    def relationships
      self.select { |e| e.is_a?(GraphletRelationship) }
    end

    def dump_by_node
      nodes.each do |node|
        puts node.to_s
        node.outgoing.each do |rel|
          puts "   #{rel.type}  -->  #{rel.end_node}"
        end
      end
    end

    def dump_by_edge
      relationships.each do |rel|
        puts "#{rel.start_node}   --[ #{rel.type} ]-->  #{rel.end_node}"
      end
    end

  end
end