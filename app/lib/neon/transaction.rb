module Neon
  class Transaction < Array 

    def self.tx(gateway=Gateway.new)
      tx = Transaction.new 
      yield tx
      tx.run gateway
    end

    def queries(queries)
      queries.each { |q| query q }
    end

    def query(query, identifier=nil)
      self.push TransactedQuery.new query, identifier
    end

    def run(gateway)
      context = {}
      self.each do |tq|
        tq.run gateway, context
      end
      self
    end

    private 
    def initialize
    end

  end

  class TransactedQuery

    attr_reader :query, :identifier, :result

    def initialize(query, identifier)
      @query = query
      @identifier = identifier
    end
    
    def run(gateway, context)
      @result = gateway.raw @query.to_s, @query.params(context)
      if identifier and @result.models.count == 1
        context[identifier] = @result.models.first.id
      end
    end   

  end
end