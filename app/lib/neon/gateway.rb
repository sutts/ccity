module Neon
  class Gateway < Neography::Rest

    def run_query(q, log=:none)
      puts "CYPHER(q): #{q.to_q}"
      raw q.to_s, q.params, log
    end

    def raw(q, params={}, log=:none)
      # puts "CYPHER(r): #{q} // #{params}" 
      hash = execute_query q, params
      puts PP.pp hash if log == :json or log == :all
      result = QueryResult.new hash
      if log == :result or log == :all
        puts "RESULT>>>"
        result.dump_by_node
        result.dump_by_edge
        puts "<<<<"
      end
      MappedResult.new result
    end

  end
end