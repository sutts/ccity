module RequirementsHelper

  def to_html(filter)
    if filter.include?
      if filter.tags.empty?
        "Only #{filter.type.downcase} <code>#{filter.references.first.name}</code>"
      else
        "App #{filter.type.downcase.pluralize} tagged <code>#{filter.tags.first}</code>"
      end
    else
      if filter.tags.empty?
        "All #{filter.type.downcase.pluralize} except <code>#{filter.references.first.name}</code>"
      else
        "All #{filter.type.downcase.pluralize} not tagged <code>#{filter.tags.first}</code>"
      end
    end      
  end
end

def style_for_decision(decision)
  if decision.approved?
    style = 'ok'
  elsif decision.declined?
    style = 'not-ok'
  elsif decision.cancelled?
    style = 'cancelled'
  else
    style = nil
  end
end