module BootstrapFormsHelper

  def input_with_label(label, container, field, options={})
    id = "#{container}[#{field}]"
    rendered_label = "<label for='#{id}'>#{label}</label>"
    rendered_input = input container, field, options.merge({:id => id})
    "#{rendered_label}#{rendered_input}"
  end

  def input(container, field, options={})
    defaults = { :type => 'text', :class => 'form-control', :required => true }
    attrs = defaults.merge options
    attrs[:name] = "#{container}[#{field}]" 
    attrs[:id] = "#{container}[#{field}]" 
    attrs['ng-model'] = "#{container}.#{field}" 
    rendered_attrs = attrs.map do | k, v | 
      if v == true
        k
      elsif v.blank?
        ''
      else
        "#{k}='#{v}'" 
      end
    end
    "<input #{rendered_attrs.join ' '}>"
  end 

  def error_block(field_name, validation_name, msg)
    "<p class='help-block' ng-if='form.#{field_name}.$error.#{validation_name}'>#{msg}</p>"
  end

end