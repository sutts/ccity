module JsonHelper

  def to_json_snippet(obj, var_name, *attrs)
    hash = {}
    attrs.each do |attr|
      hash[attr] = obj.send attr
    end
    "#{var_name}=#{hash.to_json}"
  end

end