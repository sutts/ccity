module ApplicationHelper
  
  def class_for_nav_tab(area_name)
    params[:controller].include?(area_name)  ? 'active' : ''
  end

  def class_for_admin_tab(area_name, area_current)
    (area_name == area_current)  ? 'active' : ''
  end

  def requirement_summary_name(req)
    if req.type == 'Submittable'
      "Submission of #{req.target.category} / #{req.target.name}"
    elsif req.type == 'Approval'
      "Approval by #{req.target.full_name}"
    else
      raise "Unhandled requirement type #{req.type}"
    end
  end

end
