module BootstrapPanelsHelper

  def panel_with_glyph_link(title, glyph, url, options={}, &content)
    template = (<<-TPL)
      <span class='pull-right'>
        <a href='#{url}'>
          <span class='glyphicon glyphicon-#{glyph}'/>
        </a>
      </span>
    TPL
    options[:after_title] = template
    panel title, options, &content
  end

  def panel(title, options={}, &content)
    options = { :style => :default }.merge(options)
    template = (<<-TPL)
      <div class='panel panel-#{options[:style]}'>
        <div class='panel-heading'>
          <h3 class='panel-title'>#{title}#{options[:after_title]}</h3>
        </div>                      
        <div class='panel-body'>
          #{capture(&content)}
        </div>
      </div>
    TPL
  end

  def collapsible_panel(title, collapse_id, ng_in_or_out, options={}, &content)
    options = { :style => :default }.merge(options)
    ng_init = options[:ng_init] ? "ng-init='#{options[:ng_init]}'" : ''
    ng_show = options[:ng_show] ? "ng-show='#{options[:ng_show]}'" : ''
    ng_bind = options[:ng_bind] ? "(<span ng-bind='#{options[:ng_bind]}'></span>)" : ''
    template = (<<-TPL)
      <div class='panel panel-#{options[:style]}' #{ng_init} #{ng_show}>
        <div class='panel-heading'>
          <h3 class="panel-title">
            <a data-toggle="collapse" href="##{collapse_id}">#{title} #{ng_bind}</a>
          </h3>
        </div>                      
        <div id="#{collapse_id}" class="panel-collapse collapse" ng-class="#{ng_in_or_out}">
          <div class='panel-body'>
            #{capture(&content)}
          </div>
        </div>
      </div>
    TPL
  end

  def modal(id, title, ng_click, &content)
    template = (<<-TPL)
      <div class="modal fade" id="#{id}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title">#{title}</h4>
            </div>
            <div class="modal-body">
              #{capture(&content)}
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="#{ng_click}">Yes</button>
              <button type="button" class="btn btn-link" data-dismiss="modal">Rather not</button>
            </div>
          </div>
        </div>
      </div>
    TPL
  end

  def admin_panel(title, link, &content)
    template = (<<-TPL)
      <div class="panel">
        <div class="panel-body">
          <h3>#{title}</h3>
          <p>
            <a href='#{link}'>
              <button type="button" class="btn btn-success btn-lg" style="margin-left: 20px">Manage &raquo;</button>
            </a>
            #{capture(&content)}
          </p>
        </div>
      </div>
    TPL
  end

end