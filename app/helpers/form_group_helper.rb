module FormGroupHelper

  class FormGroup

    attr_reader :options

    def initialize(options={})
      @options = {required: true}.merge options
      @error_blocks = []
    end

    def error(error_key, display)
      @error_blocks << "<p class='help-block' ng-if='form.#{fetch :name}.$error.#{error_key}'>#{display}</p>"
      self
    end

    def horizontal
      template = (<<-TPL)
        <div class="form-group" show-errors>
          <label class="col-sm-#{label_width} control-label">#{fetch :label}</label>
          <div class="col-sm-#{fetch :control_width}">
            <input  type="#{fetch :type}" 
                    class="form-control" 
                    name="#{fetch :name}" 
                    placeholder="#{get :placeholder}" 
                    ng-model="#{fetch :ng_model}" 
                    #{get(:required) ? 'required' : ''}>
            #{@error_blocks.join ' '}
          </div>
        </div>
      TPL
      template
    end

    private 

    def fetch(key, defaultValue=nil)
      @options.fetch key, defaultValue
    end

    def get(key)
      @options[key]
    end

    def label_width
      12 - fetch(:control_width)
    end

  end

  def new_form_group(options={})
    group = FormGroup.new options
    yield group if block_given?
    group
  end

end