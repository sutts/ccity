module BootstrapMiscHelper

  def glyph(g)
    "<span class='glyphicon glyphicon-#{g}'></span>"
  end

  def glyph_with_text(g, txt)
    "#{glyph(g)} #{txt}"
  end

end