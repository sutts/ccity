module PageControllerHelper

  def controller_dot_last_error
    template = (<<-TPL)
      <div class='alert alert-danger' ng-if='ctrl.lastError != null'>
        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
        <h4>Darn it! Something went wrong</h4>
        <p>
          There was a bit of a problem under the hood. 
          No big deal but you should probably reload the page or you might lose some work. Sorry 'bout that.
        </p>
        <p>
          <button type='button' class='btn btn-danger' onclick='window.location.reload()'>Reload the page now</button>
          <button type='button' class='btn btn-default' data-toggle='collapse' data-target='#error'>Show/hide details</button>
        </p>
        <div id='error' class='collapse'>
          <hr>
          <p>
            {{ctrl.lastError}}
          </p>
        </div>
      </div>
    TPL
  end

  def controller_div_with_busy_and_last_error(ctrl, options={}, &content)
    template = (<<-TPL)
      <div ng-controller='#{ctrl}' ng-cloak>
        #{controller_dot_last_error}
        <div ng-if='!ctrl.ready' class='loading'>
          <div class='animated fadeIn' ng-if='!ctrl.lastError'>
            <span class='fa fa-circle-o-notch fa-spin fa-3x'></span> Loading... 
          </div>
        </div>
        <div ng-if='ctrl.ready' ng-init="#{options[:ng_init]}">
          #{capture(&content)}
        </div>
      </div>
    TPL
  end

end