module OldBootstrapHelper

  def horizontal_input(label, control_width, type, name, ng_model, options={})

    form_name = options.fetch(:form_name, 'form')
    label_width = 12 - control_width
    required = options.fetch(:required, true) ? 'required' : ''

    attrs = options.fetch :attributes, []
    attrs << ['placeholder', options[:placeholder]] if options.has_key? :placeholder
    attribute_blocks = attrs.map { |pair| " #{pair.first}='#{pair.last}'" }

    help_blocks = options.fetch(:validations,[]).map do |pair| 
      condition = "#{form_name}.#{name}.$error.#{pair.first}"
      display = pair.last
      "<p class='help-block' ng-if='#{condition}'>#{display}</p>"
    end

    template = (<<-TPL)
      <div class="form-group" show-errors>
        <label class="col-sm-#{label_width} control-label">#{label}</label>
        <div class="col-sm-#{control_width}">
          <input  type="#{type}" 
                  class="form-control" 
                  name="#{name}" 
                  ng-model="#{ng_model}" 
                  #{attribute_blocks.join ' '} #{required}>
          #{help_blocks.join ' '}
        </div>
      </div>
    TPL

    template.html_safe
  end

  def alert(type, condition, text)
    template = (<<-TPL)
      <div class="alert alert-#{type} alert-dismissible" role="alert" ng-show="#{condition}">
        <button type="button" class="close" data-dismiss="alert">
          <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
        </button>
        #{text}
      </div>
    TPL
    template.html_safe
  end

end