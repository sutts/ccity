class ContextualizedRequirementList < Array

  def self.for(tenant, context)
    RequirementList.for(tenant).applicables(context).contextualized(context)
  end

  def self.wrap(requirement_list, registration)
    ContextualizedRequirementList.new.tap do |list|
      requirement_list.each do |req| 
        list.push ContextualizedRequirement.new req, registration
      end
    end
  end

  def user_facings
    ContextualizedRequirementList.new self.select { |cr| cr.user_facing? }
  end
  def mandatories
    ContextualizedRequirementList.new user_facings.select { |cr| cr.mandatory? }
  end
  def provideds
    ContextualizedRequirementList.new user_facings.select { |cr| cr.has_valid_submission? }
  end
  def checks
    ContextualizedRequirementList.new user_facings.select { |cr| cr.check? }
  end

  def non_user_facings
    ContextualizedRequirementList.new self.select { |cr| not cr.user_facing? }
  end
  def non_mandatories
    ContextualizedRequirementList.new user_facings.select { |cr| not cr.mandatory? }
  end
  def non_provideds
    ContextualizedRequirementList.new user_facings.select { |cr| not cr.has_valid_submission? }
  end

  def exemptables
    non_mandatories.non_provideds
  end

end