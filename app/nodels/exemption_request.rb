class ExemptionRequest < Neon::Node
  with_timestamps
  include Decidable

  has :one,  :outgoing, :requirement, :required => true
  has :one,  :outgoing, :submitter, :required => true
  has :one,  :incoming, :exemption_request,  :as => :registration, :required => true

  has :many, :outgoing, :decision,  :as => :decisions


  property :reason, :required => true

  def self.deep(locator)
    locator.join(:requirement).join(:target)
    locator.join(:decision)
    locator.join(:submitter)
    locator
  end


end