class Contract < Neon::Node
  has :one,  :outgoing, :client, :required => true
  has :one,  :outgoing,  :vendor, :required => true

  property :name
  property :start_date, :required => false
  property :end_date, :required => false

  def self.with_defaults(locator)
    locator .with_clients!
            .with_vendors!
  end

  def self.with_clients!(locator)
    locator.with '>client!'
  end

  def self.with_vendors!(locator)
    locator.with '>vendor!'
  end

  def self.with_client_filter(locator, tenant)
    locator.tap { |l| l.find_join(:client).where :id, tenant.id }
  end

  def self.with_vendor_filter(locator, tenant)
    locator.tap { |l| l.find_join(:vendor).where :id, tenant.id }
  end

  def self.clients_of(tenant)
    locator.with_vendor_filter(tenant).all
  end

  def self.vendors_to(tenant)
    locator.with_client_filter(tenant).all
  end

  def self.between(vendor, client)
    locator.with_vendor_filter(vendor).with_client_filter(client).all
  end

  def to_hash
    super do |hash|
      hash[:vendor] = vendor.to_mini_hash
      hash[:client] = client.to_mini_hash
    end
  end

end
