class Location < Tenanted
  has :one,  :incoming, :owns, :as => :tenant, :anchor => true

  property :name
  property :tags, :required => false, :default => []
end

