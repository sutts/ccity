class TenantInfo

  attr_reader :tenant, :requirements

  def initialize(tenant)
    @tenant = tenant
  end

  def requirements
    @requirements ||= RequirementList.for(tenant)
  end

end