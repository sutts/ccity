class PersonInfo

  attr_reader :tenant_info, :person

  def self.all(tenant_info)
    tenant_info.tenant.locator_for(Person).deep.all.map do |person|
      PersonInfo.new tenant_info, person 
    end
  end

  def self.find(tenant_info, person_id)
    PersonInfo.new tenant_info, tenant_info.tenant.locator_for(Person).deep.find(person_id)
  end

  def self.sort(person_infos)
    person_infos.sort_by { |e| [e.person.family_name, e.person.given_name] }
  end

  def initialize(tenant_info, person)
    @tenant_info = tenant_info
    @person = person
  end

  def timeline
    @timeline ||= PersonTimeline.new @person
  end

  def registration_infos
    @registration_infos ||= @person.registrations.map do |reg|
      RegistrationInfo.new @tenant_info, self, reg
    end
  end

  def registration_infos_sorted
    registration_infos.sort { |a,b| a.registration.position.name <=> b.registration.position.name }
  end

  def submission_infos
    unless @submission_infos
      @submission_infos = []
      groups = {}
      @person.submissions.each do |submission|
        groups[submission.submittable] ||= SubmissionInfo.new submission.submittable
        groups[submission.submittable].push submission
      end
      groups.values.each do |submission_info|
        submission_info.freeze
        @submission_infos << submission_info
      end
      @submission_infos.sort! { |a,b| a.submittable.name <=> b.submittable.name }
    end
    @submission_infos
  end

end