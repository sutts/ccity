class Requirement < Tenanted

  @@types_map = Hash[['Submittable', 'Approval'].map.with_index.to_a]
  @@whens_map = Hash[['upfront', 'soon', 'later'].map.with_index.to_a]

  def self.types
    @@types_map.keys
  end

  def self.whens
    @@whens_map.keys
  end

  def type_sort_index()
    @@types_map[type]
  end

  def when_sort_index()
    @@whens_map[self.send :when]
  end

  has :one,  :incoming, :owns, :as => :tenant, :anchor => true

  has :one,  :outgoing,  :target, :required => true
  has :many, :outgoing,  :filters, :nested => true, :clazz => ReferenceFilter, :mutable => true

  property :type, :default => 'Submittable', :validates => { :inclusion => types }
  property :mandatory, :default => true
  property :when, :default => 'upfront', :validates => { :inclusion => ['upfront', 'soon', 'later'] }
  property :check, :default => true
  property :exemption_guidelines, :required => false

  def self.with_defaults(locator)
    locator.with_target.with_filters
  end

  def self.with_target(locator)
    locator.with '>target!'
  end

  def self.with_filters(locator)
    locator.with '>filters', '>references'
  end

  def applicable_to?(ctx)
    filters.each { |filter| return false unless filter.applicable_to? ctx }
    true
  end

  def upfront?
    self.when == 'upfront'
  end

  def soon?
    self.when == 'soon'
  end

  def to_hash
    super do |hash|
      hash[:id] = id
      hash[:filters] = filters.map(&:to_hash)
      hash[:target] = target.to_mini_hash
    end 
  end

  def self.from_hash(hash)
    Requirement.new(hash).tap do |req|
      req.filters = Array(hash[:filters]).map { |filter_hash| ReferenceFilter.from_hash filter_hash }
      req.target = from_mini_hash hash.fetch(:target)
    end
  end

  def submittable
    target
  end

  def <=> other
    return 0 if self == other
    ret = self.type_sort_index <=> other.type_sort_index
    ret = self.when_sort_index <=> other.when_sort_index if ret == 0
    ret = self.mandatory ? -1 : 1 if ret == 0
    ret = self.name <=> other.name if ret == 0
    ret
  end

end
  

