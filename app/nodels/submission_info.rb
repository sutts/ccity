class SubmissionInfo
  
  attr_reader :submittable, :current, :previous

  def initialize(submittable)
    @submittable = submittable
    @submissions = []
  end

  def push(submission)
    @submissions << submission
  end

  def freeze
    raise 'Illegal state' if @submissions.empty?
    sorted = @submissions.sort { |a,b| b.created_at <=> a.created_at }
    @current = sorted.first
    @previous = sorted[1..-1]
    @submissions.freeze
  end

end