class S3

  attr_reader :bucket, :key

  def initialize
    [:bucket, :key, :secret].each do |attr|
      value = ENV["S3_#{attr.upcase}"]
      raise "S3 #{attr} not set!" unless value
      self.instance_variable_set "@#{attr}", value
      puts self.inspect
    end
  end 

  def host
    "#{bucket}.s3.amazonaws.com"
  end

  def url
    "https://#{bucket}.s3.amazonaws.com"
  end

  def uploads_url
    "#{url}/uploads"
  end

  def create_policy
    Base64.encode64(
      {
        "expiration" => 1.hour.from_now.utc.xmlschema,
        "conditions" => [ 
          { "bucket" => @bucket }, 
          [ "starts-with", "$key", "uploads/" ],
          { "acl" => "public-read" },
          [ "starts-with", "$Content-Type", "" ],
          [ "content-length-range", 0, 5 * 1024 * 1024 ]
        ]
      }.to_json).gsub(/\n/,'')
  end

  def sign(policy)
    Base64.encode64(OpenSSL::HMAC.digest(OpenSSL::Digest::Digest.new('sha1'), @secret, policy)).gsub("\n","")
  end

  def with_signed_policy
    S3Moment.new self
  end

end

class S3Moment

  attr_reader :policy, :signature

  def initialize(s3) 
    @s3 = s3
    @policy = s3.create_policy
    @signature = s3.sign(@policy)
  end

  def method_missing(method_name, *args, &block)
    @s3.send method_name, *args
  end

end

