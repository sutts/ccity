class ContextualizedRequirement

  attr_reader :requirement, :context

  def initialize(requirement, context)
    @requirement = requirement
    @context = context
  end

  def person
    @context.person
  end

  alias_method :definition, :requirement

  def submissions
    person.submissions.select { |submission| submission.submittable.same_as @requirement.target } 
  end

  def submission
    submissions.first
  end

  def has_valid_submission?
    not submissions.empty?
  end

  def requirement_type
    requirement.type
  end

  def mandatory?
    requirement.mandatory
  end

  def check?
    requirement.check
  end

  def user_facing?
    requirement_type == 'Submittable'
  end

  def <=> other
    return self.requirement <=> other.requirement
  end

end