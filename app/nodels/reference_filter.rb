require 'set'

class ReferenceFilter < Neon::Node

  property :type, :validates => { :inclusion => ['Position', 'Location'] }
  property :mode, :validates => { :inclusion => ['include', 'exclude'] }, :default => 'include'
  property :tags, :required => false, :default => [], 
           :validates => { :presence => true, :if => Proc.new { |filter| filter.references.empty? } }
  has      :many, :outgoing,  :references

  validations :references_must_match_type

  def include?
    mode != 'exclude'
  end

  def applicable_to?(reg)
    obj = reg.send self.type.downcase
    if obj
      negate_if_exclude includes?(obj) 
    else
      true
    end
  end

  def to_hash
    super do |hash|
      hash[:references] = references.map(&:to_mini_hash)
    end
  end

  def self.from_hash(hash)
    filter = ReferenceFilter.new hash
    filter.references = Array(hash[:references]).map { |ref_hash| from_mini_hash ref_hash }
    filter
  end

  private 

  def includes?(obj)
    return true if references_by_id.include? obj.id
    return true unless (self.tags & obj.tags).empty?
    false
  end

  def negate_if_exclude(val)
    include? ? val : not(val) 
  end

  def references_must_match_type(errors)
    references.each do |ref|
      errors.add :reference, "expected type '#{type}' but got '#{ref.class.name}'" unless ref.class.name == type
    end
  end

  def collect_reference_ids_deep(collection, the_item)
    if the_item.respond_to? :members
      the_item.members.each { |member| collect_reference_ids_deep collection, member }
    else
      collection << the_item.id
    end
  end

end
