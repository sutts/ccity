class Submittable < Tenanted
  has :one,  :incoming, :owns, :as => :tenant, :anchor => true

  property :name
  property :category
  property :description,    :required => false

  property :number,         :default => false
  property :issued_by,      :default => false
  property :issue_date,     :default => false
  property :expiry_date,    :default => false
  property :attachments,    :default => false

  property :number_as,      :default => 'Number'
  property :issued_by_as,   :default => 'Issued by'
  property :issue_date_as,  :default => 'Issue date'
  property :expiry_date_as, :default => 'Expiry date'

  def full_name(sep = ' / ')
    "#{category}#{sep}#{name}"
  end

  def has_at_least_one_field?
    number or issued_by or issue_date or expiry_date
  end

end
