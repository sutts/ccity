class Attachment < Neon::Node
  with_timestamps

  property :original_name
  property :s3_name
  property :type
  property :size

end
