class Tenant < Neon::Node
  property :short_name, :mini_hash => true
  property :long_name, :mini_hash => true
  property :admin, :default => false
  
  has :many, :incoming, :vendor, :as => :vendor_contracts
  has :many, :incoming, :client, :as => :client_contracts

  def name
    long_name
  end

  def locator_for(clazz)
    Neon::Locator.new(clazz).with_anchor(self, :owns)
  end

  def find(clazz, id)
    locator_for(clazz).find(id.to_i)
  end

  def all(clazz)
    if clazz.ancestors.include? Tenanted
      locator_for(clazz).all
    else
      raise "Not tenanted!!!"
    end
  end

  def create(clazz, attrs={})
    clazz.new(attrs).tap do |obj| 
      obj.tenant = self
      obj.save 
    end
  end

end