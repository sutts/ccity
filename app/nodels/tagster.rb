class Tagster

  def initialize(tenant, clazz)
    @tenant = tenant
    @clazz = clazz
  end

  def rename(old_name, new_name)
    items = find_by_tag old_name
    Neon::Transaction.tx do |tx|
      items.each do |item|
        item.tags.map! { |t| t == old_name ? new_name : t }
        item.tags.uniq!
        item.save
      end
    end
  end

  def remove(name)
    items = find_by_tag name
    Neon::Transaction.tx do |tx|
      items.each do |item|
        item.tags.select! { |t| t != name }
        item.save
      end
    end
  end

  #
  # categories 
  # teeny bit different to tags cos cardinality is one rather than many
  # for now co-locating code but nothing more
  #

  def rename_category(old_name, new_name)
    items = find_by_category old_name
    Neon::Transaction.tx do |tx|
      items.each do |item|
        item.category = new_name
        item.save
      end
    end
  end

  private 

  def find_by_tag(name)
    @tenant.locator_for(@clazz).where do |q|
      q.where.var(:tag_name, name, 'IN', 'c.tags') # when this breaks it will be the 'c' in c.tags #|
    end
  end

  def find_by_category(name)
    @tenant.locator_for(@clazz).where :category => name
  end

end