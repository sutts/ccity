class Tenanted < Neon::Node
  def with_tenant(tenant)
    self.tenant = tenant
    self
  end
end