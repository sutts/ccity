class RegistrationInfo

  attr_reader :registration, :timeline, :submitter_tasks, :reviewer_tasks

  def initialize(tenant_info, person_info, registration)
    @tenant_info = tenant_info
    @person_info = person_info
    @registration = registration
    engine = TaskEngine.new @person_info.timeline
    requirements = @tenant_info.requirements.applicables registration
    @submitter_tasks = engine.for_submitter requirements
    @reviewer_tasks = engine.for_reviewer requirements
  end

  def status_info
    { :color => status_color, :pending => not(@reviewer_tasks.empty?) }
  end

  def to_builder
    Jbuilder.new do |registration|
      registration.(@registration, :id)
    end
  end

  private 
  
  def status_color
    whens = @submitter_tasks.map { |task| task.requirement.when }
    if whens.include? 'upfront'
      'red'
    elsif whens.include? 'soon'
      'amber'
    else
      'green'
    end
  end

end