class TaskEngine

  def initialize(timeline)
    @timeline = timeline
  end

  def for_submitter(reqs)
    [].tap do |tasks|
      Array(reqs).each do |req| 
        unless exempt? req
          submission_event = @timeline.most_recent_submission_for req.target
          if submission_event
            submission = submission_event.obj
            if submission.rejected?
              tasks.push Hashie::Mash.new :requirement => req, :problem => :rejected_submission
            else
              unless submission.correct?
                tasks.push Hashie::Mash.new :requirement => req, :problem => :incorrect_submission
              end
              if submission.expired?
                tasks.push Hashie::Mash.new :requirement => req, :problem => :expired_submission
              elsif submission.expiring?
                tasks.push Hashie::Mash.new :requirement => req, :problem => :expiring_submission
              end
            end
          else
            tasks.push Hashie::Mash.new :requirement => req, :problem => :no_submission
          end
        end
      end
    end
  end

  def for_reviewer(reqs)
    [].tap do |tasks|
      Array(reqs).each do |req| 
        exemption_event = @timeline.most_recent_exemption_request_for req
        if exemption_event and exemption_event.obj.decision_value.nil?
          tasks.push Hashie::Mash.new :requirement => req, :problem => :pending_exemption_request_decision, target: exemption_event.obj
        elsif exemption_event and exemption_event.obj.decision_value == true
          # nothing to check
        else
          submission_event = @timeline.most_recent_submission_for req.target
          if submission_event 
            submission = submission_event.obj
            if submission.decisions.empty? and submission.correct? and not submission.expired?
              tasks.push Hashie::Mash.new :requirement => req, :problem => :pending_submission_review, target: submission
            end
          end
        end
      end
    end
  end

  private 

  def exempt?(req)
    exemption_event = @timeline.most_recent_exemption_request_for req
    exemption_event and not(exemption_event.obj.rejected?)
  end

end