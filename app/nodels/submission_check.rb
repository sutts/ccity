class SubmissionCheck < Neon::Node

  has :one,  :outgoing, :requirement
  has :many, :outgoing, :decision,  :as => :decisions, :required => false, :nested => true, :clazz => Decision
  has :one,  :incoming, :check, :as => :registration

  def self.with_defaults(locator)
    locator.with '<exemption(registration)!'
  end

  def to_hash
    super.tap do |hash|
      hash[:decisions] = decisions.sort.reverse.map(&:to_hash)
    end
  end

  def decision
    decisions.sort.last || Decision.new
  end

  def <=> (other)
    requirement <=> other.requirement
  end

end