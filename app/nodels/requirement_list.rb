class RequirementList < Array

  def self.for(tenant)
    RequirementList.new tenant.all(Requirement)
  end

  def applicables(registration)
    RequirementList.new self.select { |req| req.applicable_to? registration }
  end 

  def contextualized(registration)
    ContextualizedRequirementList.wrap self, registration
  end

end