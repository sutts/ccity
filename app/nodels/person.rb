class Person < Tenanted

  has :one,  :incoming, :owns, :as => :tenant, :anchor => true
  has :many, :outgoing, :submission, :as => :submissions
  has :many, :outgoing, :registration, :as => :registrations

  property :given_name
  property :family_name
  property :middle_name, :required => false
  property :preferred_name, :required => false
  property :email

  def self.deep(locator)
    locator.join(:submission).tap do |submission_join| 
      submission_join.join :submittable
      submission_join.join :decision
      submission_join.join :attachment
    end
    locator.join(:registration).tap do |reg_join|
      Registration.with_position reg_join
      Registration.with_submitter reg_join
      Registration.with_exemption_requests_deep reg_join
    end
    locator
  end

  def full_name
    "#{given_name} #{family_name}"
  end

  def full_name_commafied
    "#{family_name}, #{given_name}"
  end

end