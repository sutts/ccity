class RegistrationEngine

  attr_reader :registration, :contextualized_requirements
  attr_accessor :persistor

  def self.for(registration)
    contextualized_requirements = ContextualizedRequirementList.for registration.tenant, registration
    RegistrationEngine.new registration, contextualized_requirements
  end

  def initialize(registration, contextualized_requirements)
    @registration = registration
    @contextualized_requirements = contextualized_requirements
  end

  alias_method :requirements, :contextualized_requirements

  ###
  ### submit
  ###

  def can_submit?
    requirements.non_provideds.each do |req|
      return false unless registration.exemption_for(req.definition)
    end
    true
  end

  def could_submit_with_exemptions?
    requirements.mandatories.non_provideds.empty?
  end

  def submit
    raise "Illegal state" unless registration.current_status.nil?
    raise "Rejected" unless can_submit?
    requirements.checks.each do |check|
      unless @registration.exemption_for(check.definition)
        @registration.checks.push SubmissionCheck.new registration: @registration, requirement: check.definition
      end
    end
    auto_approve = @registration.checks.empty? && @registration.exemptions.empty?
    registration.current_status = auto_approve ? :approved : :pending_approval
    registration.save persistor
  end

  ###
  ### approve / decline component
  ###

  def can_decide_component?(component, decision)
    raise "Unknown decision '#{decision}'" unless [:approved, :declined].include? decision
    @registration.pending_approval? || @registration.declined? || (@registration.approved? && decision == :approved)
  end

  def decide_component(component, decision, options={})
    raise "Rejected" unless can_decide_component? component, decision
    make_it_so persistor, component, decision, options
  end

  ###
  ### approve / decline registration
  ###

  def can_decide?(decision)
    raise "Unknown decision '#{decision}'" unless [:approved, :declined, :cancelled].include? decision
    if @registration.cancelled?
      false
    elsif @registration.pending_approval? || @registration.declined? || @registration.approved? 
      if decision == :approved
        @registration.unapproved_components.empty?
      else
        true
      end
    else
      false
    end
  end

  def decide(decision, options={})
    raise "Rejected" unless can_decide? decision
    tx = persistor
    make_it_so tx, @registration, decision, options
    @registration.current_status = decision
    @registration.save tx
  end

  private 

  def make_it_so(tx, decidable, decision, options)
    d = Decision.new  value: decision.to_s, 
                      decidable: decidable,
                      comments: options[:comments], 
                      by: options[:user].try(:full_name)
    d.save tx
    decidable.decisions << d
    d
  end

  def persistor
    @persistor || Neon::Persistor.new
  end

end