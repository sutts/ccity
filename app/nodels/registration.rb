class Registration < Tenanted
  with_timestamps
  has :one,  :incoming, :owns, :as => :tenant, :required => true

  has :one,   :incoming, :registration,       :as => :person,     :required => true
  has :one,   :outgoing, :position,                               :required => true
  has :one,   :outgoing, :submitter,                              :required => true

  has :many,  :outgoing, :exemption_request,  :as => :exemption_requests
  has :many,  :outgoing, :exemption,          :as => :exemptions

  def self.with_person(locator)
    locator.with '<registration(person)'
  end

  def self.with_position(locator)
    locator.with '>position'
  end

  def self.with_submitter(locator)
    locator.with '>submitter'
  end

  def self.with_exemption_requests_deep(locator)
    locator.join(:exemption_request).tap do |ex_locator|
      ExemptionRequest.deep ex_locator
    end
    locator
  end

  def exemption_for(requirement)
    exemptions.detect { |x| x.requirement.same_as requirement }
  end

  def check_for(requirement)
    checks.detect { |x| x.requirement.same_as requirement }
  end

  def submission_for(submittable)
    person.submissions.detect { |s| s.submittable.same_as submittable }
  end

  def components
    exemptions + checks
  end

  def find_component(id)
    components.detect{ |c| c.id == id.to_i }.tap { |c| raise "No component with id #{id}" if c.nil? }     
  end

  def unapproved_components
    components.select { |c| not c.decision.try(:approved?) }
  end

  def current_status_name
    @current_status.to_s.humanize
  end
  def current_status
    @current_status.try(:to_sym)
  end
  def pending_approval?
    current_status == :pending_approval
  end 
  def approved?
    current_status == :approved
  end 
  def cancelled?
    current_status == :cancelled
  end 
  def declined?
    current_status == :declined
  end 

end