class User < Tenanted
  property :given_name
  property :family_name
  property :email, :validates => {:format => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i}
  property :admin, :default => false
  property :tags, :required => false, :default => []

  property :password_salt, :public => false 
  property :password_hash, :public => false
  property :auth_token, :public => false

  has :one,  :incoming, :owns, :as => :tenant, :anchor => true

  def self.with_tenant!(locator)
    locator.with '<owns!'
  end

  def full_name
    "#{given_name} #{family_name}"
  end

  def password=(password)
    # proper password checks later, for now just...
    raise ArgumentError, "Supplied password is not acceptable" unless password and password.length >= 3
    self.password_salt = BCrypt::Engine.generate_salt
    self.password_hash = BCrypt::Engine.hash_secret(password, self.password_salt)
  end 

  def before_save
    self.auth_token ||= SecureRandom.hex
  end

  def self.authenticate(email, password)
    user = locator.with_tenant!.where(email: email).first
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end

  def admin?
    # until we have data types on properties...
    admin
  end

end

