class PersonTimeline

  def initialize(person) 
    @person = person
    events = []
    person.registrations.each do |registration|
      events.push new_event registration, :registration, registration.submitter.full_name
      registration.exemption_requests.each do |exemption_request|
        events.push new_event exemption_request, :exemption_request, exemption_request.submitter.full_name
      end
    end
    person.submissions.each do |submission|
      events.push new_event submission, :submission, submission.by
      submission.decisions.each do |decision|
        events.push new_event decision, :decision, decision.by
      end
    end
    @events = events.sort! { |a,b| b.when <=> a.when }.freeze
  end

  def to_a
    @events
  end

  def most_recent_exemption_request_for(requirement)
    @events.find { |evt| evt.type == :exemption_request and evt.obj.requirement.same_as(requirement) }
  end

  def most_recent_submission_for(submittable)
    @events.find { |evt| evt.type == :submission and evt.obj.submittable.same_as(submittable) }
  end

  private 

  def new_event(obj, type, by)
    Hashie::Mash.new.tap do |event|
      event.when = obj.created_at
      event.type = type
      event.by = by
      event.obj = obj
    end
  end

end