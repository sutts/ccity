class Decision < Neon::Node
  with_timestamps
  
  has :one, :incoming, :decision,  :as => :decidable, :required => true

  property :ok
  property :by, :required => false
  property :comment, :required => false

  def ok?
    ok
  end

  def <=> other
    self.last_modified <=> other.last_modified
  end

end