module Decidable

  def decision
    decisions.sort.last
  end

  def decision_value
    d = decision
    d.nil? ? nil : d.ok?
  end

  def rejected?
    decision_value == false
  end

end