class Submission < Neon::Node
  with_timestamps
  include Decidable

  attr_accessor :current

  has :one,   :incoming, :submission, :as => :person
  has :one,   :outgoing, :submittable
  has :many,  :outgoing, :attachment, :as => :attachments, :nested => true, :clazz => Attachment
  has :many,  :outgoing, :decision, :as => :decisions

  property :number, :required => false
  property :issued_by, :required => false
  property :issue_date, :required => false, :type => Date
  property :expiry_date, :required => false, :type => Date
  property :by, :required => false

  def self.with_defaults(locator)
    locator.with_attachments
  end

  def self.with_attachments(locator)
    locator.with '>attachment'
  end

  def correct?
    entries.map(&:error).compact.empty? and not expired?
  end

  def expired?
    days_to_expiry < 0
  end

  def expiring?
    (0..30).include? days_to_expiry
  end

  def entries()
    [].tap do |list|
      [:number, :issued_by, :issue_date, :expiry_date].each do |field|
        if self.submittable.send field
          mash = Hashie::Mash.new 
          mash.property = field
          mash.name = self.submittable.send "#{field}_as"
          mash.value = self.send field 
          mash.error = :missing unless mash.value
          if mash.value and field == :expiry_date
            mash.error = :expired if self.expired?
            mash.warning = :expiring if self.expiring?
          end
          list.push mash
        end
      end
    end
  end

  private 

  def days_to_expiry
    (submittable.expiry_date and not self.expiry_date.nil?) ? self.expiry_date - Date.today : 999
  end

end
