class UserContext

  attr_reader :user, :tenant

  def initialize(user, tenant)
    raise ArgumentError unless user and tenant
    @user = user
    @tenant = tenant
    unless home?
      @applicable_contracts = Contract.between @user.tenant, @tenant
    end
  end

  def home?
    @user.tenant.same_as @tenant
  end

  def permitted?
    home? or not @applicable_contracts.empty?
  end

  def admin?
    home? and @user.admin?
  end

  def roles
    admin? ? [:admin, :approver] : []
  end

end