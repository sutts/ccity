class Position < Tenanted
  has :one,  :incoming, :owns, :as => :tenant, :anchor => true

  property :name, :mini_hash => true
  property :tags, :required => false, :default => []
end
