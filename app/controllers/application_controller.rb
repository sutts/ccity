class ApplicationController < ActionController::Base
  include UrlHelper
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :add_universal_headers
  
  def render_404
    respond_to do |format|
      format.html { render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found }
      format.xml  { head :not_found }
      format.any  { head :not_found }
    end
  end

  private

  helper_method :current_user

  def current_user
    if cookies[:auth_token]
      @current_user ||= User.locator.with_tenant!.where(auth_token: cookies[:auth_token]).first
    end
  end

  def add_universal_headers
    # issues where browser back/forward shows json when a single route is used for multiple formats (html and json)
    # this appears to fix
    response.headers["Vary"]= "Accept"
  end
  
end
