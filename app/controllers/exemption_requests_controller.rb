class ExemptionRequestsController < TenantedController

  def create
    request = ExemptionRequest.new 
    request.submitter = @current_user
    p = safe_params
    request.reason = p.fetch :reason
    request.registration = @current_tenant.find Registration, p.fetch(:registration_id)
    request.requirement = @current_tenant.find Requirement, p.fetch(:requirement_id)
    request.save
    render json: request.to_hash
  end

  private 

  def safe_params
    params.require(:exemption_request).permit!
  end

end