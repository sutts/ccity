class TenantsController < ApplicationController
  
  def index
    @tenants = TenantBroker.new.all
  end
  
  def show
    @tenant = TenantBroker.new.find params[:id].to_i
  end
  
end
