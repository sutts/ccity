class SubmissionsController < TenantedController

  def for_person
    person = @current_tenant.locator_for(Person).with_submissions_incl_submittables
                                                .find params.fetch(:person_id)
    render :json => person.submissions.map(&:to_maxi_hash)
  end

  def index
    respond_to do |type|
      type.html { 
        @person = @current_tenant.locator_for(Person) .with_registrations_incl_decisions_and_position_and_submitter
                                                      .find params.fetch(:person_id)
        @s3 = S3.new.with_signed_policy
        response.headers['Access-Control-Allow-Methods'] = 'POST'
        response.headers['Access-Control-Allow-Origin']  = @s3.host
        response.headers['Access-Control-Allow-Headers'] = 'Content-Type'
      }
      type.json { 
        person = @current_tenant.locator_for(Person).with_submissions_incl_submittables
                                                    .find params.fetch(:person_id)
        render :json => person.submissions.map(&:to_hash) 
      }
    end
  end

  def create
    create_or_update Submission.new
  end

  def update
    submission = Submission.locator.find params[:id]
    create_or_update submission
  end

  private 

  def create_or_update(submission)

    person = @current_tenant.find Person, params[:submission].fetch(:person).fetch(:id)
    submittable = @current_tenant.find Submittable, params[:submission].fetch(:submittable).fetch(:id)

    submission.person = person
    submission.submittable = submittable
    submission.assign safe_params(Submission)
    Array(params[:submission][:attachments]).each do |attachment_params|
      submission.attachments.push Attachment.new(attachment_params)
    end
    submission.by = current_user.full_name
    submission.save

    render :json => submission.to_maxi_hash

  end

  def safe_params(clazz)
    p = params[clazz.name.underscore]
    {}.tap do |hash|
      clazz.properties.each do |property|
        value = p[property.name]
        value = Date.parse value if value and property.explicit_type == Date
        hash[property.name] = value
      end
    end
  end

end
