class TenantedController < ApplicationController

	before_action :get_check_and_set_tenant

	private

  def current_tenant
    @current_tenant ||= Tenant.locator.where(short_name: request.subdomain).first
    @current_tenant_info = TenantInfo.new @current_tenant
    @current_tenant
  end
	
	def get_check_and_set_tenant
    render_404 unless current_tenant
    if current_user
      @current_user_context = UserContext.new current_user, current_tenant
      redirect_to sign_in_path, :alert => 'Permission denied' unless @current_user_context.permitted?
      @roles = @current_user_context.roles
    else
      redirect_to sign_in_path, :alert => 'Please log in'
    end
	end
	
end