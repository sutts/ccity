module Admin
  class StandardAdminController < TenantedController

    before_action :ensure_admin_user

    rescue_from 'Exception' do |exception|
      puts exception.message, exception.backtrace
      respond_to do |type|
        hash = {}
        [:class, :message].each do |attr|
          hash[attr] = exception.send attr
        end
        hash[:offender] = exception.backtrace.first
        type.json { render :json => hash, :status => 500 }
        type.html { render :template => "errors/error_404", :status => 404 }
        type.all  { render :nothing => true, :status => 404 }
      end
    end

    def index
      render :json => @current_tenant.all(clazz).map(&:to_hash)
    end

    def create
      item = @current_tenant.create clazz, safe_params
      render :json => item.to_hash
    end

    def update
      item = @current_tenant.find clazz, params[:id]
      item.assign safe_params
      maybe_call :before_update, item
      item.save
      render :json => item.to_hash
    end

    def destroy
      item = @current_tenant.find clazz, params[:id]
      item.destroy
      render :nothing => true, :status => :no_content
    end

    def rename_tag
      p = params.require(:tag).permit!
      tagster.rename p.fetch(:id), p.fetch(:name)
      render :json => params
    end

    def remove_tag
      tagster.remove params.fetch(:id)
      render :nothing => true, :status => :no_content
    end
  
    def tagster
      Tagster.new @current_tenant, clazz
    end

    private 

    def maybe_call(method_name, arg)
      self.send method_name, arg if self.respond_to? method_name
    end

    def ensure_admin_user
      raise 'PermissionDenied' unless @current_user.try(:admin?)
    end

  end
end
