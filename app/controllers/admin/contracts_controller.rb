module Admin
  class ContractsController < TenantedController

    def index
      hashes = find_contracts.map do |contract|
        contract.to_hash.tap do |hash|
          hash[:vendor][:home] = root_url :subdomain => contract.vendor.short_name
          hash[:client][:home] = root_url :subdomain => contract.client.short_name
        end
      end
      render :json => hashes
    end

  end
end

