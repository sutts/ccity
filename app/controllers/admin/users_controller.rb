module Admin
  class UsersController < StandardAdminController

    def before_update(user)
      # cheap way to ensure there is always at least one admin user for the tenant
      if user.same_as @current_user and not(user.admin == true)
        raise "Cannot remove admin permissions from self"
      end
    end

    def safe_params
      params.require(:user).permit!
    end

    def clazz
      User
    end

  end
end
