module Admin
  class PositionsController < StandardAdminController

    def clazz
      Position
    end

    def safe_params
      params.require(:position).permit!
    end

  end
end
