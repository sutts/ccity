module Admin
  class RequirementsController < StandardAdminController

    def clazz
      Requirement
    end

    def create
      req = Requirement.from_hash safe_params
      req.tenant = @current_tenant
      req.save
      render :json => req.to_hash
    end

    def update
      create
    end

    def safe_params
      params.require(:requirement).permit!
    end

  end
end
