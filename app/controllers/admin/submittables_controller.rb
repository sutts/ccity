module Admin
  class SubmittablesController < StandardAdminController

    def clazz
      Submittable
    end

    def safe_params
      params.require(:submittable).permit!
    end
  
    def rename_category
      p = params.require(:category).permit!
      tagster.rename_category p.fetch(:id), p.fetch(:name)
      render :json => params
    end

  end
end