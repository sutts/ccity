module Admin
  class ClientsController < ContractsController
    def find_contracts
      Contract.clients_of @current_tenant
    end
  end
end
