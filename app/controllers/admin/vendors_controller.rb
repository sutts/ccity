module Admin
  class VendorsController < ContractsController
    def find_contracts
      Contract.vendors_to @current_tenant
    end
  end
end
