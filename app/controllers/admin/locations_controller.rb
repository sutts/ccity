module Admin
  class LocationsController < StandardAdminController

    def clazz
      Location
    end

    def safe_params
      params.require(:location).permit!
    end

  end
end
