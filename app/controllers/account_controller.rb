class AccountController < TenantedController
  
  def show
    respond_to do |format|
      format.html
      format.json { render :json => [@current_user.to_hash] }
    end
  end
 
  def change_password
    unless User.authenticate @current_user.email, safe_password_params.fetch(:old)
      render json: { :errors => 'Incorrect password' }, status: :precondition_failed
      return
    end
    @current_user.password = safe_password_params.fetch( :new)
    @current_user.save
    render :nothing => true, :status => :no_content
  end

  def update
    if @current_user.email != params[:account][:email]
      render json: { :errors => 'Email (might be) already taken' }, status: :precondition_failed
      return
    end
    [:given_name, :family_name, :email].each do |attr|
      @current_user.send "#{attr}=", params[:account][attr]
    end
    @current_user.save
    render :json => @current_user.to_hash
  end

  private

  def safe_password_params
    params.require(:password).permit(:old, :new)
  end

end
