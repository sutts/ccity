class AdminController < TenantedController

  def index
  end

  [:positions, :certifications, :locations, :requirements, :users, :vendors, :clients, :submittables].each do |admin_entity|
    send :define_method, admin_entity do 
      display_name = admin_entity
      instance_variable_set "@entity_name_singular", admin_entity.to_s.singularize
      instance_variable_set "@entity_name_plural", admin_entity
      instance_variable_set "@display_name_singular", display_name.to_s.singularize
      instance_variable_set "@display_name_plural", display_name
    end
  end

end
