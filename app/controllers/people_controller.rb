class PeopleController < TenantedController

  def index
    @person_infos = PersonInfo.all @current_tenant_info
  end

  def show
    respond_to do |type|
      type.html { 
        @person = @current_tenant.find Person, params.fetch(:id)
        @s3 = S3.new.with_signed_policy
        response.headers['Access-Control-Allow-Methods'] = 'POST'
        response.headers['Access-Control-Allow-Origin']  = @s3.host
        response.headers['Access-Control-Allow-Headers'] = 'Content-Type'
      }
      type.json {
        @person_info = PersonInfo.find @current_tenant_info, params.fetch(:id)
      }
    end
  end

  def create
    person = Person.new safe_params
    person.tenant = @current_tenant
    person.save
    render :json => person.to_hash
  end

  def update
    person = @current_tenant.find Person, params[:id]
    person.assign safe_params
    person.save
    render :json => person.to_hash
  end

  private 

  def safe_params
    params[:person].permit(:given_name, :family_name, :middle_name, :email, :date_of_birth)
  end

end