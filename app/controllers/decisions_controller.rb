class DecisionsController < TenantedController

  def create
    clazz = Object::const_get params.fetch(:decision).fetch(:decidable).fetch(:type)
    decidable = clazz.locator.find params.fetch(:decision).fetch(:decidable).fetch(:id)
    if decidable.is_a? Decidable
      decision = Decision.new safe_params
      decision.decidable = decidable
      decision.by = @current_user.full_name
      decision.save
      render :json => decision.to_hash
    else
      raise 'Not a decidable'
    end
  end

  private 

  def safe_params
    params.require(:decision).permit(:ok, :comment)
  end

end