class SessionsController < ApplicationController

  skip_before_action :verify_authenticity_token

  def new
  end

  def create
    user = User.authenticate params[:email], params[:password]
    if user
      cookie_store = params[:remember_me] ? cookies.permanent : cookies
      cookie_store[:auth_token] = { value: user.auth_token, domain: domain_for_cookies } 
      target_url = root_url :subdomain => user.tenant.short_name
      redirect_to target_url
    else
      flash.now.alert = "Invalid email or password"
      render "new"      
    end
  end

  def destroy
    cookies.delete :auth_token, :domain => '.lvh.me'
    redirect_to sign_in_path, :notice => "Logged out"
  end

  private 

  def domain_for_cookies
    # root domain prededed by a dot so that the cookie is valid across subdomains
    ".#{ENV['APP_DOMAIN']}"
  end

end
