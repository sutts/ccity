class RequirementsController < TenantedController
  
  def index
    @ctx = RequirementsContext.new
    [:position, :location].each do |attr|
      unless params[attr].blank?
        obj = Object.const_get(attr.capitalize).new id: Integer(params[attr])
        @ctx.send "#{attr}=", obj
      end      
    end
    unless @ctx.empty?
     req_list = RequirementsList.new @current_tenant.all(Requirement)
     @applicables = req_list.applicables @ctx 
    end
  end
 
end
