class RegistrationsController < TenantedController

  def index
    locator = @current_tenant.locator_for(Registration).wide
    if params[:position].present?
      locator.find_join(:position).where! :id, params[:position].to_i 
    end
    [:given_name, :family_name].each do |name_field|
      if params[name_field].present?
        regex = "(?i)#{params[name_field]}.*"
        locator.find_join(:registration).where! name_field, regex, '=~'
      end
    end
    local_filters = {}
    local_filters[:current_status] = params[:status] if params[:status].present?
    filters = [:position, :given_name, :family_name, :status].select { |f| params[f].present? } 
    @filtered_search = not(filters.empty?)
    @registrations = locator.where local_filters
  end

  def for_person
    person = @current_tenant.locator_for(Person).with_registrations_incl_decisions_and_position_and_submitter
                                                .find params.fetch(:person_id)
    render :json => person.registrations.map(&:to_maxi_hash)
  end

  def submittables_for_position
    position = @current_tenant.find Position, params.fetch(:position_id) 
    @registration = Registration.new tenant: @current_tenant, position: position
    @engine = RegistrationEngine.for @registration
    render :json => @engine.requirements.map(&:definition).map(&:to_hash)
  end

  def new
    person = @current_tenant.find Person, params[:person]
    position = @current_tenant.find Position, params[:position]
    registration = Registration.new tenant: @current_tenant, person: person, position: position, submitter: current_user
    registration.save
    redirect_to registration_path(registration.id)
  end

  def show
    respond_to do |type|
      type.html { 
        @s3 = S3.new.with_signed_policy
      }
      type.json {
        registration = @current_tenant.locator_for(Registration).with_person.find params[:id]
        @person_info = PersonInfo.find @current_tenant_info, registration.person.id
        @registration_info = @person_info.registration_infos.find { |ri| ri.registration.id == registration.id }
        @registration = @registration_info.registration
      }
    end
  end

end