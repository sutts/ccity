json.array! PersonInfo.sort(@person_infos) do |person_info|
  json.xtract! person_info.person
  json.registrations person_info.registration_infos_sorted do |reg_info|
    json.hash! reg_info.registration, :position
    json.status reg_info.status_info
  end 
end