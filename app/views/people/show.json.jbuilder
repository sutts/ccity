json.person do

  json.xtract! @person_info.person

  json.registrations @person_info.registration_infos do |reg_info|
    json.hash! reg_info.registration, :position
    json.status reg_info.status_info
  end

  json.submissions @person_info.submission_infos do |submission_info|
    json.named_hash! :submittable, submission_info.submittable
    json.current do
      json.hash! submission_info.current, :attachments, :entries, :decision
    end
    json.previous submission_info.previous do |submission|
      json.hash! submission, :attachments, :entries, :decision
    end
  end

end

# blob for now
json.submittables @current_tenant.all(Submittable) do |submittable| 
  json.hash! submittable 
end
json.positions @current_tenant.all(Position) do |position| 
  json.hash! position
end





