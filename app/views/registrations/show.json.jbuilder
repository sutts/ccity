json.xtract! @registration_info.registration
json.status @registration_info.status_info

json.person do
  json.xtract! @registration.person 
end
json.position do
  json.xtract! @registration.position 
end
json.tasks do
  json.array! @registration_info.submitter_tasks do |task|
    json.extract! task, :problem
    json.requirement do
      json.xtract! task.requirement
    end
    json.submittable do
      json.xtract! task.requirement.submittable
    end
  end
end
json.reviews do
  json.array! @registration_info.reviewer_tasks do |task|
    json.extract! task, :problem
    json.target_id task.target.try :id
    json.requirement do
      json.xtract! task.requirement
    end
    json.submittable do
      json.xtract! task.requirement.submittable
    end
  end
end
json.timeline do
  json.array! @person_info.timeline.to_a do |evt|
    json.(evt, :type, :by, :when)
    json.obj do
      json.xtract! evt.obj
      if evt.type == :submission
        json.submittable do
          json.xtract! evt.obj.submittable
        end
        json.attachments do
          json.array! evt.obj.attachments do |attachment|
            json.xtract! attachment
          end
        end
      end
      if evt.type == :decision
        json.submission do
          json.xtract! evt.obj.decidable
          json.submittable do
            json.xtract! evt.obj.decidable.submittable
          end
        end
      end
      if evt.type == :exemption_request
        json.required_submission do
          json.xtract! evt.obj.requirement.submittable
        end
      end
    end
  end
end  

