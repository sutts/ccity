namespace :test do
  desc "Run the coffeescript tests using mocha/node (no browser anywhere in sight)"
  task :coffee do
    sh "mocha -R Spec --recursive --compilers coffee:coffee-script/register specs"
  end
end
