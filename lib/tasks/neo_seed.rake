namespace :neo do

  desc "Purge all data (dev env only, for now at least)"
  task :purge => :environment do
    unless Rails.env.development?
      raise "At this stage I think it would be prudent to reject this for anything other than dev env" 
    end
    neo_purge
  end

  desc "Populate seed data (dev env only, for now at least)"
  task :seed => :environment do
    unless Rails.env.development?
      raise "At this stage I think it would be prudent to reject this for anything other than dev env" 
    end
    neo_seed
  end


  desc "Purge purge purge"
  task :purge! => :environment do
    neo_purge
  end

  desc "Seed seed seed"
  task :seed! => :environment do
    neo_seed
  end

end

def neo_purge
    puts "Purging database..."
    Neon::Gateway.new.raw "MATCH (n) OPTIONAL MATCH (n)-[r]-() DELETE n,r"
    puts "DONE"
end

def neo_seed
    puts "Seeding data..."
    create_admin
    larry = create_larry
    dave = create_dave
    create_demo1 larry, dave
    create_demo2 larry, dave
    create_fit
    puts "DONE"
end

@ohs_card_description = 
"Also known as white or green card, the construction induction card provides proof \
of completion of general construction induction training and is a regulatory requirement for all people \
involved in 'construction work' in Australia"

def create_admin
  tenant = Tenant.create short_name: 'sysadmin', long_name: "Rekwire System Administration", admin: true
  User.create tenant: tenant, given_name: 'Adam', family_name: 'Atwell', email: 'adam@holonesque.com.au', password: 'abc', admin: true
  tenant
end


def create_larry
  tenant = Tenant.create short_name: 'larry', long_name: "Larry's Labour Suppliers"
  User.create tenant: tenant, given_name: 'Larry', family_name: 'Landsdale', email: 'larry@holonesque.com.au', password: 'abc', admin: true
  tenant
end

def create_dave
  tenant = Tenant.create short_name: 'dave', long_name: "Dave's Diggers"
  User.create tenant: tenant, given_name: 'Dave', family_name: 'Duncraig', email: 'dave@holonesque.com.au', password: 'abc', admin: true
  tenant
end

def create_demo1(*vendors)

  tenant = Tenant.create short_name: 'demo1', long_name: 'Demo Company 1'
  
  sally = User.create tenant: tenant, given_name: 'Sally', family_name: 'Noon',      email: 'sally@holonesque.com.au', password: 'abc', admin: true
  susan = User.create tenant: tenant, given_name: 'Susan', family_name: 'Nollamara', email: 'susan@holonesque.com.au', password: 'abc', admin: false
  
  electrician = Position.create tenant: tenant, name: 'Electrician',  tags: ['Trade']
  welder      = Position.create tenant: tenant, name: 'Welder',       tags: ['Trade']
  visitor     = Position.create tenant: tenant, name: 'Visitor',      tags: []

  electrician_cert      = Submittable.create  tenant: tenant, category: 'Trade licence', name: 'Electrical Licence',           
                                              attachments: true,
                                              number: true, number_as: 'License number',
                                              issued_by: true, issued_by_as: 'Licensing authority (e.g. WA)',
                                              issue_date: true, issue_date_as: 'Date of issue'
  welders_cert          = Submittable.create  tenant: tenant, category: 'Trade licence', name: 'Welders Licence',              
                                              attachments: true,
                                              number: true, number_as: 'License number',
                                              issued_by: true, issued_by_as: 'Licensing authority (e.g. WA)',
                                              issue_date: true, issue_date_as: 'Date of issue'
  fit_for_work          = Submittable.create  tenant: tenant, category: 'Medical', name: 'Fit for work',                 
                                              issued_by: true, issued_by_as: 'Name of medical centre',
                                              issue_date: true, issue_date_as: 'Date performed'
  ohs_card              = Submittable.create  tenant: tenant, category: 'Regulatory', name: 'Construction induction card',  
                                              attachments: true,
                                              number: true, number_as: 'Card number',
                                              issue_date: true, issue_date_as: 'Completion date',
                                              description: @ohs_card_description                         

  only_electricians = ReferenceFilter.new type: 'Position', references: [electrician]
  only_welders      = ReferenceFilter.new type: 'Position', references: [welder]
  only_trade        = ReferenceFilter.new type: 'Position', tags:       ['Trade']
  not_visitors      = ReferenceFilter.new type: 'Position', references: [visitor], mode: 'exclude'

  Requirement.create tenant: tenant, type: 'Submittable', when: 'upfront', mandatory: false, target: electrician_cert,  filters: [only_electricians]
  Requirement.create tenant: tenant, type: 'Submittable', when: 'upfront', mandatory: false, target: welders_cert,      filters: [only_welders]
  Requirement.create tenant: tenant, type: 'Submittable', when: 'soon',      mandatory: true,  target: ohs_card,          filters: [only_trade]
  Requirement.create tenant: tenant, type: 'Submittable', when: 'soon',      mandatory: false, target: fit_for_work,      filters: [not_visitors]

  Person.create tenant: tenant, given_name: 'Bilbo', family_name: 'Baggins', email: 'bilbo@abc.com'


  vendors.each_with_index do |vendor, index|
    Contract.create client: tenant, vendor: vendor, name: "DEMO1 ABC00#{index+1}"
  end

end

def create_demo2(*vendors)
  
  tenant = Tenant.create short_name: 'demo2', long_name: 'Demo Company 2'
  
  User.create tenant: tenant, given_name: 'Clive', family_name: 'Cottlesloe', email: 'clive@holonesque.com.au', password: 'abc', admin: true
  
  ['Jimblebar Mine', 'Jimblebar Rail', 'Yarrie Mine', 'Yarrie Rail', 'Hedland Port', 'Hedland Rail', 'Perth'].each do |location|
    tokens = location.split ' '
    tags = tokens.count > 1 ? tokens : []
    Location.create tenant: tenant, name: location, tags: tags 
  end

  File.readlines('seed_data/positions.txt').each do |line|
    Position.create tenant: tenant, name: line.strip
  end

  category = nil
  File.readlines('seed_data/quals.txt').each do |line|
    if line[0] != ' '
      category = line.strip
    else
      Submittable.create tenant: tenant, category: category, name: line.strip
    end
  end

  vendors.each_with_index do |vendor, index|
    Contract.create client: tenant, vendor: vendor, name: "DEMO2 ABC00#{index+1}"
  end

end

def create_fit()

  tenant = Tenant.create short_name: 'fitlife', long_name: 'Fitlife'
  
  User.create tenant: tenant, given_name: 'Hilary', family_name: 'Warwick', email: 'hilary@holonesque.com.au', password: 'abc', admin: true

  personal_trainer  = Position.create tenant: tenant, name: 'Personal trainer', tags: []
  yoga_instructor   = Position.create tenant: tenant, name: 'Yoga instructor',  tags: ['Group fitness trainer']
  zumba_instructor  = Position.create tenant: tenant, name: 'Zumba instructor', tags: ['Group fitness trainer']

  first_aid_cert   = Submittable.create tenant: tenant, category: 'Certifications', name: 'First Aid (Basic) certification',           
                                        attachments: true,
                                        issued_by: true, issued_by_as: 'Issued by (RTO)',
                                        issue_date: true, issue_date_as: 'Completion date',
                                        expiry_date: true, issue_date_as: 'Valid to',
                                        description:  "HLTAID002: Provide Basic Emergency Life Support "\
                                                      "(previously known as Workplace Level 1 or Emergency First Aid)"
  cpr_cert         = Submittable.create tenant: tenant, category: 'Certifications', name: 'First Aid (CPR) certification',           
                                        attachments: true,
                                        issued_by: true, issued_by_as: 'Issued by (RTO)',
                                        issue_date: true, issue_date_as: 'Completion date',
                                        expiry_date: true, issue_date_as: 'Valid to',
                                        description: "HLTAID002: Provide Cardiopulmonary Resuscitation"
  insurance        = Submittable.create tenant: tenant, category: 'Insurance', name: 'Public Liability / Professional Indemnity insurance',           
                                        attachments: true,
                                        number: true, number_as: 'Member number',
                                        issued_by: true, issued_by_as: 'Insurance company',
                                        issue_date: true, issue_date_as: 'Policy start date'
  cert_3_fitness   = Submittable.create tenant: tenant, category: 'Certifications', name: 'Certificate III in Fitness',           
                                        attachments: true,
                                        issued_by: true, issued_by_as: 'Issued by (RTO)',
                                        issue_date: true, issue_date_as: 'Completion date'
  cert_4_fitness   = Submittable.create tenant: tenant, category: 'Certifications', name: 'Certificate IV in Fitness',           
                                        attachments: true,
                                        issued_by: true, issued_by_as: 'Issued by (RTO)',
                                        issue_date: true, issue_date_as: 'Completion date'
  fitness_oz       = Submittable.create tenant: tenant, category: 'Professional Membership', name: 'Membership of Fitness Australia',           
                                        number: true, number_as: 'Member number',
                                        issue_date: true, issue_date_as: 'Date joined / last renewed'
  yoga_oz          = Submittable.create tenant: tenant, category: 'Professional Membership', name: 'Membership of Yoga Alliance or Yoga Australia',           
                                        number: true, number_as: 'Member number',
                                        issued_by: true, issued_by_as: 'Name of organisation',
                                        issue_date: true, issue_date_as: 'Date joined / last renewed'
  zumba_oz         = Submittable.create tenant: tenant, category: 'Professional Membership', name: 'Membership of Zumba Instructor Network',           
                                        attachments: true,
                                        number: true, number_as: 'ZIN number',
                                        issue_date: true, issue_date_as: 'Date joined / last renewed'

  only_personal_trainers    = ReferenceFilter.new type: 'Position', references: [personal_trainer]
  only_yoga_instructors     = ReferenceFilter.new type: 'Position', references: [yoga_instructor]
  only_zumba_instructors    = ReferenceFilter.new type: 'Position', references: [zumba_instructor]
  only_group_fitness        = ReferenceFilter.new type: 'Position', tags:       ['Group fitness trainer']

  Requirement.create tenant: tenant, type: 'Submittable', when: 'soon',     mandatory: true,  target: first_aid_cert, filters: []
  Requirement.create tenant: tenant, type: 'Submittable', when: 'soon',     mandatory: true,  target: cpr_cert,       filters: []

  Requirement.create tenant: tenant, type: 'Submittable', when: 'soon',     mandatory: false, target: insurance,      filters: [only_group_fitness]
  Requirement.create tenant: tenant, type: 'Submittable', when: 'soon',     mandatory: false, target: fitness_oz,     filters: []
  Requirement.create tenant: tenant, type: 'Submittable', when: 'upfront',  mandatory: false, target: yoga_oz,        filters: [only_yoga_instructors]
  Requirement.create tenant: tenant, type: 'Submittable', when: 'upfront',  mandatory: false, target: zumba_oz,       filters: [only_zumba_instructors]

  Requirement.create tenant: tenant, type: 'Submittable', when: 'upfront',  mandatory: false, target: cert_3_fitness, filters: [only_personal_trainers]
  Requirement.create tenant: tenant, type: 'Submittable', when: 'upfront',  mandatory: false, target: cert_4_fitness, filters: [only_personal_trainers]

end


