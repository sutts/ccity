namespace :db do
  desc "Kill the DB (purge all data!!!)"
  task :recreate do
    # probably not the "right" way to do this (shell script) but better than a separate shell script?
    sh "rake db:drop && rake db:create && rake db:migrate && rake db:schema:dump && rake db:seed"
  end
end
