task :stats => "c3:stats"

namespace :c3 do
  task :stats do
    require 'rails/code_statistics'
    ::STATS_DIRECTORIES << ["Lib", "app/lib"]
    ::STATS_DIRECTORIES << ["Nodels", "app/nodels"]
    ::STATS_DIRECTORIES << ["Coffee", "coffee/src"]
    ::STATS_DIRECTORIES << ["Coffee Tests", "coffee/test"]
    CodeStatistics::TEST_TYPES << 'Coffee Tests'
  end
end