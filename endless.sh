#!/bin/bash
while true
do
  watchify -v -c  'coffee -sc' coffee/src/manifest.coffee -o public/javascripts/bundle.js
  sleep 5
done