class Subdomain
  def self.matches?(request)
    request.subdomain.present? && request.subdomain != "www"
  end
end

Rails.application.routes.draw do
  
  root to: "welcome#index"
  
  get "sign_in" => "sessions#new", :as => "sign_in"  
  get "sign_out" => "sessions#destroy", :as => "sign_out"

  resources :sessions
  resources :users
  
  constraints(Subdomain) do

    get 'account', to: 'account#show'
    put 'account', to: 'account#update'
    put 'account/password', to: 'account#change_password'

    resources :requirements

    get "submissions/person/:person_id", to: "submissions#for_person"
    get "registrations/person/:person_id", to: "registrations#for_person"
    get "registrations/submittables/position/:position_id", to: "registrations#submittables_for_position"

    resources :people
    resources :submissions 
    resources :exemption_requests 
    resources :decisions 
    resources :registrations 

    get 'admin' => "admin#index", :as => "admin"
    
    admin_entities = [:positions, :submittables, :requirements, :users, :vendors, :clients]
    
    admin_entities.each do |refdata|
      get "admin_#{refdata}" => "admin##{refdata}", :as => "admin_#{refdata}"
    end
    
    namespace 'admin' do
      admin_entities.each do |admin_entity|
        resources admin_entity do
          collection do
            if [:positions, :users].include? admin_entity
              put     'tags/:id', action: :rename_tag
              delete  'tags/:id', action: :remove_tag
            end
            if admin_entity == :submittables
              put 'categories/:id', action: :rename_category 
            end
          end
        end
      end
    end

  end

end
