Jbuilder.key_format camelize: :lower

# arguably dubious monkey patch...
class Jbuilder
  def named_xtract!(name, node, *xtras)
    set! name do 
      xtract! node, *xtras
    end
  end
  def named_hash!(name, node, *xtras)
    set! name do 
      hash! node, *xtras
    end
  end
  def xtract!(node, *xtras)
    extract! node, *node.class.api.concat(xtras)
  end
  def hash!(node, *xtras)
    hash = node.to_jhash *xtras
    merge! hash
  end
end


