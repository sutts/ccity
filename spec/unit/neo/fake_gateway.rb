class FakeGateway

  attr_accessor :queries, :result

  def initialize(result = FakeGatewayResult.new)
    @receiveds = []
    @result = result
  end

  def run_query(q, params={})
    @receiveds << q
    @result
  end

  def received
    @receiveds.first
  end

end

class FakeGatewayResult < Neon::MappedResult

  def initialize(*models)
    @models = models
  end

end
