require_relative '../spec_helper'

describe 'Neon::Persistor' do

  let (:tx)      { instance_double 'Neon::Trans' }  

  context "in the simplest case (no relationships)" do

    let (:clazz)  { 
      Class.new(Neon::Node) do |c|
        property :name
      end
    }

    describe '#save (when the node to be saved does not yet exist)' do
      it "should just create the node and nothing else" do
        instance = clazz.new name: 'whatever'
        expect(tx).to receive(:create_node) do |node|
          expect(node).to eq instance
          node.id = 101
        end
        instance.save Neon::Persistor.new tx
      end
      it "should complain vociferously if not valid" do
        instance = clazz.new
        expect(instance.valid?).to eq false
        expect { instance.save Neon::Persistor.new tx }.to raise_error
      end
    end
    
    describe '#save (when the node to be saved does already exist)' do
      it "should just update the node and nothing else" do
        instance = clazz.new name: 'whatever', id: 101
        expect(tx).to receive(:update_node) do |node|
          expect([node, node.id]).to eq [instance, 101]
        end
        instance.save Neon::Persistor.new tx
      end
    end
    
    describe 'destroy' do
      it "should just kill the node and nothing else" do
        instance = clazz.new name: 'whatever', id: 101
        expect(tx).to receive(:remove_node) do |node|
          expect([node, node.id]).to eq [instance, 101]
        end
        instance.destroy Neon::Persistor.new tx
      end
    end

  end

  context "with mutable relationships" do

    let (:clazz)  { 
      Class.new(Neon::Node) do |c|
        c.has :many, :outgoing, :references, :mutable => true 
      end 
    }

    let (:referenced_node)  { Neon::Node.new id: 17 }
    let (:new_node)         { clazz.new references: [referenced_node] }

    describe '#save (when the node to be saved does not yet exist)' do
      it "should create the focus node plus one edge to each of the referenced nodes" do
        expect(tx).to receive(:create_node) do |node|
          expect(node).to eq new_node
          node.id = 101
        end
        expect(tx).to receive(:create_edge) do |node, ref, rel|
          expect([node, rel.name, ref, node.id, ref.id]).to eq [new_node, :references, referenced_node, 101, 17]
        end
        new_node.save Neon::Persistor.new tx
      end
      it "should complain vociferously if a referenced does not exist" do
        referenced_node.id = nil
        expect {new_node.save Neon::Persistor.new tx}.to raise_error
      end
    end

    let (:existing_node)    { clazz.new id: 101, references: [referenced_node] }
    
    describe '#save (when the node to be saved does already exist)' do
      it "should remove all (relevant) existing edges, update the focus node, and recreate the edges" do
        expect(tx).to receive(:remove_edges) do |node, rel|
          expect([node, rel.name, node.id]).to eq [existing_node, :references, 101]
        end
        expect(tx).to receive(:update_node) do |node|
          expect([node, node.id]).to eq [existing_node, 101]
        end
        expect(tx).to receive(:create_edge) do |node, ref, rel|
          expect([node, rel.name, ref, node.id, ref.id]).to eq [existing_node, :references, referenced_node, 101, 17]
        end
        existing_node.save Neon::Persistor.new tx
      end
      it "should complain vociferously if a referenced does not exist" do
        referenced_node.id = nil
        expect {existing_node.save Neon::Persistor.new tx}.to raise_error
      end
    end

    describe 'destroy' do
      it "should remove relevant edges and then kill the node" do
        expect(tx).to receive(:remove_edges) do |node, rel|
          expect([node, rel.name, node.id]).to eq [existing_node, :references, 101]
        end
        expect(tx).to receive(:remove_node) do |node|
          expect([node, node.id]).to eq [existing_node, 101]
        end
        existing_node.destroy Neon::Persistor.new tx
      end
    end

  end

  context "with immutable relationships (this is the default)" do

    let (:clazz)  { 
      Class.new(Neon::Node) do |c|
        c.has :many, :outgoing, :references
      end 
    }

    let (:referenced_node)  { Neon::Node.new id: 17 }
    let (:new_node)         { clazz.new references: [referenced_node] }
    
    describe '#save (when the node to be saved does not yet exist)' do
      it "should be exactly the same as for mutable relationships" do
        expect(tx).to receive(:create_node) { |node| node.id = 101 }
        expect(tx).to receive(:create_edge)
        new_node.save Neon::Persistor.new tx
      end
    end
    
    let (:existing_node)    { clazz.new id: 101, references: [referenced_node] }

    describe '#save (when the node to be saved does already exist)' do
      it "should update the node itself but leave the relationships intact (rather than drop and recreate)" do
        expect(tx).to receive(:update_node)
        existing_node.save Neon::Persistor.new tx
      end
    end

    describe 'destroy' do
      it "should be exactly the same as for mutable relationships" do
        expect(tx).to receive(:remove_edges)
        expect(tx).to receive(:remove_node)
        existing_node.destroy Neon::Persistor.new tx
      end
    end

  end

  context "with nested relationships" do

    let (:clazz)  { 
      Class.new(Neon::Node) do |c|
        c.has :many, :outgoing, :nesteds, :nested => true
      end 
    }
    let (:nested)  { 
      Class.new(Neon::Node) do |c|
        c.has :many, :outgoing, :references 
      end 
    }

    let (:referenced_node)  { Neon::Node.new id: 17 }
    let (:nested_node)      { nested.new references: [referenced_node] }
    let (:new_node)         { clazz.new nesteds: [nested_node]  }
  
    describe '#save (when the node to be saved does not yet exist)' do
      it "should create the focus node, recurse on nesteds, and then add edges to its nesteds" do
        expect(tx).to receive(:create_node) do |node|
          expect(node).to eq new_node
          node.id = 101
        end
        expect(tx).to receive(:create_node) do |node|
          expect(node).to eq nested_node
          node.id = 102
        end
        expect(tx).to receive(:create_edge) do |node, ref, rel|
          expect([node, rel.name, ref, node.id, ref.id]).to eq [nested_node, :references, referenced_node, 102, 17]
        end
        expect(tx).to receive(:create_edge) do |node, ref, rel|
          expect([node, rel.name, ref, node.id, ref.id]).to eq [new_node, :nesteds, nested_node, 101, 102]
        end
        new_node.save Neon::Persistor.new tx
      end
      it "should complain vociferously if a nested node references a (non-nested) node that does not already exist" do
        referenced_node.id = nil
        expect {new_node.save Neon::Persistor.new tx}.to raise_error
      end
      it "should complain vociferously if a nested node already exists" do
        nested_node.id = 102
        expect {new_node.save Neon::Persistor.new tx}.to raise_error
      end
    end

    let (:existing_nested_node)   { nested.new id: 102, references: [referenced_node] }
    let (:existing_node)          { clazz.new id: 101, nesteds: [existing_nested_node] }

    describe '#save (when the node to be saved already exists)' do

      context "when mutable" do

        before do
          clazz.relationships.detect{ |r| r.name == :nesteds}.options[:mutable] = true
        end

        it "should kill its nested nodes and their relationships before doing same as create" do
          expect(tx).to receive(:update_node) do |node|
            expect([node, node.id]).to eq [existing_node, 101]
          end
          expect(tx).to receive(:remove_nesteds) do |node, rel|
            expect([node, rel.name, node.id]).to eq [existing_node, :nesteds, 101]
          end
          expect(tx).to receive(:create_node) do |node|
            expect(node).to eq existing_nested_node
            node.id = 103
          end
          expect(tx).to receive(:create_edge) do |node, ref, rel|
            expect([node, rel.name, ref, node.id, ref.id]).to eq [existing_nested_node, :references, referenced_node, 103, 17]
          end
          expect(tx).to receive(:create_edge) do |node, ref, rel|
            expect([node, rel.name, ref, node.id, ref.id]).to eq [existing_node, :nesteds, existing_nested_node, 101, 103]
          end
          existing_node.save Neon::Persistor.new tx
        end

      end
    
      context "when immutable" do
  
        before do
          clazz.relationships.detect{ |r| r.name == :nesteds}.options[:mutable] = false
        end
  
        it "should update the node itself but leave the relationships intact (no drop and recreate)" do
          expect(tx).to receive(:update_node)
          existing_node.save Neon::Persistor.new tx
        end

      end
    
    end

    describe 'destroy' do
      it "should destroy its nesteds first and then kill itself" do
        expect(tx).to receive(:remove_nesteds) do |node, rel|
          expect([node, rel.name, node.id]).to eq [existing_node, :nesteds, 101]
        end
        expect(tx).to receive(:remove_node) do |node|
          expect([node, node.id]).to eq [existing_node, 101]
        end
        existing_node.destroy Neon::Persistor.new tx
      end
    end

  end

end
