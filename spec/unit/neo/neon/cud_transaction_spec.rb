require_relative '../spec_helper'

describe 'Neon::CudTransaction' do

  def fake_node(options = {})
    instance_double('Neon::Node').tap do |node|
      options.each { |k, v| allow(node).to receive(k).and_return v }
    end
  end

  def fake_rel(options = {})
    instance_double('Neon::Relationship').tap do |rel|
      allow(rel).to receive(:outgoing?).and_return options[:outgoing] || true
      options.each { |k, v| allow(rel).to receive(k).and_return v }
    end
  end

  let (:node_properties)  { {:some => 'map'} }
  let (:gateway)          { FakeGateway.new }
  subject                 { Neon::CudTransaction.new gateway}

  describe '#create_node' do
    it "should construct an appropriate query" do
      node = fake_node properties_to_persist: node_properties, label: 'LABEL'
      expect(node).to receive(:id=).with 101
      gateway.result = FakeGatewayResult.new fake_node(:id => 101)
      subject.create_node node
      expect(gateway.received.to_s).to eq 'CREATE (n:LABEL {map_for_n}) RETURN n' 
      expect(gateway.received.params).to eq( {:map_for_n => node_properties } )
    end
  end

  describe '#update_node' do
    it "should construct an appropriate query" do
      node = fake_node id: 101, properties_to_persist: node_properties
      subject.update_node node
      expect(gateway.received.to_s).to eq 'MATCH (n) WHERE id(n)={id_of_n} SET n={map_for_n} RETURN n' 
      expect(gateway.received.params).to eq( {:id_of_n => 101, :map_for_n => node_properties } )
    end
  end

  describe '#remove_node' do
    it "should construct an appropriate query" do
      node = fake_node id: 101
      subject.remove_node node
      expect(gateway.received.to_q).to eq 'MATCH (n) WHERE id(n)=101 DELETE n' 
    end
  end

  describe '#create_edge' do
    it "should construct an appropriate query" do
      node1 = fake_node :id => 1
      node2 = fake_node :id => 2
      rel = fake_rel :name => :likes
      subject.create_edge node1, node2, rel
      expect(gateway.received.to_q).to eq 'START n=node(1), r=node(2) MERGE (n) -[nr:likes]-> (r) RETURN n,nr,r' 
    end
  end

  describe '#remove_edges' do
    it "should construct an appropriate query" do
      node = fake_node :id => 1
      rel = fake_rel :name => :likes
      subject.remove_edges node, rel
      expect(gateway.received.to_q).to eq 'START n=node(1) MATCH (n) -[r:likes]-> (m) DELETE r' 
    end
  end

  describe '#remove_nesteds' do
    it "should, all being well, construct an appropriate query" do
      node = fake_node :id => 1, :id! => 1
      rel = fake_rel :name => :likes
      subject.remove_nesteds node, rel
      expect(gateway.received.to_q).to eq 'START n=node(1) MATCH (n) -[r:likes]-> (m) OPTIONAL MATCH (m) -[any]- (x) DELETE r,m,any' 
    end
    it "should explode if the focus node is a new record" do
      node = fake_node
      rel = fake_rel :name => :likes
      expect{subject.remove_nesteds node, rel}.to raise_error
    end
  end

end
