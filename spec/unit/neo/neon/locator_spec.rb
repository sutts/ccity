require_relative '../spec_helper'

describe 'Neon::Locator' do

  let (:gateway)  {
    mock_result = instance_double('Neon::MappedResult').as_null_object 
    FakeGateway.new mock_result 
  }

  class LocatorTest < Neon::Node
  end

  subject { Neon::Locator.new LocatorTest, gateway }

  describe "simple queries (no relationships)" do

    describe '#all' do
      it "should emit a simple match query and select returned items based on its class" do
        result = subject.all
        expect(gateway.received.to_q).to eq 'MATCH (a:LocatorTest) RETURN a' 
        expect(result).to have_received(:select).with LocatorTest
      end
    end

    describe '#where' do
      it "should augment the base query with passed filters and select returned items based on its class" do
        result = subject.where name: 'Bill', color: 'green'
        expect(gateway.received.to_q).to eq "MATCH (a:LocatorTest {name: 'Bill', color: 'green'}) RETURN a"
        expect(result).to have_received(:select).with LocatorTest
      end
    end

    describe '#find_by' do
      it "should augment the base query with passed filters same as :where but then call one_and_only on the result" do
        result = subject.find_by name: 'Bill', color: 'green'
        expect(gateway.received.to_q).to eq "MATCH (a:LocatorTest {name: 'Bill', color: 'green'}) RETURN a"
        expect(result).to have_received(:one_and_only).with LocatorTest
      end
    end

    describe '#find' do
      it "should augment the base query with an id filter and call one_and_only on the result" do
        result = subject.find 27
        expect(gateway.received.to_q).to eq 'MATCH (a:LocatorTest) WHERE id(a)=27 RETURN a' 
        expect(result).to have_received(:one_and_only).with LocatorTest
      end
    end

  end

  describe "queries with 'joins' to other nodes" do

    describe '#join' do
      
      it "should extend the base query with an additional match" do
        subject.join(:likes, direction: :left, optional: false)
        subject.all 
        expect(gateway.received.to_q).to eq 'MATCH (a:LocatorTest) '\
                                            'MATCH (a) <-[b:likes]- (c) '\
                                            'RETURN a,b,c' 
      end
      it "should default to outgoing and optional" do
        subject.join(:likes)
        subject.all 
        expect(gateway.received.to_q).to eq 'MATCH (a:LocatorTest) '\
                                            'OPTIONAL MATCH (a) -[b:likes]-> (c) '\
                                            'RETURN a,b,c' 
      end
      it "should be 'chainable'" do
        subject.join(:likes).join(:adores)
        subject.all
        expect(gateway.received.to_q).to eq 'MATCH (a:LocatorTest) '\
                                            'OPTIONAL MATCH (a) -[b:likes]-> (c) '\
                                            'OPTIONAL MATCH (c) -[d:adores]-> (e) '\
                                            'RETURN a,b,c,d,e' 
      end
      it "should be 'branchable'" do
        subject.join(:likes).join(:adores).parent.join(:detests).root.all
        expect(gateway.received.to_q).to eq 'MATCH (a:LocatorTest) '\
                                            'OPTIONAL MATCH (a) -[b:likes]-> (c) '\
                                            'OPTIONAL MATCH (c) -[d:adores]-> (e) '\
                                            'OPTIONAL MATCH (c) -[f:detests]-> (g) '\
                                            'RETURN a,b,c,d,e,f,g' 
      end
      it "should automatically adjust the sequence to non-optional matches before optionals" do
        subject.join(:likes)
        subject.join(:adores, optional: false)
        subject.all
        expect(gateway.received.to_q).to eq 'MATCH (a:LocatorTest) '\
                                            'MATCH (a) -[b:adores]-> (c) '\
                                            'OPTIONAL MATCH (a) -[d:likes]-> (e) '\
                                            'RETURN a,b,c,d,e' 
      end
      it "should play nicely with other decorations" do
        subject.join(:likes).root.find 27
        expect(gateway.received.to_q).to eq 'MATCH (a:LocatorTest) '\
                                            'WHERE id(a)=27 '\
                                            'OPTIONAL MATCH (a) -[b:likes]-> (c) '\
                                            'RETURN a,b,c' 
      end
      
      context 'with join filters' do

        describe '#where' do
          it "should append an appropriate where clause" do
            subject.join(:likes).where(:first_name, 'John').root.all 
            expect(gateway.received.to_q).to eq 'MATCH (a:LocatorTest) '\
                                                'OPTIONAL MATCH (a) -[b:likes]-> (c) '\
                                                "WHERE c.first_name = 'John' " \
                                                'RETURN a,b,c' 
          end
          it "should support arbitrary operators" do
            subject.join(:likes).where(:age, 22, '>').root.all 
            expect(gateway.received.to_q).to eq 'MATCH (a:LocatorTest) '\
                                                'OPTIONAL MATCH (a) -[b:likes]-> (c) '\
                                                "WHERE c.age > 22 " \
                                                'RETURN a,b,c' 
          end
          it "should do something special for ids (cos Cypher says so)" do
            subject.join(:likes).where(:id, 27).root.all 
            expect(gateway.received.to_q).to eq 'MATCH (a:LocatorTest) '\
                                                'OPTIONAL MATCH (a) -[b:likes]-> (c) '\
                                                'WHERE id(c)=27 '\
                                                'RETURN a,b,c' 
          end
          it "should switch from WHERE to AND as needed (cos Cypher says so)" do
            subject .join(:likes)
                    .where(:id, 27)
                    .where(:first_name, 'John')
                    .where(:last_name, 'Smith')
                    .root.all 
            expect(gateway.received.to_q).to eq 'MATCH (a:LocatorTest) '\
                                                'OPTIONAL MATCH (a) -[b:likes]-> (c) '\
                                                'WHERE id(c)=27 '\
                                                "AND c.first_name = 'John' " \
                                                "AND c.last_name = 'Smith' " \
                                                'RETURN a,b,c' 
          end
        end
        describe '#where!' do
          it "should do same as #where but also set optional=false on this join and all ancestors" do
            subject.join(:likes).join(:hates).where!(:first_name, 'John').root.all 
            expect(gateway.received.to_q).to eq 'MATCH (a:LocatorTest) '\
                                                'MATCH (a) -[b:likes]-> (c) '\
                                                'MATCH (c) -[d:hates]-> (e) '\
                                                "WHERE e.first_name = 'John' " \
                                                'RETURN a,b,c,d,e' 
          end
        end
      end

    end

    describe '#with' do
      it "should provide an alternative (more concise) syntax for specifying joins" do
        subject.with('>likes!').with('<hates').all
        expect(gateway.received.to_q).to eq 'MATCH (a:LocatorTest) '\
                                            'MATCH (a) -[b:likes]-> (c) '\
                                            'OPTIONAL MATCH (a) <-[d:hates]- (e) '\
                                            'RETURN a,b,c,d,e' 
      end
      it "should support chaining" do
        subject.with('>likes', '>hates').all
        expect(gateway.received.to_q).to eq 'MATCH (a:LocatorTest) '\
                                            'OPTIONAL MATCH (a) -[b:likes]-> (c) '\
                                            'OPTIONAL MATCH (c) -[d:hates]-> (e) '\
                                            'RETURN a,b,c,d,e' 
      end
      it "should support branching as well as chaining" do
        subject.with('>likes', ['>hates', '>detests'], ['>adores', '>loves'], '>adulates').all
        expect(gateway.received.to_q).to eq 'MATCH (a:LocatorTest) '\
                                            'OPTIONAL MATCH (a) -[b:likes]-> (c) '\
                                            'OPTIONAL MATCH (c) -[d:hates]-> (e) '\
                                            'OPTIONAL MATCH (e) -[f:detests]-> (g) '\
                                            'OPTIONAL MATCH (c) -[h:adores]-> (i) '\
                                            'OPTIONAL MATCH (i) -[j:loves]-> (k) '\
                                            'OPTIONAL MATCH (c) -[l:adulates]-> (m) '\
                                            'RETURN a,b,c,d,e,f,g,h,i,j,k,l,m' 
      end
      it "should provide a means of annotating relationships" do
        subject.with('>likes(anything in brackets is a comment)!').all
        expect(gateway.received.to_q).to eq 'MATCH (a:LocatorTest) '\
                                            'MATCH (a) -[b:likes]-> (c) '\
                                            'RETURN a,b,c' 
      end
    
    end

    describe '#find_join' do
      it "should make it easy to find a join after the event" do
        subject.with('>likes', ['>hates', '>detests'], ['>adores', '>loves'], '>adulates').all
        expect(subject.find_join(:adores).rel_name).not_to be_nil
      end
      it "should complain if there is no such join" do
        subject.with('>likes', ['>hates', '>detests'], ['>adores', '>loves'], '>adulates').all
        expect{subject.find_join(:admires)}.to raise_error
      end
    end

    describe '#with_anchor' do

      let (:anchor) { 
        node = instance_double 'Neon::Node'
        allow(node).to receive(:id!).and_return 99 
        node
      }
      it "should anchor the query with a start clause" do
        subject.with_anchor(anchor, :belongs_to, :left).all
        expect(gateway.received.to_q).to eq 'START a=node(99) '\
                                            'MATCH (a) <-[b:belongs_to]- (c:LocatorTest) '\
                                            'RETURN a,b,c' 
      end
      it "should play nicely with other decorations" do
        subject.with_anchor(anchor, :belongs_to, :left).with('>adores').find 27
        expect(gateway.received.to_q).to eq 'START a=node(99) '\
                                            'MATCH (a) <-[b:belongs_to]- (c:LocatorTest) '\
                                            'WHERE id(c)=27 '\
                                            'OPTIONAL MATCH (c) -[d:adores]-> (e) '\
                                            'RETURN a,b,c,d,e' 
      end

    end

  end

  describe 'method missing' do

    class LocatorTestForMethodMissing < Neon::Node
      def self.with_friends(locator)
        locator.with '>likes'
      end
      def self.with_friends_by_id(locator, id)
        locator.join(:likes).where! :id, id
      end
    end
    subject { Neon::Locator.new LocatorTestForMethodMissing, gateway }
    context "when the method is implemented by the associated node clazz" do
      it "should be delegated" do
        subject.with_friends.all 
        expect(gateway.received.to_q).to eq 'MATCH (a:LocatorTestForMethodMissing) '\
                                            'OPTIONAL MATCH (a) -[b:likes]-> (c) '\
                                            'RETURN a,b,c'
      end
    end
    context "when the method is implemented by the associated node clazz and takes arguments" do
      it "should be delegated along with the arguments" do
        subject.with_friends_by_id(27).all 
        expect(gateway.received.to_q).to eq 'MATCH (a:LocatorTestForMethodMissing) '\
                                            'MATCH (a) -[b:likes]-> (c) '\
                                            'WHERE id(c)=27 '\
                                            'RETURN a,b,c'
      end
    end
    context "when the method is not implemented by the associated node clazz" do
      it "should raise error" do
        expect{subject.with_enemies}.to raise_error
      end
    end

  end

  describe 'clazz#with_defaults' do

    context "when implemented by the associated node clazz" do
      class LocatorTestWithDefaults < Neon::Node
        def self.with_defaults(locator)
          locator.with '>likes'
        end
      end
      subject { Neon::Locator.new LocatorTestWithDefaults, gateway }
      it "should be invoked automatically" do
        subject.all 
        expect(gateway.received.to_q).to eq 'MATCH (a:LocatorTestWithDefaults) '\
                                            'OPTIONAL MATCH (a) -[b:likes]-> (c) '\
                                            'RETURN a,b,c'
      end
    end

  end

end
