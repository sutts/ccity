require_relative '../spec_helper'

def diff_in_ms (datetime1, datetime2)
  ((datetime1 - datetime2) * 1000).to_i.abs
end

describe Neon::Node do 

  let (:clazz) { Class.new Neon::Node }

  describe '.property and .properties' do
    it "should be tracked" do
      clazz.property :fruit
      clazz.property :color
      expect(clazz.properties.map(&:name)).to eq [:fruit, :color]
    end
    it "should generate instance accessor methods for me" do
      clazz.property :fruit
      instance = clazz.new
      instance.fruit = 'apple'
      expect(instance.fruit).to eq 'apple'
    end
  end

  describe 'property names' do
    subject {
      clazz.property :fruit
      clazz.property :color
      clazz.property :secret, :public => false
      clazz
    }
    its(:property_names)        { is_expected.to eq [:fruit, :color, :secret] }
    its(:public_property_names) { is_expected.to eq [:fruit, :color] }
    its(:api_property_names)    { is_expected.to eq [:fruit, :color, :id] }
  end

  describe '.relationship and .relationships' do
    it "should be tracked" do
      clazz.has :many, :outgoing, :likes
      clazz.has :one, :incoming, :liked_by
      expect(clazz.relationships.map(&:name)).to eq [:likes, :liked_by]
    end
    it "should generate instance accessor methods for me" do
      clazz.has :one, :outgoing, :likes
      instance = clazz.new
      instance.likes = 'Fred'
      expect(instance.likes).to eq 'Fred'
    end
  end

  describe '.label' do
    it "should infer its default value from the class name" do
      Object.const_set "MyTestNode", Class.new(Neon::Node)
      expect(MyTestNode.label).to eq 'MyTest'
    end
  end

  describe '.new' do
    subject {
      clazz.property :fruit
      clazz.has      :one, :outgoing, :likes
      clazz.new fruit: 'apple', likes: 'Fred', color: 'green'
    }
    its (:fruit) { should eq 'apple'}
    its (:likes) { should eq 'Fred'}
    context "with array types" do
      it "should initialize instance value to empty array to make everyone's life easier" do
        clazz.has :many, :outgoing, :likes
        expect(clazz.new.likes).to eq []
      end
    end
    context "with default property values" do
      it "should initialize instance values to their default to make everyone's life easier" do
        clazz.property :fruit, :default => 'apple'
        expect(clazz.new.fruit).to eq 'apple'
      end
    end
  end

  describe '.assign' do
    subject {
      clazz.property :fruit
      clazz.has      :one, :outgoing, :likes
      clazz.new.assign fruit: 'apple', likes: 'Fred', color: 'green'
    }
    its (:fruit) { should eq 'apple'}
    its (:likes) { should eq 'Fred'}
  end
  
  describe "validations" do
    it "should automatically set me up a validator on the class" do
      expect(clazz.validator).not_to be_nil
    end
    it "should, by default, treat all properties as required" do
      clazz.property :fruit
      instance = clazz.new
      expect(instance.valid?).to eq false
      expect(instance.errors).to eq( { :fruit => ['is not present'] } )
    end
    it "should exclude a property from being required if so instructed" do
      clazz.property :fruit, :required => false
      instance = clazz.new
      expect(instance.valid?).to eq true
      expect(instance.errors).to be_empty
    end
    it "should, by default, treat all relationships as _not_ required" do
      clazz.has :one, :outgoing, :fruit
      expect(clazz.new.valid?).to eq true
    end
    it "should treat a relationship as required if instructed so to do" do
      clazz.has :one, :outgoing, :fruit, :required => true
      expect(clazz.new.valid?).to eq false
      expect(clazz.new(fruit: 'apple').valid?).to eq true
    end
    it "should be easy to apply any validation provided by the veto gem to any property" do
      clazz.property :fruit, :validates => {:max_length => 5}
      expect(clazz.new(fruit: 'apple').valid?).to eq true
      expect(clazz.new(fruit: 'banana').valid?).to eq false
    end
    it "should be easy to define custom validations" do
      clazz.validations :v1, :v2
      clazz.send :define_method, :v1 do |errors|
      end
      clazz.send :define_method, :v2 do |errors| 
        errors.add :attr, 'error message'
      end
      instance = clazz.new
      expect(instance.valid?).to eq false
      expect(instance.errors.full_messages).to eq ['attr error message']
    end
    it "should, by default, not care about the validity of related objects" do
      apple = instance_double 'Neon::Node'
      clazz.has :one, :outgoing, :fruit, :required => true
      expect(clazz.new(fruit: apple).valid?).to eq true
    end
    context "where a related object is flagged as nested" do
      let (:good) {
        instance_double('Neon::Node').tap do |double|
          allow(double).to receive(:valid?).and_return true
        end        
      }
      let (:bad) {
        instance_double('Neon::Node').tap do |double|
          errors = double 'some error collection object'
          allow(double).to receive(:valid?).and_return false
          allow(double).to receive(:errors).and_return errors
          allow(errors).to receive(:full_messages).and_return [ 'shit happens' ]
        end
      }
      subject {
        clazz.has :one, :outgoing, :fruit, :required => true, :nested => true
        clazz.new
      }
      it "should be valid if nested objects are valid" do
        subject.fruit = good
        expect(subject.valid?).to eq true
      end
      it "should be invalid if at least one nested object is not valid" do
        subject.fruit = [good, bad]
        expect(subject.valid?).to eq false
      end
      it "should include error messages from nested" do
        subject.fruit = bad
        expect(subject.valid?).to eq false
        expect(subject.errors).to eq( { :fruit => [['shit happens']] } ) 
      end
    end
  end

  describe "persistence" do

    subject {
      clazz.property :name
      clazz.property :birthday, :type => Date
      clazz.property :timestamp, :type => DateTime, :required => false
      clazz.property :description, :required => false
      clazz.new
    }

    describe '#properties_to_persist' do
      context "when not valid" do
        it "should complain vociferously" do
          expect{subject.properties_to_persist}.to raise_error
        end
      end
      context "when valid" do
        it "should be a map of all the property values" do
          subject.assign name: 'Bill', birthday: Date.new, description: 'awesome'
          expect(subject.properties_to_persist).to include :name => 'Bill', :description => 'awesome'
        end
        it "should additionally include the class name to support reconstruction from a result set" do
          subject.assign name: 'Bill', birthday: Date.new
          expect(subject.properties_to_persist).to include :class => clazz.name
        end
        it "should convert dates to structured date strings cos Neo4J doesn't do dates" do
          subject.assign name: 'Bill', birthday: Date.new(1949, 10, 7)
          expect(subject.properties_to_persist).to include :birthday => '1949-10-07'
        end
        it "should convert datetimes to structured datetime strings cos Neo4J doesn't do dates" do
          date_time_string = "2014-10-18T18:34:09+08:00"
          date_time = DateTime.iso8601 date_time_string
          subject.assign name: 'Bill', birthday: Date.new, timestamp: date_time
          expect(subject.properties_to_persist).to include :timestamp => date_time_string
        end
        describe "timestamps" do
          context "with timestamps disabled (default)" do
            it "should not generate timestamps" do
              subject.assign name: 'Bill', birthday: Date.new
              expect(subject.properties_to_persist[:created_at]).to be_nil
              expect(subject.properties_to_persist[:last_modified]).to be_nil
            end
          end
          context "with timestamps enabled" do
            before :each do
              clazz.with_timestamps
              subject.assign name: 'Bill', birthday: Date.new
            end
            it "should additionally include created_at and last_modified as DateTime strings" do
              expect(subject.properties_to_persist[:created_at]).not_to be_nil
              expect(subject.properties_to_persist[:last_modified]).not_to be_nil
            end
            it "should also set class instance variables" do
              subject.properties_to_persist
              expect(subject.created_at).not_to be_nil
              expect(subject.last_modified).not_to be_nil
            end
            describe "values for created_at and last_modified" do
              context "on first call" do
                it "should both be the current time" do
                  now = DateTime.now
                  subject.properties_to_persist
                  expect(subject.created_at).to eq subject.last_modified
                  expect(diff_in_ms(now, subject.created_at) < 10).to eq true
                end
              end
              context "on subsequent calls" do
                it "should update last_modified but leave created_at as is" do
                  now = DateTime.now
                  subject.properties_to_persist
                  created_at = subject.created_at
                  last_modified = subject.last_modified
                  subject.properties_to_persist
                  expect(subject.created_at).to eq created_at
                  expect(subject.last_modified).not_to eq last_modified
                end
              end
            end
          end
        end
      end
    end

    describe '#restore_properties' do
      it "should mass assign the passed attributes" do
        subject.restore_properties name: 'Bill', description: 'awesome'
        expect(subject.name).to eq 'Bill'
        expect(subject.description).to eq 'awesome'
      end
      it "should only touch those properties that are passed in (and hence not reset default values to nil)" do
        subject.description = 'whatever'
        subject.restore_properties name: 'Bill'
        expect(subject.description).to eq 'whatever'
      end
      it "should convert date strings back to dates" do
        subject.restore_properties birthday: '1949-10-07'
        expect(subject.birthday.year).to eq 1949
      end
      it "should convert datetime strings back to datetimes" do
        date_time_string = "2014-10-18T18:34:09+08:00"
        date_time = DateTime.iso8601 date_time_string
        subject.restore_properties timestamp: date_time_string
        expect(subject.timestamp).to eq date_time
      end
    end
  
  end

  describe '#to_hash' do
    it "should, by default, contain all properties plus id" do
      clazz.property :first_name
      clazz.property :description
      instance = clazz.new first_name: 'Bill' 
      expect(instance.to_hash).to eq( {:id => nil, first_name: 'Bill', :description => nil } )
    end  
    it "should exclude properties flagged as private" do
      clazz.property :first_name
      clazz.property :secret, :public => false
      instance = clazz.new first_name: 'Bill', secret: 42 
      expect(instance.to_hash).to eq( {:id => nil, first_name: 'Bill' } )
    end  
  end

  describe '#to_mini_hash' do
    it "should, by default, contain just type and id" do
      Object.const_set "MyTestNode1", Class.new(Neon::Node)
      instance = MyTestNode1.new id: 1
      expect(instance.to_mini_hash).to eq({:type => 'MyTestNode1', :id => 1})
    end  
    it "should contain any additional properties so flagged" do
      Object.const_set "MyTestNode2", Class.new(Neon::Node)
      MyTestNode2.property :name, :mini_hash => true
      instance = MyTestNode2.new id: 1, name: 'Test'
      expect(instance.to_mini_hash).to eq({:type => 'MyTestNode2', :id => 1, :name => 'Test'})
    end  
  end
  
  describe '#to_maxi_hash' do
    before :all do
      Object.const_set "MyTestNode3", Class.new(Neon::Node)
      MyTestNode3.property :name
      MyTestNode3.has :many, :outgoing, :likes, :as => :friends
    end
    let (:instance1) { MyTestNode3.new id: 1, name: 'i1' }
    let (:instance2) { MyTestNode3.new id: 2, name: 'i2' }
    it "should contain same as to_hash plus rels" do
      expected = {
        id: 1,
        name: 'i1',
        friends: []
      }
      expect(instance1.to_maxi_hash).to eq expected
    end  
    it "should recurse" do
      instance1.friends = [instance2]
      expected = {
        id: 1,
        name: 'i1',
        friends: [ {
          id: 2,
          name: 'i2',
          friends: []
        } ] 
      }
      expect(instance1.to_maxi_hash).to eq expected
    end  
    it "should automagically switch to to_hash to avoid endless loops" do
      instance1.friends = [instance2]
      instance2.friends = [instance1]
      expected = {
        id: 1,
        name: 'i1',
        friends: [ {
          id: 2,
          name: 'i2',
          friends: [ { 
            id: 1,
            name: 'i1'
          } ]
        } ] 
      }
      expect(instance1.to_maxi_hash).to eq expected
    end  
  end

  describe '#rel' do
    context "with cardinality of 'one'" do
      subject { 
        c = Class.new(Neon::Node) do |c| 
          c.has :one, :outgoing, :likes, as: :best_friend
          c.has :one, :incoming, :likes, as: :best_friend_of 
        end
        c.new 
      }
      it "should provide a generic relationship accessor method for me" do
        subject.rel :likes, :outgoing, 'Fred'
        expect(subject.best_friend).to eq 'Fred'
      end
      it "should be direction aware" do
        subject.rel :likes, :incoming, 'Bill', :strict
        expect(subject.best_friend_of).to eq 'Bill'
      end
      it "should overwrite previous value" do
        subject.rel :likes, :outgoing, 'Fred'
        subject.rel :likes, :outgoing, 'Bill'
        expect(subject.best_friend).to eq 'Bill'
      end
      it "should ignore unknown relationships by default" do
        subject.rel :despises, :outgoing, 'Fred'
      end
      it "should complain about unknown relationships if told to do so" do
        expect {subject.rel :despises, :outgoing, 'Fred', :strict}.to raise_error
      end
    end
    context "with cardinality of 'many'" do
      subject { 
        c = Class.new(Neon::Node) do |c| 
          c.has :many, :outgoing, :likes, as: :friends
        end
        c.new 
      }
      it "should provide a generic relationship accessor method for me" do
        subject.rel :likes, :outgoing, 'Fred'
        expect(subject.friends).to eq ['Fred']
      end
      it "should replace value if passed argument is an array" do
        subject.rel :likes, :outgoing, ['Fred'], true
        subject.rel :likes, :outgoing, ['Bill'], true
        expect(subject.friends).to eq ['Bill']
      end
      it "should collect values if passed argument is not an array" do
        subject.rel :likes, :outgoing, 'Fred'
        subject.rel :likes, :outgoing, 'Bill'
        expect(subject.friends).to eq ['Fred', 'Bill']
      end
    end
  end

end