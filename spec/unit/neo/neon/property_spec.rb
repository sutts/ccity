require_relative '../spec_helper'

describe Neon::Property do

  let (:clazz) { Class.new }

  describe 'name and options' do

    it "should take a name and some options" do
      attr = Neon::Property.new clazz, :fruit
      expect(attr.name).to eq :fruit
    end

    describe '#required' do
      it "should default to true" do
        attr = Neon::Property.new clazz, :fruit
        expect(attr.required?).to eq true
      end
      it "can be set to false" do
        attr = Neon::Property.new clazz, :fruit, required: false
        expect(attr.required?).to eq false
      end
    end

  end

  describe "instance accessor methods" do 

    let (:instance) { Neon::Property.new clazz, :fruit
                      Neon::Property.new clazz, :color, type: String
                      Neon::Property.new clazz, :rank, type: Integer
                      Neon::Property.new clazz, :active, type: :boolean
                      clazz.new }

    it "should generate instance accessor methods on the passed class" do
      instance.fruit = 'apple'
      expect(instance.fruit).to eq 'apple'
    end
    it "should enforce type safety where a type is specified" do
      instance.color = 'green'
      instance.color = nil
      expect{instance.color=27}.to raise_error
      instance.rank = 27
      instance.rank = nil
      expect{instance.rank='27'}.to raise_error
      instance.active = true
      instance.active = nil
      expect{instance.active='true'}.to raise_error
    end
    describe "bang getters" do
      it "should behave sames as non-bang when a value is set" do
        instance.fruit = 'apple'
        expect(instance.fruit!).to eq 'apple'
      end
      it "should complain if a value is not set" do
        expect{instance.fruit!}.to raise_error
      end
    end

  end  

end
  
