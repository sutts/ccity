require_relative '../spec_helper'

class TestNodeForMappedResult < Neon::Node
  property :name
  has      :many, :outgoing, :likes
  has      :many, :incoming, :likes, as: :liked_by
end

def fake_query_result(nodes, *relationships)
  query_result = instance_double('Neon::QueryResult')
  allow(query_result).to receive(:nodes).and_return nodes
  allow(query_result).to receive(:relationships).and_return relationships
  query_result
end

def fake_query_result_with_nodes_only(how_many)
  nodes = []
  how_many.times { |i| nodes << fake_query_result_node }
  fake_query_result nodes
end

def fake_query_result_node(id=99, props = {})
  properties = { 'class' => 'TestNodeForMappedResult', 'name' => 'John' }
  props.each { |k, v| properties[k.to_s] = v }
  node = instance_double 'Neon::GraphletNode'
  allow(node).to receive(:data).and_return properties
  allow(node).to receive(:id).and_return id
  node
end

def fake_query_result_edge(start_node, type, end_node)
  edge = instance_double 'Neon::GraphletRelationship'
  allow(edge).to receive(:start_node).and_return start_node
  allow(edge).to receive(:end_node).and_return end_node
  allow(edge).to receive(:type).and_return type
  edge
end

describe Neon::MappedResult do

  describe "mapping query result nodes to model nodes" do
    context "happy path" do
      subject { 
        node1 = fake_query_result_node 27, name: 'Bill'
        node2 = fake_query_result_node 28, name: 'Fred'
        result = fake_query_result [node1, node2]
        Neon::MappedResult.new result
      }
      its('models.count')              { should eq 2 } 
      its('models.first.class.name')   { should eq 'TestNodeForMappedResult' }
      its('models.first.id')           { should eq 27 }
      its('models.first.name')         { should eq 'Bill' }
      its('models.last.id')            { should eq 28 }
      its('models.last.name')          { should eq 'Fred' }
    end
    context "unhappy path" do
      it "should explode if it can't instantiate model class" do
        bad_node = fake_query_result_node 99, class: 'garbage_or_nil'
        bad_result = fake_query_result [bad_node]
        expect{Neon::MappedResult.new bad_result}.to raise_error 
      end
    end
  end

  describe "mapping query result relationships to model attributes" do
    context "happy path" do
      subject { 
        bill = fake_query_result_node 27, name: 'Bill'
        fred = fake_query_result_node 28, name: 'Fred'
        edge = fake_query_result_edge bill, 'likes', fred
        result = fake_query_result [bill, fred], edge 
        Neon::MappedResult.new result 
      }
      it "should hook up the nodes for me" do
        expect(subject.models.count).to eq 2
        bill = subject.models.first
        fred = subject.models.last
        expect(bill.likes).to eq [fred]
        expect(fred.liked_by).to eq [bill]
      end
    end
  end

  describe '#one_and_only' do
    it "should return the first model where there is only model" do
      result = Neon::MappedResult.new fake_query_result_with_nodes_only(1)
      expect(result.one_and_only(TestNodeForMappedResult).name).to eq 'John'
    end
    it "should complain if there is not one and only one model" do
      result = Neon::MappedResult.new fake_query_result_with_nodes_only(2)
      expect{result.one_and_only(TestNodeForMappedResult).name}.to raise_error
    end
  end

end
