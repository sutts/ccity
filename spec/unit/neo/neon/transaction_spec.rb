require_relative '../spec_helper'

describe Neon::Transaction do

  it "should take a bunch of queries and then run 'em one by one, augmenting context as it goes (no actual transaction as yet)" do

    gateway = instance_double Neon::Gateway
    mapped_result = instance_double Neon::MappedResult
    model = instance_double Neon::Node
    q1 = instance_double Neon::CypherQuery
    q2 = instance_double Neon::CypherQuery

    allow(gateway).to receive(:raw).and_return mapped_result
    allow(mapped_result).to receive(:models).and_return [model]
    allow(model).to receive(:id).and_return 27

    expect(q1).to receive(:to_s)
    expect(q1).to receive(:params).with({})
    expect(q2).to receive(:to_s)
    expect(q2).to receive(:params).with({:some_entity => 27})

    Neon::Transaction.tx gateway do |tx|
      tx.query q1, :some_entity
      tx.query q2
    end

  end

end
