require_relative '../spec_helper'

describe Neon::Relationship do

  let (:clazz) { Class.new }

  describe 'name and options' do

    it "takes quite a few parameters" do
      rel = Neon::Relationship.new clazz, :likes, :many, :outgoing
      expect(rel.name).to eq :likes
      expect(rel.cardinality).to eq :many
      expect(rel.direction).to eq :outgoing
    end

    describe '#as' do
      it "should default to the relationship name" do
        rel = Neon::Relationship.new clazz, :likes, :many, :outgoing
        expect(rel.as).to eq :likes
      end
      it "can be specified explicitly" do
        rel = Neon::Relationship.new clazz, :likes, :many, :outgoing, as: :favorites
        expect(rel.as).to eq :favorites
      end
    end

  end
 
  describe "instance accessor methods" do 

    context "when accessor name is unambiguous" do
      let (:instance) { 
        Neon::Relationship.new clazz, :likes, :many, :outgoing
        clazz.new 
      }
      it "should generate instance accessor methods on the passed class" do
        instance.likes = ['apple']
        expect(instance.likes).to eq ['apple']
      end
      describe "bang getter" do
        it "should behave sames as non-bang when a value is set" do
          instance.likes = ['apple']
          expect(instance.likes!).to eq ['apple']
        end
        it "should complain if a value is not set" do
          expect{instance.likes!}.to raise_error
        end
      end
      describe "id getters and setters" do
        it "should additionally generate a getter by id" do
          instance.likes = [Neon::Node.new(id: 1), Neon::Node.new(id: 2)]
          expect(instance.likes_by_id).to eq [1,2]
        end
        it "should additionally generate a setter by id" do
          instance.likes_by_id = [1,2]
          expect(instance.likes_by_id).to eq [1,2]
        end
        it "should expand ids to placeholder objects when setting by id" do
          instance.likes_by_id = [1,2]
          expect(instance.likes.first.id).to eq 1
        end
      end
    end
    context "when names are correctly disambiguated" do
      let (:instance) { 
        Neon::Relationship.new clazz, :begat, :many, :outgoing, as: :sons
        Neon::Relationship.new clazz, :begat, :one, :incoming, as: :father
        clazz.new 
      }
      it "should not get entangled" do
        instance.father = 'Dad'
        instance.sons = ['Junior']
        expect(instance.father).to eq 'Dad'
        expect(instance.sons).to eq ['Junior']
      end
    end
    pending "when names conflict" do
      it "should explode" do
        Neon::Relationship.new clazz, :begat, :many, :outgoing
        expect { Neon::Relationship.new clazz, :begat, :one, :incoming }.to raise_error
      end
    end
  
  end  

end
