require_relative '../spec_helper'

describe Neon::CypherQuery do 

  it "should be able to construct a simple MATCH for me" do
    subject.match.node :n
    subject.return subject.nodes.first.symbol
    expect(subject.to_s).to eq "MATCH (n) RETURN n"
  end

  describe "the return method (which is a dangerous name for a method but heck that's what it's called in cypher so there)" do
    it "should be possible to flag items for return as they are added" do
      subject.match.node(:a, return: true).node(:b, return: true).return
      expect(subject.to_s).to eq "MATCH (a) (b) RETURN a,b"
    end
    it "should complain if there is nothing to return" do
      expect{subject.match.node(:n).return.to_s}.to raise_error
    end
    context "with explicit value" do
      it "should override any items flagged as return" do
        expect(subject.match.node(:n, return: true) .return('banana').to_s).to eq "MATCH (n) RETURN banana"
      end
      it "should not complain that there is nothing to return" do
        expect(subject.match.node(:n) .return('banana').to_s).to eq "MATCH (n) RETURN banana"
      end
    end
  end

  describe '#auto_return' do
    context "when switched on (default)" do 
      it "should append a noargs return automatically" do
        subject.match.node :n, return: true
        expect(subject.to_s).to eq "MATCH (n) RETURN n"
      end
    end
    context "when switched off" do
      it "should not do any automatic appending" do
        subject.auto_return(false).match.node :n
        expect(subject.to_s).to eq "MATCH (n)"
      end
    end
  end

  describe '#delete' do
    it "should behave exactly the same as return (except that it spits out 'DELETE' rather than 'RETURN')" do
      subject.match.node(:n, return: true).delete
      expect(subject.to_s).to eq "MATCH (n) DELETE n"
    end
  end

  describe '#node' do

    context "with labels" do
      it "should be possible to specify node labels" do
        subject.match.node :n, label: 'Fruit', return: true
        expect(subject.to_s).to eq "MATCH (n:Fruit) RETURN n"
      end
    end

    context "with properties" do 
      context "when specified individually" do
        before do
          subject.auto_return false
          subject.node :x do |node|
            node.property :name, 'Bill'
            node.property_blob "color: 'green'"
            node.property :fruit, 'apple'
          end
        end 
        its(:to_s)   { should eq "(x {name: {name_for_x}, color: 'green', fruit: {fruit_for_x}})" }
        its(:params) { should eq({:name_for_x => 'Bill', :fruit_for_x => 'apple'}) }
        its(:to_q)   { should eq "(x {name: 'Bill', color: 'green', fruit: 'apple'})" }
      end
      context "when specified as a single map" do
        before do
          subject.auto_return false
          subject.node :x, properties: {:name => 'Bill', :fruit => 'apple'}
        end 
        its(:to_s)   { should eq "(x {map_for_x})" }
        its(:params) { should eq({:map_for_x => {:name => 'Bill', :fruit => 'apple'}}) }
      end
    end

    context "with blob" do
      it "should be possible to throw in a string blob" do
        subject.auto_return false
        subject.node :x, label: 'Label', blob: "{this is a blob}" do |node|
          node.property :fruit, 'apple'
        end
        expect(subject.to_s).to eq "(x:Label {this is a blob} {fruit: {fruit_for_x}})"
      end
    end
  end

  describe '#node!' do
    it "should do same as #node but auto set the return flag" do
      expect(subject.match.node!(:n).to_s).to eq 'MATCH (n) RETURN n'
    end
  end

  describe '#id' do
    before do
      subject.auto_return false
      subject.match.node(:x).where.id(:x, 27).match.node(:y).where.id(:y, '28')
    end
    its(:to_s)   { should eq "MATCH (x) WHERE id(x)={id_of_x} MATCH (y) WHERE id(y)={id_of_y}" }
    its(:params) { should eq({:id_of_x => 27, :id_of_y => 28}) }
  end

  describe '#var' do
    before do
      subject.auto_return false
      subject.match.node(:x).where.var(:var_name, 'A').s('IN x.some_multi_value_property')
    end
    its(:to_s)   { should eq "MATCH (x) WHERE {var_name} IN x.some_multi_value_property" }
    its(:params) { should eq({:var_name => 'A'}) }
  end

  describe '#start_with_node' do
    before do
      subject.auto_return(false).start_with_node :n, 27
    end
    its(:to_s)   { should eq "START n=node({n_id})" }
    its(:params) { should eq({:n_id => 27}) }
  end

  describe '#start_with_nodes' do
    before do
      subject.auto_return(false).start_with_nodes [:n, 27], [:m, 28]
    end
    its(:to_q)   { should eq "START n=node(27), m=node(28)" }
  end

  describe '#set' do
    context "with implicit variable name" do
      before do
        subject.match.node(:n, return: true).set(:n, :fruit, 'apple')
      end
      its(:to_s)   { should eq "MATCH (n) SET n.fruit={set_fruit_for_n} RETURN n" }
      its(:params) { should eq({:set_fruit_for_n => 'apple'}) }
    end
  end

  describe '#set_map' do
    before do
      properties = {:fruit => 'apple'}
      subject.match.node(:n, return: true).set_map(:n, properties)
    end
    its(:to_s)   { should eq "MATCH (n) SET n={map_for_n} RETURN n" }
    its(:params) { should eq({:map_for_n => {:fruit => 'apple'}}) }
  end

  describe '#relationship' do
    before { subject.auto_return false }
    it "should be possible to specify a simple relationship" do
      subject.match.node(:a).relationship(:r).node(:b)
      expect(subject.to_s).to eq 'MATCH (a) -[r]-> (b)'
    end 
    it "should be possible to suppress the symbol if you really want to" do
      subject.match.node(:a).relationship(nil).node(:b)
      expect(subject.to_s).to eq 'MATCH (a) --> (b)'
    end 
    it "should be possible to specify a relationship type" do
      subject.match.node(:a).relationship(:r, type: 'OWNS').node(:b)
      expect(subject.to_s).to eq 'MATCH (a) -[r:OWNS]-> (b)'
    end
    it "should be possible to flag the relationship for return" do
      subject.auto_return true
      subject.match.node(:a).relationship(:r, return: true).node(:b)
      expect(subject.to_s).to eq 'MATCH (a) -[r]-> (b) RETURN r'
    end
    it "should be possible to flip the direction" do
      subject.match.node(:a).relationship(:r, arrow: :left).node(:b)
      expect(subject.to_s).to eq 'MATCH (a) <-[r]- (b)'
    end
    it "should be possible to omit the direction" do
      subject.match.node(:a).relationship(:r, arrow: :none).node(:b)
      expect(subject.to_s).to eq 'MATCH (a) -[r]- (b)'
    end
  end

  describe '#relationship!' do
    it "should do same as #relationship but auto set the return flag" do
      subject.match.node(:a).relationship!(:r).node(:b)
      expect(subject.to_s).to eq 'MATCH (a) -[r]-> (b) RETURN r'
    end
  end

  describe '#find_elements' do
    before { subject.node(:a).relationship(:r).node(:a) }
    it "should be possible to find all occurrences of a node by symbol" do
      expect(subject.find_elements(:a).count).to eq 2
    end
    it "should be possible to find all occurrences of a relationship by symbol" do
      expect(subject.find_elements(:r).count).to eq 1
    end
  end

  describe '#for_each' do
    context "with string literal" do
      before do
        subject.auto_return(false).for_each :value, "['Apple', 'Kiwi']" do
          subject.create.node :n, label: 'Fruit', return: true, blob: '{name:value}'
        end 
      end
      its(:to_s)   { should eq "FOREACH ( value IN ['Apple', 'Kiwi'] | CREATE (n:Fruit {name:value}) )" }
      its(:params) { should be_empty }
    end
    context "with array parameter" do
      before do
        subject.auto_return(false).for_each :value, ['Apple', 'Kiwi'] do
          subject.create.node :n, label: 'Fruit', return: true, blob: '{name:value}'
        end 
      end
      its(:to_s)   { should eq "FOREACH ( value IN {for_each_values} | CREATE (n:Fruit {name:value}) )" }
      its(:params) { should eq({:for_each_values => ['Apple', 'Kiwi']}) }
    end
  end

  describe "fiddling with stuff after it was added" do
    it "should be possible to add e.g. properties to a node long after it was created" do
      subject.auto_return(false).match.node(:a).relationship(:r).node(:b)
      subject.find_elements(:a).first.property :fruit, 'apple'
      subject.find_elements(:r).first.options[:type] = 'has'
      expect(subject.to_s).to eq "MATCH (a {fruit: {fruit_for_a}}) -[r:has]-> (b)"
    end
  end

  describe "late binding of params" do
    before do
      deferred_param = subject.deferred_parameter :banana
      subject.match.node(:n, return: true).where.id(:n, deferred_param)
    end
    context "when unresolved" do
      it "should complain vociferously" do
        expect{subject.params}.to raise_error
      end
    end
    context "when resolved" do
      it "should work same as if the parameter value had been supplied from the get-go" do
        ctx = {:banana => 27}
        expect(subject.to_s).to eq 'MATCH (n) WHERE id(n)={id_of_n} RETURN n'
        expect(subject.params(ctx)).to eq({:id_of_n => 27}) 
      end
    end
  end

  describe '#start_with_deferred' do
    it "should be more convenient than start_with_node id query.deferred etc." do
      subject.auto_return(false).start_with_deferred(:n, :var_name)
      expect(subject.to_s).to eq 'START n=node({n_id})'
      ctx = {:var_name => 27}
      expect(subject.params(ctx)).to eq({:n_id => 27})
    end
  end

  describe '#next_symbol' do
    it "should return the passed symbol if that symbol is not in use" do
      expect(subject.next_symbol(:a)).to eq :a
    end
    it "should move on to :b if :a is in use" do
      subject.match.node(:a).match.node(:b)
      expect(subject.next_symbol(:a)).to eq :c
    end
  end

  describe '#reserve_symbol' do
    it "should behave same as next_symbol but also mark the returned symbol as in use" do
      expect(subject.reserve_symbol).to eq :a
      expect(subject.reserve_symbol).to eq :b
    end
  end

  describe '#symbols' do
    it "should return the last symbol in the chain" do
      subject.match.node(:a).match.node(:a).rel(:c).where.optional
      expect(subject.symbols).to eq [:a, :a, :c]
    end
  end

end