require_relative '../spec_helper'

def fake_result(*elements)
  hash = {}
  hash['data'] = []
  hash['data'] << elements
  Neon::QueryResult.new hash
end

def fake_node(id, data = {})
  {
    'self' => "http://somehost:7474/db/data/node/#{id}",
    'data' => data
  }
end

def fake_edge(start_id, end_id, type, data = {})
  {
    'start' => "http://somehost:7474/db/data/node/#{start_id}",
    'end' => "http://somehost:7474/db/data/node/#{end_id}",
    'type' => type,
    'data' => data
  }
end

describe Neon::QueryResult do 
  
  context "with just a single node" do
    subject { 
      fake_result fake_node(5, name: 'bill')
    }
    # query result is an array of nodes and relationships 
    its("count")                    {should eq 1}
    its("first.id")                 {should eq 5}
    its("first.to_mash.name")       {should eq 'bill'}
    # access by type
    its("nodes.first.to_mash.name") {should eq 'bill'}
    its("relationships.count")      {should eq 0}
  end
  
  context "with 2 nodes and one edge (but both nodes and edges potentially reported many times)" do
    subject {
      fake_result fake_node(5, name: 'bill'), 
                  fake_edge(5, 6, 'DESPISES'), 
                  fake_node(6, name: 'fred'),
                  fake_node(5, name: 'bill'), 
                  fake_node(5, name: 'bill'), 
                  fake_edge(5, 6, 'DESPISES'), 
                  fake_node(6, name: 'fred')
    }
    its("count")                {should eq 3}
    its("nodes.count")          {should eq 2}
    its("relationships.count")  {should eq 1}
    it "should join up the dots for me" do
      node1 = subject[0]
      node2 = subject[2]
      rel = subject[1]
      expect(rel.start_node).to eq node1
      expect(rel.end_node).to eq node2
      expect(rel.type).to eq 'DESPISES'
      expect(node1.outgoing).to eq [rel]
      expect(node2.incoming).to eq [rel]
    end
  end


end
