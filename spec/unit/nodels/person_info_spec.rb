require_relative './spec_helper'

describe Person do 
  
  let(:submittable1)    { Submittable.new category: 'Cat 2', name: 'Item 2', number: true, number_as: 'Fruit' }
  let(:submittable2)    { Submittable.new category: 'Cat 2', name: 'Item 1', number: true, number_as: 'Vegetable'}
  let(:submittable3)    { Submittable.new category: 'Cat 1', name: 'Item 1', number: true, number_as: 'Color'}
  let(:submission1a)    { Submission.new submittable: submittable1, created_at: DateTime.parse('10 Dec 2014'), number: 'banana' }
  let(:submission1b)    { Submission.new submittable: submittable1, created_at: DateTime.parse('11 Dec 2014'), number: 'banana' }
  let(:submission2)     { Submission.new submittable: submittable2, created_at: DateTime.parse('12 Dec 2014'), number: 'carrot' }
  let(:submission3)     { Submission.new submittable: submittable3, created_at: DateTime.parse('13 Dec 2014'), number: 'pink' }
  let(:submissions)     { [submission3, submission1b, submission2, submission1a ] }
  
  describe '#submission_infos' do
    
    subject { 
      person = Person.new submissions: submissions
      tenant_info = instance_double TenantInfo
      PersonInfo.new(tenant_info, person).submission_infos
    }

    it "should group submissions by submittable" do
      expect(subject.count).to eq 3
    end

    it "should sort top-level by submission category and then name" do
      full_names = subject.map { |submission| submission.submittable.full_name }
      expect(full_names).to eq ['Cat 1 / Item 1', 'Cat 2 / Item 1', 'Cat 2 / Item 2']
    end

    it "should sort submissions within a group by their submission date (most recent first)" do
      expect(subject.last.submittable).to be submittable1
      expect(subject.last.current).to be submission1b
      expect(subject.last.previous).to eq [submission1a]
    end

  end

end
