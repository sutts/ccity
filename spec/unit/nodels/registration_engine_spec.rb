require_relative './spec_helper'

describe RegistrationEngine do 

  let (:registration)     { Registration.new }
  let (:requirement)      { Requirement.new }
  let (:exemption)        { Exemption.new requirement: requirement, registration: registration }
  let (:check)            { SubmissionCheck.new requirement: requirement, registration: registration }
  let (:ctxd_requirement) { ContextualizedRequirement.new requirement, registration }
  let (:requirements)     { instance_double 'ContextualizedRequirementList' }
  let (:user)             { User.new first_name: 'John', last_name: 'Smith' }
  let (:persistor)        { instance_double 'Neon::Persistor' }
  subject                 { RegistrationEngine.new(registration, requirements).tap { |engine| engine.persistor=persistor } }

  describe 'submitting a registration' do

    describe '#can_submit?' do
      context "when all user facing stuff has been provided" do
        it "should be true if all user facing stuff has been provided" do
          allow(requirements).to receive(:non_provideds).and_return [] 
          expect(subject.can_submit?).to eq true
        end
      end
      context "when at least one user facing thing has not been provided" do
        before :each do
          allow(requirements).to receive(:non_provideds).and_return [ ctxd_requirement ] 
        end
        it "should be false if no exemption has been requested" do
          expect(subject.can_submit?).to eq false
        end
        it "should be true if exemptions have been requested" do
          registration.exemptions << exemption
          expect(subject.can_submit?).to eq true
        end
      end
    end

    describe '#could_submit_with_exemptions?' do
      it "should be true if all mandatory user facing stuff has been provided" do
        allow(requirements).to receive(:mandatories).and_return requirements
        allow(requirements).to receive(:non_provideds).and_return []
        expect(subject.could_submit_with_exemptions?).to eq true
      end
      it "should be false if any user facing stuff has not been provided" do
        allow(requirements).to receive(:mandatories).and_return requirements
        allow(requirements).to receive(:non_provideds).and_return [ :something ] 
        expect(subject.could_submit_with_exemptions?).to eq false
      end
    end

    describe '#submit' do
      context "unhappy path" do
        it "should complain if the registration already has a status" do
          allow(requirements).to receive(:non_provideds).and_return [] 
          registration.current_status = :something
          expect{subject.submit}.to raise_error
        end
        it "should complain if can_submit? says no" do
          expect(requirements).to receive(:non_provideds).and_return [ :something ] 
          expect{subject.submit}.to raise_error
        end
      end
      context "happy path" do
        before :each do 
          expect(requirements).to receive(:non_provideds).and_return [] 
          expect(persistor).to receive(:save) { |reg| reg.id = 27 }
        end
        context "when there are no submission checks and no exemption requests" do
          before :each do 
            expect(requirements).to receive(:checks).and_return [] 
            subject.submit
          end
          it "should set registration status to :approved and save" do
            expect(registration.current_status).to eq :approved
            expect(registration.id).to eq 27
          end
        end
        context "when the registration includes one or more exemption requests" do
          before :each do 
            expect(requirements).to receive(:checks).and_return [] 
            registration.exemptions << exemption
            subject.submit
          end
          it "should set registration status to :pending_approval and save" do
            expect(registration.current_status).to eq :pending_approval
            expect(registration.id).to eq 27
          end
        end
        context "when there are any submission requirements that need to be checked" do
          before :each do 
            expect(requirements).to receive(:checks).and_return [ ctxd_requirement ] 
            subject.submit
          end
          it "should create the corresponding SubmissionCheck items" do
            expect(registration.checks.count).to eq 1
            expect(registration.checks.first.requirement).to eq ctxd_requirement.definition 
          end
          it "should set registration status to :pending_approval and save" do
            expect(registration.current_status).to eq :pending_approval
            expect(registration.id).to eq 27
          end
        end
        context "when there is an exemption request for a submissions that would otherwise need checking" do
          before :each do 
            expect(requirements).to receive(:checks).and_return [ ctxd_requirement ] 
            registration.exemptions << exemption
            subject.submit
          end
          it "should not create any SubmissionCheck items (because the exemption kicks in)" do
            expect(registration.checks.count).to eq 0
          end
          it "should set registration status to :pending_approval and save" do
            expect(registration.current_status).to eq :pending_approval
            expect(registration.id).to eq 27
          end
        end
      end
    end

  end

  describe 'component decisions' do

    describe '#can_decide_component?' do
      before :each do
        registration.exemptions << exemption
      end
      it "should complain vociferously if the component does not belong to the registration" do
        expect{subject.can_decide_component? Exemption.new, :approved}.to raise_error
      end
      it "should be false if the registration status is nil or :cancelled" do
        [nil, :cancelled].each do |status|
          registration.current_status = status
          expect(subject.can_decide_component? exemption, :approved).to eq false
          expect(subject.can_decide_component? exemption, :declined).to eq false
        end
      end
      context "when the registration has been declined" do
        it "should be possible to approve or decline" do
          registration.current_status = :declined
          expect(subject.can_decide_component? exemption, :approved).to eq true
          expect(subject.can_decide_component? exemption, :declined).to eq true
        end
      end
      context "when the registration has been approved" do
        it "should be possible to approve but not decline" do
          registration.current_status = :approved
          expect(subject.can_decide_component? exemption, :approved).to eq true
          expect(subject.can_decide_component? exemption, :declined).to eq false
        end
      end
    end

    describe '#decide_component' do
      before :each do
        registration.exemptions << exemption
      end
      context "unhappy path" do
        it "should complain vociferously if can_decide_component? says no" do
          expect{subject.decide_component Exemption.new, :approved}.to raise_error
        end
      end
      context "happy path" do
        before :each do
          registration.current_status = :declined
          allow(persistor).to receive(:save) { |decision| decision.id = 42 }
        end
        it "should record the decision" do
          subject.decide_component exemption, :approved
          expect(exemption.decision.value).to eq 'approved'
        end
        it "should also record comments if passed" do
          subject.decide_component exemption, :approved, comments: 'ho hum'
          expect(exemption.decision.comments).to eq 'ho hum'
        end
        it "should also record who did it if passed" do
          subject.decide_component exemption, :approved, user: user
          expect(exemption.decision.by).to eq 'John Smith'
        end
        it "should also save the decision" do
          subject.decide_component exemption, :approved
          expect(exemption.decision.id).to eq 42
        end
      end
    end

  end

  describe 'registration decisions' do

    describe '#can_decide?' do
      before :each do
        registration.exemptions << exemption
      end
      it "should be false if the registration status is nil or :cancelled" do
        [nil, :cancelled].each do |status|
          registration.current_status = status
          expect(subject.can_decide? :approved).to eq false
          expect(subject.can_decide? :declined).to eq false
        end
      end
      context "when all registration components have been approved" do
        it "should be possible to approve or decline" do
          exemption.decisions.push Decision.new value: :approved
          [:pending_approval, :approved, :declined].each do |status|
            registration.current_status = status
            expect(subject.can_decide? :approved).to eq true
            expect(subject.can_decide? :declined).to eq true
          end
        end
      end
      context "when one or more registration components have not been approved" do
        it "should be possible to decline but not approve" do
          [:pending_approval, :approved, :declined].each do |status|
            registration.current_status = status
            expect(subject.can_decide? :approved).to eq false
            expect(subject.can_decide? :declined).to eq true
          end
        end
      end
    end

    describe '#decide' do
      context 'unhappy path' do
        it "should complain if can_decide? says no" do
          registration.current_status = :cancelled
          expect{subject.decide :approved}.to raise_error
        end
      end
      context 'happy path' do
        before :each do
          registration.current_status = :pending_approval
          allow(persistor).to receive(:save) do |obj|
            obj.id = 58
            obj.last_modified = DateTime.now
          end
        end
        it "should record the decision" do
          subject.decide :approved
          expect(registration.decision.value).to eq 'approved'
        end
        it "should also record comments if passed" do
          subject.decide :approved, comments: 'ho hum'
          expect(registration.decision.comments).to eq 'ho hum'
        end
        it "should also record who did it if passed" do
          subject.decide :approved, user: user
          expect(registration.decision.by).to eq 'John Smith'
        end
        it "should also save the decision" do
          subject.decide :approved
          expect(registration.decision.id).to eq 58
        end
        it "should also set current status on the registration itself" do
          subject.decide :approved
          expect(registration.current_status).to eq :approved
        end
        it "should also save the registration itself" do
          last_modified = registration.last_modified
          subject.decide :approved
          expect(registration.last_modified).not_to eq last_modified
        end
      end
    end

  end

  # describe 'cancelling a registration' do

  #   describe '#can_cancel?' do
  #     it "should be true for any registration that has not already been cancelled" do
  #       expect(subject.can_cancel?).to eq true
  #     end
  #     it "should be false for a registration that has already been cancelled" do
  #       registration.current_status = :cancelled
  #       expect(subject.can_cancel?).to eq false
  #     end
  #   end

  #   describe '#cancel' do
  #     it "should complain if can_cancel? says no" do
  #       registration.current_status = :cancelled
  #       expect{subject.cancel}.to raise_error
  #     end
  #     it "should set current status" do
  #       subject.cancel
  #       expect(registration.current_status).to eq :cancelled
  #     end
  #   end

  # end

end
