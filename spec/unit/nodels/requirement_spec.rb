require_relative './spec_helper'

describe Requirement do 

  def fake_filter(options = {})
    filter = instance_double('ReferenceFilter')
    allow(filter).to receive(:valid?).and_return options[:valid] != false
    allow(filter).to receive(:errors).and_return double('some error collection object').as_null_object
    allow(filter).to receive(:applicable_to?).and_return options[:applicable_to]
    allow(filter).to receive(:to_hash).and_return({:filter => 'handles this'})
    filter
  end

  describe 'comparisons (for sorting)' do

    let (:submittable_a_a)  { Submittable.new type: 'A', name: 'A' } 
    let (:submittable_a_b)  { Submittable.new type: 'A', name: 'B' } 
    let (:submittable_b_a)  { Submittable.new type: 'B', name: 'B' } 

    let (:mandatory_a_a)    { Requirement.new mandatory: true,  type: 'Submittable', target: submittable_a_a }
    let (:mandatory_a_b)    { Requirement.new mandatory: true,  type: 'Submittable', target: submittable_a_b }
    let (:mandatory_b_a)    { Requirement.new mandatory: true,  type: 'Submittable', target: submittable_b_a }

    let (:optional_a_a)     { Requirement.new mandatory: false,  type: 'Submittable', target: submittable_a_a }
    let (:optional_a_b)     { Requirement.new mandatory: false,  type: 'Submittable', target: submittable_a_b }
    let (:optional_b_a)     { Requirement.new mandatory: false,  type: 'Submittable', target: submittable_b_a }

    let (:approval_1)       { Requirement.new type: 'Approval' }
    let (:approval_2)       { Requirement.new type: 'Approval' }

    it "should deal with same" do
      expect(mandatory_a_a <=> mandatory_a_a).to eq 0
    end
    it "should always put submittables before approvals" do
      expect(mandatory_a_a <=> approval_1).to eq -1
      expect(optional_b_a <=> approval_1).to eq -1
    end
    it "should put mandatories before optionals" do
      expect(mandatory_b_a <=> optional_a_a).to eq -1
    end
    it "should sort by type next" do
      expect(mandatory_a_b <=> mandatory_b_a).to eq -1
    end
    it "should sort by name next" do
      expect(mandatory_a_a <=> mandatory_a_b).to eq -1
    end
    it "should not crash where fields are not available" do
      expect(Requirement.new <=> Requirement.new).not_to eq 0
    end
  end

  describe '#valid?' do
    subject { 
      tenant = Tenant.new id: 1
      cert = Submittable.new id: 27
      Requirement.new tenant: tenant, target: cert, mandatory: true
    }
    it "should be true if no filters are present" do
      expect(subject.valid?).to eq true      
    end
    it "should be true if all filters are valid" do
      subject.filters = [ fake_filter, fake_filter ]
      expect(subject.valid?).to eq true      
    end
    it "should be false if any of the filters are not valid" do
      subject.filters = [ fake_filter, fake_filter(valid: false) ]
      expect(subject.valid?).to eq false      
    end
  end
  
  describe '#applicable_to?' do

    let (:ctx) { instance_double('RequirementContext') }

    it "should be true by default" do
      req = Requirement.new
      expect(req.applicable_to?(ctx)).to eq true
    end

    it "should be true if no filters reject it" do
      req = Requirement.new filters: [ fake_filter(applicable_to: true), fake_filter(applicable_to: true) ]
      expect(req.applicable_to?(ctx)).to eq true
    end

    it "should be false if any filter rejects it" do
      req = Requirement.new filters: [ fake_filter(applicable_to: true), fake_filter(applicable_to: false) ]
      expect(req.applicable_to?(ctx)).to eq false
    end

  end

  describe "to and from hash" do

    let (:req) {
      cert = Submittable.new id: 27
      Requirement.new id: 28, target: cert, filters: [ fake_filter, fake_filter ]
    }

    it "should be able to represent itself as a hash" do
      expected = {  :id => 28, 
                    :target => { :type => 'Submittable', :id => 27 },
                    :filters=> [ { :filter => "handles this" }, { :filter => "handles this" } ] } 
      expect(req.to_hash).to include expected
    end
    it "should be able to roundtrip" do
      double = class_double('ReferenceFilter').as_stubbed_const
      allow(double).to receive(:from_hash).and_return fake_filter
      hash = req.to_hash
      req2 = Requirement.from_hash hash
      expect(req2.to_hash).to eq hash
    end

  end

end

