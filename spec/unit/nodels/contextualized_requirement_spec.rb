require_relative './spec_helper'

describe ContextualizedRequirement do 
  
  let (:registration)                 { Registration.new person: person, position: position }

  let (:submittable)                  { Submittable.new }
  let (:submission)                   { Submission.new submittable: submittable }
  let (:required_submittable)         { Requirement.new type: 'Submittable', target: submittable }

  let (:required_approval)            { Requirement.new type: 'Approval' }

  let (:person_with_no_submission)    { Person.new }
  let (:person_with_submission)       { Person.new submissions: [submission] }

  let (:registration)                 { Registration.new }

  describe '#has_valid_submission?' do
    subject { ContextualizedRequirement.new required_submittable, registration }
    it "should be false if there are no submissions" do
      registration.person = person_with_no_submission
      puts registration.inspect
      expect(subject.has_valid_submission?).to eq false
    end
    it "should be true if there are valid submissions" do
      registration.person = person_with_submission
      expect(subject.has_valid_submission?).to eq true
    end
  end

  describe '#user_facing?' do
    it "should be true for submittable requirements" do
      cr = ContextualizedRequirement.new required_submittable, registration
      expect(cr.user_facing?).to eq true
    end
    it "should be false for approval requirements" do
      cr = ContextualizedRequirement.new required_approval, registration
      expect(cr.user_facing?).to eq false
    end
  end



end