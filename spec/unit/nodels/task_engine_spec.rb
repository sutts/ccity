require_relative './spec_helper'

def exemption_request_event(decision)
  exemption_request = ExemptionRequest.new
  exemption_request.decisions.push Decision.new(ok: decision) unless decision.nil?
  Hashie::Mash.new type: :exemption_request, obj: exemption_request
end

describe TaskEngine do

  let (:submittable)      { Submittable.new }
  let (:requirement)      { Requirement.new target: submittable, when: :upfront }
  let (:engine)           { TaskEngine.new timeline }
  let (:submission)       { instance_double Submission }
  let (:submission_event) { Hashie::Mash.new type: :submission, obj: submission }
  let (:timeline)         { instance_double PersonTimeline }

  describe '#for_submitter' do

    context "with an exemption request that has not been decided" do
      subject {
        expect(timeline).to receive(:most_recent_exemption_request_for).and_return exemption_request_event(nil)
        engine.for_submitter requirement
      }
      its('count')              { is_expected.to eq 0 }
    end

    context "with an exemption request that has been granted" do
      subject {
        expect(timeline).to receive(:most_recent_exemption_request_for).and_return exemption_request_event(true)
        engine.for_submitter requirement
      }
      its('count')              { is_expected.to eq 0 }
    end

    context "with an exemption request that has been declined (should behave exact same as no exemption request in the first place" do
      subject {
        expect(timeline).to receive(:most_recent_exemption_request_for).and_return exemption_request_event(false)
        expect(timeline).to receive(:most_recent_submission_for).and_return nil
        engine.for_submitter requirement
      }
      its('count')              { is_expected.to eq 1 }
      its('first.problem')      { is_expected.to eq :no_submission }
      its('first.requirement')  { is_expected.to eq requirement }
    end

    context "with an exemption request that has been declined (same as no exemption request at all)" do

      before :each do
        allow(timeline).to receive(:most_recent_exemption_request_for).and_return nil
      end

      context "when there are no submissions for the given requirement" do
        subject {
          expect(timeline).to receive(:most_recent_submission_for).and_return nil
          engine.for_submitter requirement
        }
        its('count')              { is_expected.to eq 1 }
        its('first.problem')      { is_expected.to eq :no_submission }
        its('first.requirement')  { is_expected.to eq requirement }
      end

      context "when there is a most recent submission (that may or not be valid)" do

        context "where the submission has been explicitly rejected" do
          it "should create one and only one resubmit task (and not check other aspects of the submission)" do
            expect(timeline).to receive(:most_recent_submission_for).and_return submission_event
            allow(submission).to receive(:rejected?).and_return true
            tasks = engine.for_submitter requirement
            expect(tasks.count).to eq 1
          end
        end

        context "where the submission has not been explicitly rejected" do
          it "should create a task where the submission is expired or expiring" do
            allow(timeline).to receive(:most_recent_submission_for).and_return submission_event
            allow(submission).to receive(:rejected?).and_return false
            [true, false].each do |expired|
              [true, false].each do |expiring|
                [true, false].each do |incorrect|
                  allow(submission).to receive(:expired?).and_return expired
                  allow(submission).to receive(:expiring?).and_return expiring
                  allow(submission).to receive(:correct?).and_return not(incorrect)
                  tasks = engine.for_submitter requirement
                  # there are some cases where there should be 2 tasks...
                  if incorrect and (expired or expiring)
                    expect(tasks.count).to eq 2
                  # for others we check that the correct :problem is being set
                  elsif incorrect and not (expired or expiring)
                    expect(tasks.count).to eq 1
                    expect(tasks.first.problem).to eq :incorrect_submission
                  elsif expired and not incorrect
                    expect(tasks.first.problem).to eq :expired_submission
                  elsif expiring and not incorrect
                    expect(tasks.first.problem).to eq :expiring_submission
                  end
                end
              end
            end
          end
        end

      end
    end
  end

  describe '#for_reviewer' do

    context "when there is an exemption request with no decision" do
      subject {
        expect(timeline).to receive(:most_recent_exemption_request_for).and_return exemption_request_event(nil)
        engine.for_reviewer requirement
      }
      its('count')              { is_expected.to eq 1 }
      its('first.requirement')  { is_expected.to eq requirement }
      its('first.problem')      { is_expected.to eq :pending_exemption_request_decision }
    end

    context "when there is an exemption request with decision" do
      subject {
        expect(timeline).to receive(:most_recent_exemption_request_for).and_return exemption_request_event(true)
        engine.for_reviewer requirement
      }
      its('count')              { is_expected.to eq 0 }
    end

    context "when there is no exemption in place" do

      before :each do
        expect(timeline).to receive(:most_recent_exemption_request_for).and_return nil
      end

      context "when there are no submissions for the given requirement" do
        subject {
          expect(timeline).to receive(:most_recent_submission_for).and_return nil
          engine.for_reviewer requirement
        }
        its('count')              { is_expected.to eq 0 }
      end

      let (:event_for_valid_submission) {
        submission = instance_double Submission
        allow(submission).to receive(:expired?).and_return false
        allow(submission).to receive(:correct?).and_return true
        Hashie::Mash.new type: :submission, obj: submission
      }

      context "when there is a valid submission that has no decisions yet" do
        subject {
          expect(timeline).to receive(:most_recent_submission_for).and_return event_for_valid_submission
          expect(event_for_valid_submission.obj).to receive(:decisions).and_return []
          engine.for_reviewer requirement
        }
        its('count')              { is_expected.to eq 1 }
        its('first.requirement')  { is_expected.to eq requirement }
        its('first.problem')      { is_expected.to eq :pending_submission_review }
        its('first.target')       { is_expected.to eq event_for_valid_submission.obj }
      end

      context "when there is a valid submission that does have a decision" do
        subject {
          expect(timeline).to receive(:most_recent_submission_for).and_return event_for_valid_submission
          expect(event_for_valid_submission.obj).to receive(:decisions).and_return [:not_empty]
          engine.for_reviewer requirement
        }
        its('count')              { is_expected.to eq 0 }
      end

    end

  end

end 

