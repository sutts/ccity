require_relative './spec_helper'

describe ReferenceFilter do 
  
  let (:electrician)    { Position.new name: 'Electrician', id: 1, tags: ['Trade'] }
  let (:carpenter)      { Position.new name: 'Carpenter',   id: 2, tags: ['Trade'] }
  let (:cleaner)        { Position.new name: 'Cleaner',     id: 3, tags: ['Unskilled'] }

  let (:leederville)    { Location.new name: 'Leederville', id: 11 }
  let (:kingsley)       { Location.new name: 'Kingsley',    id: 12 }

  describe '#valid?' do
    it "should be true if the filter has one or more references" do
      filter = ReferenceFilter.new type: 'Position', references: [electrician]
      expect(filter.valid?).to eq true
    end
    it "should be true if the filter has one or more tag names" do
      filter = ReferenceFilter.new type: 'Position', tags: ['whatever']
      expect(filter.valid?).to eq true
    end
    it "should be false if the filter has neither references nor tag names" do
      filter = ReferenceFilter.new type: 'Position'
      expect(filter.valid?).to eq false
    end
    it "should be false if the type does not match its references" do
      filter = ReferenceFilter.new type: 'Position', references: [leederville]
      expect(filter.valid?).to eq false
      expect(filter.errors).to eq( { :reference => ["expected type 'Position' but got 'Location'"] } )
    end
  end

  describe '#applicable_to?' do
    let (:electrician_registration)        { Registration.new position: electrician }
    let (:cleaner_registation)             { Registration.new position: cleaner     }
    let (:position_not_set_registration)   { Registration.new                       }
    context "with direct references" do
      context "when mode is include" do
        subject { ReferenceFilter.new type: 'Position', references: [carpenter, electrician], mode: 'include' }
        it "should behave as you would expect" do
          expect(subject.applicable_to? electrician_registration).to eq true  
          expect(subject.applicable_to? cleaner_registation).to eq false  
          expect(subject.applicable_to? position_not_set_registration).to eq true  
        end
      end
      context "when mode is exclude" do
        subject { ReferenceFilter.new type: 'Position', references: [carpenter, electrician], mode: 'exclude' }
        it "should behave as you would expect" do
          expect(subject.applicable_to? electrician_registration).to eq false  
          expect(subject.applicable_to? cleaner_registation).to eq true  
          expect(subject.applicable_to? position_not_set_registration).to eq true  
        end
      end
    end
    context "with tags" do
      context "when mode is include" do
        subject { ReferenceFilter.new type: 'Position', tags: ['Trade'], mode: 'include' }
        it "should behave as you would expect" do
          expect(subject.applicable_to? electrician_registration).to eq true  
          expect(subject.applicable_to? cleaner_registation).to eq false  
          expect(subject.applicable_to? position_not_set_registration).to eq true  
        end
      end
      context "when mode is exclude" do
        subject { ReferenceFilter.new type: 'Position', tags: ['Trade'], mode: 'exclude' }
        it "should behave as you would expect" do
          expect(subject.applicable_to? electrician_registration).to eq false  
          expect(subject.applicable_to? cleaner_registation).to eq true  
          expect(subject.applicable_to? position_not_set_registration).to eq true  
        end
      end
    end
  end

  describe "to and from hash" do

    let (:filter) { ReferenceFilter.new id: 11, 
                                        type: 'Position', 
                                        mode: :exclude, 
                                        tags: ['Trade'], 
                                        references: [electrician] }

    it "should be able to represent itself as a hash" do
      expected = {  :id => 11,
                    :mode => :exclude, 
                    :type => 'Position',
                    :tags => ['Trade'],
                    :references => [ { :type => 'Position', :id => 1, :name => 'Electrician' } ] } 
      expect(filter.to_hash).to eq expected
    end
    it "should be able to roundtrip" do
      hash = filter.to_hash
      filter2 = ReferenceFilter.from_hash hash
      expect(filter2.to_hash).to eq hash
    end
  end

end