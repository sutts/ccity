require_relative './spec_helper'

describe Submission do 
  
  let(:submittable) { Submittable.new number: false,
                                      issued_by: true, issued_by_as: 'Name of association',
                                      issue_date: true, issue_date_as: 'Join date',
                                      expiry_date: true, expiry_date_as: 'Renewal date',
                                      attachments: false }
  
  describe '#expired? (and #expiring?)' do

    subject { Submission.new submittable: submittable }

    it "should be expired (but not expiring) if expiry date is in the past" do
      subject.expiry_date = Date.today.prev_day
      expect(subject.expired?).to be true
      expect(subject.expiring?).to be false
    end
    it "should be not expired (but expiring) if expiry date is today" do
      subject.expiry_date = Date.today
      expect(subject.expired?).to be false
      expect(subject.expiring?).to be true
    end
    it "should be not expired (but expiring) if expiry is soon" do
      subject.expiry_date = Date.today.next_day
      expect(subject.expired?).to be false
      expect(subject.expiring?).to be true
    end
    it "should both be false if expiry is more than 30 days in the future" do
      subject.expiry_date = Date.today.next_year
      expect(subject.expired?).to be false
      expect(subject.expiring?).to be false
    end
    it "should both be false if expiry date is not present" do
      subject.expiry_date = nil
      expect(subject.expired?).to be false
      expect(subject.expiring?).to be false
    end
    it "should both be false if expiry is not configured" do
      subject.submittable.expiry_date = false
      subject.expiry_date = Date.today.prev_day
      expect(subject.expired?).to be false
      expect(subject.expiring?).to be false
    end

  end                                     

  describe '#entries' do
    
    subject { Submission.new submittable: submittable, issued_by: 'ABC' }

    it "should contain one element for each configured field" do
      expect(subject.entries.count).to eq 3
    end

    it "should include the display names" do
      expect(subject.entries.map(&:name)).to eq ['Name of association', 'Join date', 'Renewal date']
    end

    it "should include the values where available" do
      expect(subject.entries.map(&:value)).to eq ['ABC', nil, nil]
    end

    it "should flag where values are missing" do
      expect(subject.entries.map(&:error)).to eq [nil, :missing, :missing]
    end

  end

  describe '#correct?' do

    subject { Submission.new submittable: submittable, issued_by: 'ABC', issue_date: Date.today, expiry_date: Date.today }

    it "should be true if all configured fields are supplied and valid" do
      expect(subject.correct?).to be true
    end 

    it "should be false if any configured fields are not supplied" do
      subject.issued_by = nil
      expect(subject.correct?).to be false
    end 

    it "should be false if expired is true" do
      subject.expiry_date = Date.today.prev_day
      expect(subject.correct?).to be false
    end 

  end                                      


end
