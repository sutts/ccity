require_relative './spec_helper'

describe PersonTimeline do 

  let (:sally)          { User.new given_name: 'Sally', family_name: 'Noon' }
  let (:submittable1)   { Submittable.new  }
  let (:submittable2)   { Submittable.new  }
  let (:submittable3)   { Submittable.new  }
  let (:registration)   { Registration.new      created_at: DateTime.parse('09 Dec 2014'), submitter: sally }
  let (:exemption_req)  { ExemptionRequest.new  created_at: DateTime.parse('10 Dec 2014'), submitter: sally }
  let (:submission1a)   { Submission.new        created_at: DateTime.parse('11 Dec 2014'), submittable: submittable1,  by: 'Bill' }
  let (:decision1a)     { Decision.new          created_at: DateTime.parse('12 Dec 2014'), ok: false,                  by: 'Frank' }
  let (:submission1b)   { Submission.new        created_at: DateTime.parse('13 Dec 2014'), submittable: submittable1,  by: 'Bill' }
  let (:decision1b)     { Decision.new          created_at: DateTime.parse('14 Dec 2014'), ok: true,                   by: 'Frank' }
  let (:submission2)    { Submission.new        created_at: DateTime.parse('15 Dec 2014'), submittable: submittable2,  by: 'Fred' }

  let (:person) { 
    registration.exemption_requests << exemption_req
    submission1a.decisions << decision1a
    submission1b.decisions << decision1b
    Person.new submissions: [ submission1b, submission2, submission1a ], registrations: [ registration ] 
  }
  
  describe '#to_a' do

    subject           { PersonTimeline.new(person).to_a }

    it "should extract events and organize in reverse chronlogical order" do
      expect(subject.map(&:obj)).to eq [submission2, decision1b, submission1b, 
                                        decision1a, submission1a, exemption_req, registration]
    end

    it "should decorate events with type" do
      expect(subject.map(&:type)).to eq [ :submission, :decision, :submission, :decision, 
                                          :submission, :exemption_request, :registration]
    end

    it "should decorate events with whodunnit" do
      expect(subject.map(&:by)).to eq ['Fred', 'Frank', 'Bill', 'Frank', 'Bill', 'Sally Noon', 'Sally Noon']
    end

    it "should decorate events with when they occurred" do
      expect(subject.first.when).to eq DateTime.parse('15 Dec 2014')
    end

  end

  describe '#most_recent_submission_for' do

    subject           { PersonTimeline.new(person) }

    it "should return the most submission-related event for the passed submittable" do
      expect(subject.most_recent_submission_for(submittable1).obj).to be submission1b
      expect(subject.most_recent_submission_for(submittable3)).to be_nil
    end

  end




end

