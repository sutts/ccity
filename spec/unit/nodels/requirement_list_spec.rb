require_relative './spec_helper'

def fake_requirement(applicable)
  req = instance_double('Requirement')
  allow(req).to receive(:applicable_to?).and_return applicable
  req
end

describe RequirementList do 
  
  describe '#applicables' do

    let (:ctx) { instance_double('RequirementContext') }

    it "should return a subset list of those requirements that apply to a given context" do
      req1 = fake_requirement false
      req2 = fake_requirement true
      list = RequirementList.new [req1, req2]
      expect(list.applicables(ctx)).to eq [req2]
    end

  end

end

