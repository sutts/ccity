require_relative './spec_helper'

def fake_contextualized_requirement(list, options={})
  defaults = { :user_facing? => true }
  instance_double('ContextualizedRequirement').tap do |cr|
    defaults.merge(options).each { |k, v| allow(cr).to receive(k).and_return v }
    list << cr
  end
end

describe ContextualizedRequirementList do 
  describe '.wrap' do
    let (:req1) { instance_double 'Requirement'  }
    let (:req2) { instance_double 'Requirement'  }
    let (:reg)  { instance_double 'Registration' }
    subject     { ContextualizedRequirementList.wrap [req1, req2], reg }
    its(:count)               { should eq 2     }
    its('first.requirement')  { should eq req1  }
    its('last.requirement')   { should eq req2  }
    its('first.context')      { should eq reg   }
    its('last.context')       { should eq reg   }
  end

  describe '#user_facings' do
    it "should return a new instance that only includes user facing elements" do
      cr1 = fake_contextualized_requirement subject, user_facing?: false
      cr2 = fake_contextualized_requirement subject, user_facing?: true
      expect(subject.user_facings).to eq [cr2]
      expect(subject.non_user_facings).to eq [cr1]
    end
  end
  describe '#mandatories' do
    it "should return a new instance that only includes mandatory elements" do
      cr1 = fake_contextualized_requirement subject, mandatory?: false
      cr2 = fake_contextualized_requirement subject, mandatory?: true
      expect(subject.mandatories).to eq [cr2]
      expect(subject.non_mandatories).to eq [cr1]
    end
  end
  describe '#provideds' do
    it "should return a new instance that only includes elements where relevant information has been provided" do
      cr1 = fake_contextualized_requirement subject, has_valid_submission?: true
      cr2 = fake_contextualized_requirement subject, has_valid_submission?: false
      expect(subject.provideds).to eq [cr1]
      expect(subject.non_provideds).to eq [cr2]
    end
  end
  describe '#exemptables' do
    it "should return a new instance that only includes non-mandatory elements where relevant information has not been provided" do
      cr1 = fake_contextualized_requirement subject, has_valid_submission?: false, mandatory?: false
      cr2 = fake_contextualized_requirement subject, has_valid_submission?: false, mandatory?: true
      expect(subject.exemptables).to eq [cr1]
    end
  end
  describe '#checks' do
    it "should return a new instance that only includes submissions that require checking" do
      cr1 = fake_contextualized_requirement subject, check?: true
      cr2 = fake_contextualized_requirement subject, check?: false
      expect(subject.checks).to eq [cr1]
    end
  end

end
