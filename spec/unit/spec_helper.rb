# Manually require some gems

require 'rubygems' 
require 'bundler/setup'
require 'hashie'
require 'neography'
require 'veto'
require 'rspec/its'

# Include bits and pieces from active support we want to use

require 'active_support/core_ext/object/try.rb'

# Manually require source (in the correct sequence) 

Dir.glob("app/lib/neon/*.rb").each              { |rel_path| require File.join File.expand_path("."), rel_path }
Dir.glob("app/lib/**.rb").each                  { |rel_path| require File.join File.expand_path("."), rel_path }
Dir.glob("app/nodels/tenanted.rb").each         { |rel_path| require File.join File.expand_path("."), rel_path }
Dir.glob("app/nodels/decision.rb").each         { |rel_path| require File.join File.expand_path("."), rel_path }
Dir.glob("app/nodels/submission_check.rb").each { |rel_path| require File.join File.expand_path("."), rel_path }
Dir.glob("app/nodels/*.rb").each                { |rel_path| require File.join File.expand_path("."), rel_path }

# Could replace all of the above with one line: require_relative '../../rails_helper.rb'
# But that's slower by several orders of magnitude

# Also configure neo to point at nothing 
Neography.configure do |config|
  config.port        = 9999
end
