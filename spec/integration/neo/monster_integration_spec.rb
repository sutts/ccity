require_relative './spec_helper'

describe 'monster integration test' do

  before :example do
    # Neon::Gateway.new.raw "MATCH (n) OPTIONAL MATCH (n)-[r]-() DELETE n,r"
  end

  let (:tenant)               { Tenant.new(short_name: 'Tenant 1', long_name: 'Tenant 1').tap { |t| t.save } }
  let (:locator)              { locator = tenant.locator_for Requirement }

  let (:electrical_license)   { tenant.create Submittable, name: 'Electrical License WA',  category: 'Trade License' }
  let (:plumbers_license)     { tenant.create Submittable, name: 'Plumbers License WA',    category: 'Trade License' }
  let (:ohs_card)             { tenant.create Submittable, name: 'OHS Card',               category: 'Regulatory' }
  let (:fit_for_work)         { tenant.create Submittable, name: 'Fit for work',           category: 'Medical' }

  let (:electrician)          { tenant.create Position, name: 'Electrician', tags: ['Trade'] }
  let (:plumber)              { tenant.create Position, name: 'Plumber', tags: ['Trade'] }
  let (:visitor)              { tenant.create Position, name: 'Visitor' }

  let (:only_electricians)    { ReferenceFilter.new type: 'Position', mode: 'include', references: [electrician] }
  let (:only_plumbers)        { ReferenceFilter.new type: 'Position', mode: 'include', references: [plumber]     }
  let (:not_visitors)         { ReferenceFilter.new type: 'Position', mode: 'exclude', references: [visitor]     }
  let (:only_trade)           { ReferenceFilter.new type: 'Position', mode: 'include', tags: ['Trade']            }

  let (:req_lic_electrician)  { tenant.create Requirement, type: 'Submittable', target: electrical_license, filters: [only_electricians], mandatory: true }
  let (:req_lic_plumber)      { tenant.create Requirement, type: 'Submittable', target: plumbers_license,   filters: [only_plumbers], mandatory: true }
  let (:req_fit_for_work)     { tenant.create Requirement, type: 'Submittable', target: fit_for_work,       filters: [not_visitors], mandatory: false }
  let (:req_ohs_card)         { tenant.create Requirement, type: 'Submittable', target: ohs_card,           filters: [only_trade], mandatory: false }

  before :example do
    [req_lic_electrician, req_lic_plumber, req_fit_for_work, req_ohs_card]
  end

  describe 'RequirementList' do

    subject { RequirementList.for tenant }

    def requirements_that_apply_to(position)
      applicables = subject.applicables(Registration.new(position: position)).map(&:target).map(&:name)
    end

    it "should make it easy for me to figure out which requirements apply to a given context independent of person" do
      expect(subject.count).to eq 4
      expect(requirements_that_apply_to(electrician)).to eq ['Electrical License WA', 'Fit for work', 'OHS Card']
      expect(requirements_that_apply_to(plumber)).to eq ['Plumbers License WA', 'Fit for work', 'OHS Card']
      expect(requirements_that_apply_to(visitor)).to eq []
    end

  end

  describe 'full flow' do

    def pluck(list, *attrs)
      attrs.empty? ? list : pluck(list.map(&attrs.first), *attrs[1..-1])
    end

    it "should mostly reflect the flow in the UI" do

      # let's grab our person
      bilbo = tenant.create Person, given_name: 'Bilbo', family_name: 'Baggins', email: 'bilbo@abc.com'
      bilbo = tenant.locator_for(Person).with_registrations_incl_positions
                                        .with_submissions_incl_submittables
                                        .find bilbo.id

      # no submissions yet and so registrations
      expect(bilbo.submissions.count).to eq 0                                        
      expect(bilbo.registrations.count).to eq 0                                        

      # play with an in-memory registration
      Registration.new(tenant: tenant, position: electrician, person: bilbo).tap do |registration|

        # standard helpers
        requirements = ContextualizedRequirementList.for tenant, registration
        engine = RegistrationEngine.for registration

        # status
        expect(pluck(requirements, :requirement, :target, :name)).to eq               ['Electrical License WA', 'Fit for work', 'OHS Card']
        expect(pluck(requirements.non_provideds, :requirement, :target, :name)).to eq ['Electrical License WA', 'Fit for work', 'OHS Card']
        expect(pluck(requirements.mandatories, :requirement, :target, :name)).to eq   ['Electrical License WA']
        expect(pluck(requirements.exemptables, :requirement, :target, :name)).to eq   ['Fit for work', 'OHS Card']

        # can't submit
        expect(engine.can_submit?).to eq false
        expect(engine.could_submit_with_exemptions?).to eq false

      end

      # okay, let's submit some stuff then
      Submission.new(person: bilbo, submittable: electrical_license).save
      Submission.new(person: bilbo, submittable: ohs_card).save
      bilbo = tenant.locator_for(Person).with_registrations_incl_positions
                                        .with_submissions_incl_submittables
                                        .find bilbo.id

      # and try again
      Registration.new(tenant: tenant, position: electrician, person: bilbo).tap do |registration|

        # standard helpers
        requirements = ContextualizedRequirementList.for tenant, registration
        engine = RegistrationEngine.for registration

        # status
        expect(pluck(requirements.non_provideds, :requirement, :target, :name)).to eq ['Fit for work']
        expect(engine.can_submit?).to eq false
        expect(engine.could_submit_with_exemptions?).to eq true

        # okay, let's request an exemption, submit and save
        registration.exemptions.push Exemption.new requirement: req_fit_for_work, reason: 'Please pretty please'
        engine.submit

      end

      # bilbo should now have one registration
      bilbo = tenant.locator_for(Person).with_registrations_incl_positions.find bilbo.id
      expect(bilbo.registrations.count).to eq 1

      # it should be status pending approval with some exemptions and checks to be done 
      registration = tenant.locator_for(Registration).wide.find bilbo.registrations.first.id
      expect(registration.current_status).to eq :pending_approval
      expect(pluck(registration.exemptions, :requirement, :target, :name)).to eq ['Fit for work']
      expect(pluck(registration.checks, :requirement, :target, :name)).to eq ['Electrical License WA', 'OHS Card']
    
      # meaning we can't approve yet
      engine = RegistrationEngine.for registration
      expect(engine.can_decide? :approved).to eq false

    end

  end

end