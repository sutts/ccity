require_relative './spec_helper'

describe 'CRUD on positions' do

  before :example do
     Neon::Gateway.new.raw "MATCH (n) OPTIONAL MATCH (n)-[r]-() DELETE n,r"
  end 

  let (:tenant)   { Tenant.new(short_name: 'Tenant 1', long_name: 'Tenant 1').tap { |t| t.save } }
  let (:locator)  { locator = tenant.locator_for Position }
  let (:tagster)  { Tagster.new tenant, Position }

  it "should do crud stuff" do

    # initial set up 
    positions = (1..3).map { |i| Position.new name: "Position #{i}", tenant: tenant }
    positions[0].tags = (1..3).map { |i| "Tag #{i}" }
    positions[1].tags = (2..4).map { |i| "Tag #{i}" }
    positions[2].tags = []

    # create
    positions.each do |p| 
      p.save
      expect(p.name).to match /Position [1-3]/
      expect(p.id).to be >= 0
    end

    # all
    positions = tenant.all Position
    expect(positions.count).to eq 3
    expect(positions.map(&:name).sort).to eq ['Position 1', 'Position 2', 'Position 3']
    
    # tags
    tags_from_positions = positions.map(&:tags).flatten.uniq.sort
    expect(tags_from_positions).to eq ['Tag 1', 'Tag 2', 'Tag 3', 'Tag 4']

    # searchy findy
    positions = locator.where name: 'Position 1'
    expect(positions.count).to eq 1
    expect(positions.first.name).to eq 'Position 1'
    expect(positions.first.tags.sort).to eq ['Tag 1', 'Tag 2', 'Tag 3']

    expect(locator.find_by(name: 'Position 1').name).to eq 'Position 1'
    expect(locator.find(positions.first.id).name).to eq 'Position 1'

    expect{locator.find_by(name: 'No such position')}.to raise_error
    expect{locator.find(-1)}.to raise_error

    # update properties
    p1 = positions.first
    p1.name = 'Position 1 renamed'
    p1.tags = ["New tag"]
    p1.save
    
    p1 = locator.find p1.id
    expect(p1.name).to eq 'Position 1 renamed'
    expect(p1.tags).to eq ['New tag']    

    # destroy
    p1.destroy
    expect(locator.all.count).to eq 2

  end

  it "should also do other taggy stuff" do

    # initial set up 
    p1 = tenant.create Position, name: 'Position 1', tags: ['A', 'B']
    p2 = tenant.create Position, name: 'Position 2', tags: ['B', 'C']

    # rename
    tagster.rename 'B', 'BB'
    expect(locator.find(p1.id).tags.sort).to eq ['A', 'BB']
    expect(locator.find(p2.id).tags.sort).to eq ['BB', 'C']

    # destroy
    tagster.remove 'BB'
    expect(locator.find(p1.id).tags).to eq ['A']
    expect(locator.find(p2.id).tags).to eq ['C']

  end

end
