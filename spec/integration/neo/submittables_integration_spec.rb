require_relative './spec_helper'

describe 'CRUD on submittables' do

  before :example do
     Neon::Gateway.new.raw "MATCH (n) OPTIONAL MATCH (n)-[r]-() DELETE n,r"
  end 

  let (:tenant)   { Tenant.new(short_name: 'Tenant 1', long_name: 'Tenant 1').tap { |t| t.save } }
  let (:locator)  { locator = tenant.locator_for Submittable }
  let (:tagster)  { Tagster.new tenant, Submittable }


  it "should do crud stuff" do

    # initial set up 
    certs = (1..3).map { |i| Submittable.new name: "Submittable #{i}", tenant: tenant }
    certs[0].category = 'Cert III'
    certs[1].category = 'Degree'
    certs[2].category = 'Cert III'

    # create
    certs.each do |cert| 
      cert.save
      expect(cert.name).to match /Submittable [1-3]/
      expect(cert.id).to be >= 0
    end

    # all
    certs = locator.all
    expect(certs.count).to eq 3
    expect(certs.map(&:name).sort).to eq ['Submittable 1', 'Submittable 2', 'Submittable 3']
    
    # categories
    categories = certs.map(&:category).uniq.sort
    expect(categories).to eq ['Cert III', 'Degree']

    # searchy findy
    certs = locator.where name: 'Submittable 1'
    expect(certs.count).to eq 1
    expect(certs.first.name).to eq 'Submittable 1'
    expect(certs.first.category).to eq 'Cert III'
  
    expect(locator.find_by(name: 'Submittable 1').name).to eq 'Submittable 1'
    expect(locator.find(certs.first.id).name).to eq 'Submittable 1'

    expect{locator.find_by(name: 'No such item')}.to raise_error
    expect{locator.find(-1)}.to raise_error

    # update properties
    c1 = certs.first
    c1.name = 'Submittable 1 renamed'
    c1.category = 'Cert IV'
    c1.save

    c1 = locator.find c1.id
    expect(c1.name).to eq 'Submittable 1 renamed'
    expect(c1.category).to eq 'Cert IV'    

    #destroy
    c1.destroy
    expect(locator.all.count).to eq 2

  end

  it "should also do other taggy stuff" do

    # initial set up 
    p1 = tenant.create Submittable, name: 'Cert 1', category: 'Degree'
    p2 = tenant.create Submittable, name: 'Cert 2', category: 'Degree'

    # rename where exists
    tagster.rename_category 'Degree', 'PhD'
    expect(locator.find(p1.id).category).to eq 'PhD'
    expect(locator.find(p2.id).category).to eq 'PhD'

  end

end